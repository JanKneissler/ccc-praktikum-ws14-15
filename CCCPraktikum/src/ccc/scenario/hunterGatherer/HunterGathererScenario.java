package ccc.scenario.hunterGatherer;

import java.awt.Color;
import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.demoScenarios.ExampleReferenceController;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actions.DiskModification;
import diskworld.actuators.Mover;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.interfaces.Sensor;
import diskworld.linalg2D.Point;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.sensors.FloorSensor;
import diskworld.sensors.PlaceSensor;
import diskworld.sensors.RGBColorDiskMaterialResponse;
import diskworld.sensors.RGBColorFloorTypeResponse;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class HunterGathererScenario extends DiskWorldScenario {

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Constants 						 													 */
	///////////////////////////////////////////////////////////////////////////////////////////

	// Minimum and maximum sizes of the environment
	private static final int ENV_MIN_SIZE_X = 50;
	private static final int ENV_MAX_SIZE_X = 100;
	private static final int ENV_MIN_SIZE_Y = 50;
	private static final int ENV_MAX_SIZE_Y = 100;
	private static final double FOODITEM_MIN_SIZE = 0.03;
	private static final double FOODITEM_MAX_SIZE = 0.5;
	private static final double ANIMAL_MIN_SIZE = 0.1;
	private static final double ANIMAL_MAX_SIZE = 0.8;
	private static final double AGENT_MIN_SCALE = 0.2;
	private static final double AGENT_MAX_SCALE = 0.5;
	private static final DiskMaterial FOOD_MATERIAL_HEALTHY = new DiskMaterial(1.0, 0.5, 1.0, 1.0, Color.RED);
	private static final DiskMaterial FOOD_MATERIAL_POISONOUS = new DiskMaterial(1.0, 0.5, 1.0, 1.0, Color.PINK);
	private static final int FOODITEM_ENERGY_PER_RADIUS = 10;
	private static final int ANIMAL_ENERGY_PER_RADIUS = 20;
	public static final FloorCellType WATER = new FloorCellType(400.0, Color.BLUE);
	private static final double RATE_OF_HEALTHY_FOODITEMS = 0.75; //ratio healthy food items / all food items
	private static final int MIN_NUM_ANIMALS = 3;
	private static final int MAX_NUM_ANIMALS = 6;
	private static final int MIN_NUM_FOODITEMS = 5;
	private static final int MAX_NUM_FOODITEMS = 15;

	private static enum AgentType {
		NPC, CONTROLLED
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 																				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	private Environment environment;
	private boolean consumedFoodItem; //true if agent ate food in the last time step
	private boolean consumedAnimal; //true if agent ate animal
	private double radiusOfConsumedFoodItem; //radius of last eaten foodItem
	private double radiusOfConsumedAnimal; //radius of last eaten animal
	private Random random;
	private int numOfFoodItems; //current number of foodItems in environment
	Landscape landscape;

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "Hunter & Gatherer";
	}

	@Override
	public String getDescription() {
		return "There's healthy and poisonous food growing which the agent can \n gather and eat. He should avoid crossing water.\n" +
				"Some animals graze on the other side of the river.";
	}

	@Override
	public String getAuthors() {
		return "Svenja Melbaum";
	}

	@Override
	public String getContactEmail() {
		return "svenja.melbaum@student.uni-tuebingen.de";
	}

	@Override
	public String getVersion() {
		return "0.1";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario          						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X, ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex, double sizey) {
		// create bounding walls at the 4 sides of the environment
		return ScenarioUtils.getBoundingWalls(sizex, sizey);
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		return 100000;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid red...
		res.getColorScheme().gridColor = Color.RED;
		// enable/disable options if needed: disable skeleton display...
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		// always one controlled agent
		return 1;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		//store environment for later use
		this.environment = environment;

		this.random = random;
		this.numOfFoodItems = 0;

		// Initialize the floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, FloorCellType.GRASS);
			}
		}

		landscape = new Landscape(environment, random);

		//create random number of food items, mainly good ones
		int numFood = ScenarioUtils.uniform(random, MIN_NUM_FOODITEMS, MAX_NUM_FOODITEMS);
		ObjectConstructor goodFoodConstructor = getFoodConstructor(FOOD_MATERIAL_HEALTHY);
		int numGoodFood = (int) (numFood * RATE_OF_HEALTHY_FOODITEMS);
		createFoodItems(goodFoodConstructor, environment, random, numGoodFood);
		ObjectConstructor badFoodConstructor = getFoodConstructor(FOOD_MATERIAL_POISONOUS);
		createFoodItems(badFoodConstructor, environment, random, numFood - numGoodFood);
	}

	/**
	 * Creates a constructor for food items.
	 * 
	 * @param material
	 * @return An {@link diskworld.ObjectConstructor} to be used to construct food items
	 */
	private ObjectConstructor getFoodConstructor(DiskMaterial material) {
		DiskType foodType = new DiskType(material);
		ObjectConstructor foodConstructor = environment.createObjectConstructor();
		foodConstructor.setRoot(1.0, true, foodType);
		return foodConstructor;
	}

	/**
	 * Creates Food Items. Food items are placed on grass, not on water or on the bridge
	 * 
	 * @param foodConstructor
	 * @param environment
	 * @param random
	 * @param numFood
	 *            Number of food items to be created
	 */
	private void createFoodItems(ObjectConstructor foodConstructor, Environment environment, Random random, int numFood) {
		int num = 0;
		do {
			// random position and size
			double posx = ScenarioUtils.uniform(random, 0.0, environment.getMaxX());
			double posy = ScenarioUtils.uniform(random, 0.0, environment.getMaxY());
			double size = ScenarioUtils.uniform(random, FOODITEM_MIN_SIZE, FOODITEM_MAX_SIZE);
			if (environment.getFloor().getType(posx, posy).equals(HunterGathererScenario.WATER) || environment.getFloor().getType(posx, posy).equals(FloorCellType.STONE)) {
				continue;
			}
			DiskComplex diskComplex = foodConstructor.createDiskComplex(posx, posy, 0.0 /* orientation does not matter */, size);
			if (diskComplex != null) {
				numOfFoodItems++;
				num++;
			}
		} while (num < numFood);
	}

	/**
	 * Creates Animals as NPC Agents. The river cuts the environment in two parts; animals are to be placed on smaller part
	 * Animals are placed on grass, not on water or on the bridge
	 * 
	 * @param animalConstructor
	 * @param environment
	 * @param random
	 * @param numAnimals
	 *            number of animals to be created
	 * @return
	 */
	private NPCAgentData[] createAnimals(AgentConstructor animalConstructor, Environment environment, Random random, int numAnimals) {
		int[] range = getPossiblePositions(AgentType.NPC);
		int minX = range[0];
		int maxX = range[1];
		int minY = range[2];
		int maxY = range[3];
		int estimatedPossibleAnimalPositions = range[4];
		NPCAgentData[] createdAnimals = new NPCAgentData[numAnimals];

		int counter = -1;
		int num = 0;
		do {
			counter++;
			// random position and size
			double posx = ScenarioUtils.uniform(random, minX, maxX);
			double posy = ScenarioUtils.uniform(random, minY, maxY);
			double size = ScenarioUtils.uniform(random, ANIMAL_MIN_SIZE, ANIMAL_MAX_SIZE);
			if (counter > estimatedPossibleAnimalPositions)
				return null; //give up when there are too little possible positions to place animals
			if (environment.getFloor().getType(posx, posy).equals(HunterGathererScenario.WATER) || environment.getFloor().getType(posx, posy).equals(FloorCellType.STONE))
				continue;

			NPCAgentData diskComplex = animalConstructor.createNPCAgent(posx, posy, Math.toRadians(random.nextDouble() * 360), size, new AnimalController(random));
			if (diskComplex != null) {
				createdAnimals[num] = diskComplex;
				num++;
			}

		} while (num < numAnimals);

		return createdAnimals;
	}

	/**
	 * Returns range for possible positions for placing NPC Agents and Controlled Agent
	 * 
	 * @param agentType
	 *            can be NPC or Controlled
	 * @return int Array, dim 5, with minX, maxX, minY, maxY, and estimate for number of possible positions
	 */
	private int[] getPossiblePositions(AgentType agentType) {
		int[] positions = new int[5];

		int minX = 0;
		int maxX = 0;
		int minY = 0;
		int maxY = 0;
		int safeDistance = 3;
		int estimatedPossiblePositions;

		Floor floor = environment.getFloor();
		if (landscape.getRiverFlow() == Landscape.RiverFlow.VERTICAL) {
			//case of environment split in two vertical halves: NPC agents must be on smaller half, controlled Agent on bigger
			minY = safeDistance;
			maxY = floor.getNumY() - safeDistance;
			if ((landscape.getRiverPosition() < floor.getNumX() / 2 && agentType == AgentType.NPC) || (landscape.getRiverPosition() >= floor.getNumX() / 2 && agentType == AgentType.CONTROLLED)) {
				minX = safeDistance;
				maxX = landscape.getRiverPosition();
				estimatedPossiblePositions = landscape.getRiverPosition() * floor.getNumY();
			} else {
				minX = landscape.getRiverPosition();
				maxX = floor.getNumX() - safeDistance;
				estimatedPossiblePositions = (floor.getNumX() - landscape.getRiverPosition()) * floor.getNumY();
			}
		} else {
			//case of environment split in two horizontal halves
			minX = safeDistance;
			maxX = floor.getNumX() - safeDistance;
			if ((landscape.getRiverPosition() < floor.getNumY() / 2 && agentType == AgentType.NPC) || (landscape.getRiverPosition() >= floor.getNumY() / 2 && agentType == AgentType.CONTROLLED)) {
				minY = safeDistance;
				maxY = landscape.getRiverPosition();
				estimatedPossiblePositions = landscape.getRiverPosition() * floor.getNumX();
			} else {
				minY = landscape.getRiverPosition();
				maxY = floor.getNumY() - safeDistance;
				estimatedPossiblePositions = (floor.getNumY() - landscape.getRiverPosition()) * floor.getNumX();
			}
		}
		positions[0] = minX;
		positions[1] = maxX;
		positions[2] = minY;
		positions[3] = maxY;
		positions[4] = estimatedPossiblePositions;

		return positions;
	}

	/**
	 * Creates a Controlled Agent
	 * Environment is split in two parts by the river; agent to be put on bigger half
	 * Controlled agent has mover, closestDiskSensor, floorSensor and PlaceSensor
	 * has EventHandler to handle eating of FoodItems and animals
	 * 
	 * @param aRandom
	 * @param eRandom
	 * @param controlledAgentStates
	 * @return
	 */
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {
		// sensors and actuators

		/*sensorValues[0]=x Position, [1]= y Position, [2]-[4]=RGB floor, [5]=diskdetector,
		* [6]=disk distance, [7]=disk Width, [8]-[10]= RGB disk*/
		Set<DiskMaterial> ignoredMaterials = new HashSet<DiskMaterial>();
		ignoredMaterials.add(DiskMaterial.RUBBER);
		ClosestDiskSensor closestDiskSensor = new ClosestDiskSensor(environment, 0.0, Math.toRadians(200.0), 1.5, 10.0, ignoredMaterials, false, true, true, new RGBColorDiskMaterialResponse());
		FloorSensor floorSensor = new FloorSensor(environment, new RGBColorFloorTypeResponse());
		PlaceSensor placeSensor = new PlaceSensor(environment);
		Actuator actuator = new Mover(1.0, 1.0, 0.1, 0.1, 0.1);

		// create disk types
		DiskType body = new DiskType(DiskMaterial.RUBBER.withColor(Color.YELLOW), actuator, new Sensor[] { placeSensor, floorSensor, closestDiskSensor });

		// now create constructor specifying relative arrangement of disks
		AgentConstructor ac = new AgentConstructor(environment);
		ac.setRoot(1.0, body);

		int[] range = getPossiblePositions(AgentType.CONTROLLED);
		int minX = range[0];
		int maxX = range[1];
		int minY = range[2];
		int maxY = range[3];
		// now repeat creating the agent at a random place
		ControlledAgentData agent = null;
		// random scale factor controlling the size (using the random object for agent)
		double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE, AGENT_MAX_SCALE);
		double initialEnergyLevel = 1.0; // start with full energy
		controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
		boolean agentCreated = false;
		do {
			// random position and orientation (using the random object for environment)
			double posx = ScenarioUtils.uniform(eRandom, minX, maxX);
			double posy = ScenarioUtils.uniform(eRandom, minY, maxY);
			if (environment.getFloorCellTypeAtPosition(posx, posy).equals(HunterGathererScenario.WATER)) {
				continue; //agent shall not be put on water
			}
			double orientation = ScenarioUtils.uniform(eRandom, 0.0, 2.0 * Math.PI);
			agent = ac.createControlledAgent(posx, posy, orientation, scaleFactor);
			if (agent != null) {
				agentCreated = true;
			}
		} while (!agentCreated);

		agent.getDisks().get(0).addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse) {
				if (collidableObject instanceof Wall) {
					System.out.println("wall");
				}
				//handles eating of food items
				else if (collidableObject instanceof Disk &&
						(((Disk) collidableObject).getDiskType().getMaterial().equals(FOOD_MATERIAL_HEALTHY)
						|| ((Disk) collidableObject).getDiskType().getMaterial().equals(FOOD_MATERIAL_POISONOUS))) {
					consumedFoodItem = true;
					numOfFoodItems--;
					radiusOfConsumedFoodItem = ((Disk) collidableObject).getRadius();
					if (((Disk) collidableObject).getDiskType().getMaterial().equals(FOOD_MATERIAL_POISONOUS)) {
						radiusOfConsumedFoodItem *= -1.0;
					}
					environment.getDiskComplexesEnsemble().removeDiskComplex(((Disk) collidableObject).getDiskComplex()); //remove eaten food
				} else if (collidableObject instanceof Disk &&
						((Disk) collidableObject).getDiskType().getMaterial().equals(AnimalConstructor.ANIMAL)) {
					//handles eating of animals
					consumedAnimal = true;
					radiusOfConsumedAnimal = ((Disk) collidableObject).getRadius();
					environment.getDiskComplexesEnsemble().removeDiskComplex(((Disk) collidableObject).getDiskComplex()); //remove eaten animal
				}
			}
		});

		return new ControlledAgentData[] { agent };
	}

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		int initNumOfAnimals = ScenarioUtils.uniform(random, MIN_NUM_ANIMALS, MAX_NUM_ANIMALS);
		AnimalConstructor animalConstructor = new AnimalConstructor(environment);
		return createAnimals(animalConstructor, environment, random, initNumOfAnimals);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics) here)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {
		//decrease harm level a little bit
		double previousHarmLevel = agentStates[0].getPreviousHarmLevel();
		agentStates[0].setHarmHealed(previousHarmLevel * 0.001);

		//not moving also costs energy...
		//agentState[0].incEnergyConsumption(0.00001);

		//if agent ate food or animal, set harm (poisonous food) or increase energy (healthy food / animal)
		if (consumedFoodItem) {
			if (radiusOfConsumedFoodItem < 0) {
				agentStates[0].incHarmInflicted(-radiusOfConsumedFoodItem / FOODITEM_ENERGY_PER_RADIUS);
			}
			agentStates[0].incEnergyGained(radiusOfConsumedFoodItem / FOODITEM_ENERGY_PER_RADIUS); //decrease or increase of energy level
			consumedFoodItem = false;
		}
		if (consumedAnimal) {
			agentStates[0].incEnergyGained(radiusOfConsumedAnimal / ANIMAL_ENERGY_PER_RADIUS); //increase of energy level
			consumedAnimal = false;
		}

		//creates new food if there is little
		if (numOfFoodItems < MIN_NUM_FOODITEMS) {
			float p = 1.0f - ((float) numOfFoodItems / 10.0f); //number of foodItems to be created depends on how many foodItems are already there
			int numFood = ScenarioUtils.uniform(random, 1, 1 + (int) p * 5);
			ObjectConstructor goodFoodConstructor = getFoodConstructor(FOOD_MATERIAL_HEALTHY);
			int numGoodFood = (int) (numFood * RATE_OF_HEALTHY_FOODITEMS);
			createFoodItems(goodFoodConstructor, environment, random, numGoodFood);
			ObjectConstructor badFoodConstructor = getFoodConstructor(FOOD_MATERIAL_POISONOUS);
			createFoodItems(badFoodConstructor, environment, random, numFood - numGoodFood);
		}

		//healthy foodItems grow
		for (DiskComplex dc : environment.getDiskComplexes()) {
			for (Disk disk : dc.getDisks()) {
				if (disk != null && disk.getDiskType().getMaterial().equals(FOOD_MATERIAL_HEALTHY) && disk.getRadius() < FOODITEM_MAX_SIZE) {
					disk.getDiskComplex().performModification(new DiskModification(disk, disk.getRadius() * 1.001));
				}
			}
		}

		double[] sensorValues = getSensorValues(0);

		//if the agent is in water, harm is increased; if he is on the bridge, harm is healed
		double posX = sensorValues[0] * environment.getFloor().getNumX();
		double posY = sensorValues[1] * environment.getFloor().getNumY();
		FloorCellType floorCellTypeAtPosition = environment.getFloorCellTypeAtPosition(posX, posY);
		if (floorCellTypeAtPosition.getTypeIndex() == HunterGathererScenario.WATER.getTypeIndex()) {
			agentStates[0].incHarmInflicted(0.001);
		}
		if (floorCellTypeAtPosition.getTypeIndex() == FloorCellType.STONE.getTypeIndex()) {
			agentStates[0].setHarmHealed(agentStates[0].getHarmLevel() * 0.01);
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provides the time dependent hardness level. 											 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected double getHardnessLevel(double time) {
		return 0.0; // we are merciful in the demo scenario, death is not possible
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		return 0.0; // no score yet
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but             */
	/* dedicated to this scenario are implemented here          							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return ExampleReferenceController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI         				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {

			always(0, 0.0);
			always(1, 0.0);
			onKey('W', 0, 1.0, false);
			onKey('S', 0, -1.0, false);
			onKey('D', 1, -1.0, false);
			onKey('A', 1, 1.0, false);

			onKey('0', 2, 0.0, false);
			onKey('0', 3, 0.0, false);
			setLogMessage("Use the following keys to control the agent:\n" +
					"w -> move forward\n" +
					"s -> move backward\n" +
					"d -> turn right \n" +
					"a -> turn left\n");
		}

	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String[] args) {
		boolean useReferenceController = true;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(HunterGathererScenario.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
