package ccc.scenario.hunterGatherer;

import diskworld.interfaces.AgentController;

import java.util.Random;

/**
 * Controller for Animal NPC Agents
 * Animals avoid water and bridge and jump away from Controlled Agent
 * Sensor values: sensor[0]=diskdetector, sensor[1]=distance, sensor[2]-[4] RGB disk, sensor[5]-[7] RGB floor
 * @author Svenja Melbaum
 */
public class AnimalController implements AgentController {

    private static final int NUM_STEPS_WITHOUT_ROTATION = 5; // animals shall turn every 5 timesteps
    private int lastWaterStepsAgo = -1; //memorizes how many timesteps ago the last contact to water happened
    private int noTurnCounter = NUM_STEPS_WITHOUT_ROTATION;
    private Random random;

    public AnimalController(Random random) {
        this.random = random;
    }

    @Override
    public void doTimeStep(double[] sensorValues, double[] actuatorValues) {
        if (lastWaterStepsAgo != -1) {
            lastWaterStepsAgo++;
        }

        //IMPORTANT: setting actuator values of animals leads to energylevel bug for controlled agent
        //energylevel of controlled agent does not change any more

        noTurnCounter--;
        actuatorValues[0] = 0.05;

        if (lastWaterStepsAgo == -1 || lastWaterStepsAgo > 20) { //when last contact with water is some timesteps ago...
            if ((sensorValues[5] == 0 && sensorValues[6] == 0 && sensorValues[7] == 1) || (sensorValues[5] == 64 / 255 && sensorValues[6] == 64 / 255 && sensorValues[7] == 64 / 255)) {
                //change movement when on water / bridge
                lastWaterStepsAgo = 0;
                actuatorValues[0] = -1.0;
                actuatorValues[1] = Math.PI;
            } else if (noTurnCounter <= 0) {
                //else turn randomly
                actuatorValues[1] = random.nextDouble() - 0.5;
                noTurnCounter = NUM_STEPS_WITHOUT_ROTATION;
            }
        } else {
            //last contact with water / bridge is not long ago: keep speed, change direction a little bit
            actuatorValues[1] = random.nextDouble() - 0.5;
        }

        //when controlled agent is seen, jump away
        if (sensorValues[2] == 1 && sensorValues[3] == 1 && sensorValues[4] == 0) {
            actuatorValues[0] = -2.0;
        }

    }
}
