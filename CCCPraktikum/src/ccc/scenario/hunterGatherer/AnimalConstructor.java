package ccc.scenario.hunterGatherer;

import ccc.DiskWorldScenario.AgentConstructor;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Mover;
import diskworld.interfaces.Sensor;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.sensors.FloorSensor;
import diskworld.sensors.RGBColorDiskMaterialResponse;
import diskworld.sensors.RGBColorFloorTypeResponse;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Constructor for Animal NPC Agents with Mover, ClosestDiskSensor and FloorSensor
 * Sensor values: sensor[0]=diskdetector, sensor[1]=distance, sensor[2]-[4] RGB disk, sensor[5]-[7] RGB floor
 * @author Svenja Melbaum
 */
public class AnimalConstructor extends AgentConstructor {

    private static final double MAX_FORWARD_SPEED = 1.0;
    private static final double MAX_BACKWARD_SPEED = 5;
    private static final double MAX_ROTATION_SPEED = 1.0;
    private static final double ENERGY_CONSUMPTION = 0;

    static final DiskMaterial ANIMAL = new DiskMaterial(1.0, 0.2, 0.2, 0.2, Color.BLACK);

    public AnimalConstructor(Environment env) {
        super(env);
        Mover mover = new Mover(MAX_BACKWARD_SPEED, MAX_FORWARD_SPEED, MAX_ROTATION_SPEED, ENERGY_CONSUMPTION, ENERGY_CONSUMPTION);
        Set<DiskMaterial> ignoredMaterials = new HashSet<DiskMaterial>();
        ignoredMaterials.add(ANIMAL);
        Sensor sensor = new ClosestDiskSensor(env, 0.0, Math.toRadians(90.0), 1.5, 3.0, ignoredMaterials, false, true, false, new RGBColorDiskMaterialResponse());
        FloorSensor floorSensor = new FloorSensor(env, new RGBColorFloorTypeResponse());
        DiskType body = new DiskType(ANIMAL, mover, new Sensor[]{sensor, floorSensor});
        this.setRoot(0.5, body);
    }
}
