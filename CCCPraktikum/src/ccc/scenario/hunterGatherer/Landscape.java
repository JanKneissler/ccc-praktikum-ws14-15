package ccc.scenario.hunterGatherer;

import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.Environment;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;

import java.util.Random;

/**
 * Class to create a Landscape with river and lake
 * environment is split in an imaginary vertical or horizontal border
 * lake is one side of the border, river on the other
 * @author Svenja Melbaum
 */
class Landscape {

    enum RiverFlow {
        HORIZONTAL, VERTICAL
    }

    private static final int SAFE_DISTANCE = 3; //keep distance to boundary walls

    private Environment environment;
    private Random random;
    private RiverFlow riverFlow;
    private int riverPosition; // x Position of vertical river or y position of horizontal river
    private int splitBorder; //x Position of vertical border or y position of horizontal border

    private double minLakePropSize; //minimal size of Lake in relation to environment size
    private double maxLakePropSize;
    private int maxRiverWidth;

    Landscape(Environment environment, Random random) {
        this(environment, random, 0.1, 0.35, ScenarioUtils.uniform(random, 2, 13));
    }

    Landscape(Environment environment, Random random, double minLakePropSize, double maxLakePropSize, int maxRiverWidth) {
        this.minLakePropSize = minLakePropSize;
        this.maxLakePropSize = maxLakePropSize;
        this.environment = environment;
        this.random = random;
        //this.maxRiverWidth = maxRiverWidth < SAFE_DISTANCE * 2 ? maxRiverWidth : SAFE_DISTANCE * 2;
        this.maxRiverWidth = maxRiverWidth;
        this.riverFlow = random.nextBoolean() ? RiverFlow.HORIZONTAL : RiverFlow.VERTICAL; //random riverflow in horizontal or vertical direction

        final Floor floor = environment.getFloor();
        final int half = riverFlow == RiverFlow.HORIZONTAL ? floor.getNumY() / 2 : floor.getNumX() / 2;
        final double variation = (double) half * 2.0 * 0.01;
        this.splitBorder = ScenarioUtils.uniform(random, (int) (half - variation), (int) (half + variation));

        if (random.nextBoolean()) {
            this.riverPosition = ScenarioUtils.uniform(random, splitBorder, half * 2 - maxRiverWidth*2);
        } else {
            this.riverPosition = ScenarioUtils.uniform(random, maxRiverWidth*2, splitBorder);
        }

        createRiver();
        createLake();
    }

    public RiverFlow getRiverFlow() {
        return riverFlow;
    }

    public int getRiverPosition() {
        return riverPosition;
    }

    protected void createLake() {
        final Floor floor = environment.getFloor();

        //determine range for possible positions
        int lakePositionXMin;
        int lakePositionXMax;
        int lakePositionYMin;
        int lakePositionYMax;
        if (riverFlow == RiverFlow.VERTICAL) { // case of vertical river
            lakePositionYMin = SAFE_DISTANCE;
            lakePositionYMax = floor.getNumY() - SAFE_DISTANCE;
            if (riverPosition > splitBorder) {
                //create lake on left half
                lakePositionXMin = SAFE_DISTANCE;
                lakePositionXMax = splitBorder;
            } else {
                //create lake on right half
                lakePositionXMin = splitBorder;
                lakePositionXMax = floor.getNumX() - SAFE_DISTANCE;
            }

        } else { // case of horizontal river
            lakePositionXMin = SAFE_DISTANCE;
            lakePositionXMax = floor.getNumX() - SAFE_DISTANCE;
            if (riverPosition < splitBorder) {
                //create lake on top half
                lakePositionYMin = splitBorder;
                lakePositionYMax = floor.getNumY() - SAFE_DISTANCE;
            } else {
                //create lake on bottom half
                lakePositionYMin = SAFE_DISTANCE;
                lakePositionYMax = splitBorder;
            }
        }


        //create 2 lake coordinates; make sure it isn't too small or too big
        int lakeX1;
        int lakeX2;
        int lakeY1;
        int lakeY2;
        int sizeLakeYDirection;
        int sizeLakeXDirection;
        do {
            lakeX1 = ScenarioUtils.uniform(random, lakePositionXMin, lakePositionXMax);
            lakeY1 = ScenarioUtils.uniform(random, lakePositionYMin, lakePositionYMax);
            lakeX2 = ScenarioUtils.uniform(random, lakeX1, lakePositionXMax);
            lakeY2 = ScenarioUtils.uniform(random, lakeY1, lakePositionYMax);
            sizeLakeXDirection = lakeX2 - lakeX1;
            sizeLakeYDirection = lakeY2 - lakeY1;
        }
        while (sizeLakeXDirection < floor.getNumX() * minLakePropSize || sizeLakeYDirection < floor.getNumY() * minLakePropSize
                || sizeLakeXDirection > floor.getNumX() * maxLakePropSize || sizeLakeYDirection > floor.getNumY() * maxLakePropSize);


        //create lake around the two coordinates
        for (int x = lakeX1; x < lakeX2; x++) {
            for (int y = lakeY1 + ScenarioUtils.uniform(random, 1, SAFE_DISTANCE); y < lakeY2 - ScenarioUtils.uniform(random, 1, SAFE_DISTANCE); y++) {
                floor.setType(x, y, HunterGathererScenario.WATER);
            }
        }
    }


    /**
     * creates a river in vertical (case riverFlow == vertical) or horizontal direction (case riverFlow == horizontal)
     * a bridge is also built
     */
    protected void createRiver() {
        Floor floor = environment.getFloor();
        int limit = riverFlow == RiverFlow.HORIZONTAL ? floor.getNumX() : floor.getNumY();
        for (int i = 0; i < limit; i++) {
            for (int j = riverPosition - ScenarioUtils.uniform(random, 1 + maxRiverWidth / 4, maxRiverWidth / 2);
                 j < riverPosition + ScenarioUtils.uniform(random, 1 + maxRiverWidth / 4, maxRiverWidth / 2); j++) {
                if (riverFlow == RiverFlow.HORIZONTAL)
                    floor.setType(i, j, HunterGathererScenario.WATER);
                else
                    floor.setType(j, i, HunterGathererScenario.WATER);
            }
        }

        //create bridge
        int bridgePos = ScenarioUtils.uniform(random, SAFE_DISTANCE * 2, limit - SAFE_DISTANCE * 2);
        for (int i = bridgePos - ScenarioUtils.uniform(random, 1, SAFE_DISTANCE); i < bridgePos + ScenarioUtils.uniform(random, 1, SAFE_DISTANCE); i++) {
            for (int j = riverPosition - (maxRiverWidth / 2 + 1); j < riverPosition + maxRiverWidth / 2 + 1; j++) {
                if (riverFlow == RiverFlow.HORIZONTAL)
                    floor.setType(i, j, FloorCellType.STONE);
                else
                    floor.setType(j, i, FloorCellType.STONE);

            }
        }
    }


}
