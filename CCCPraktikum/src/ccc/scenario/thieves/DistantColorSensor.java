package ccc.scenario.thieves;

import java.awt.Color;

import diskworld.Environment;
import diskworld.environment.Floor;
import diskworld.linalg2D.Utils;
import diskworld.sensors.ConeSensor;

public class DistantColorSensor extends ConeSensor {

	private double maxRange;
	private double openingAngle;
	private Floor floor;

	public DistantColorSensor(Environment environment, double centerAngle,
			double openingAngle, double minRangeRelativeToRadius,
			double maxRangeAbsolute) {
		super(environment, centerAngle, openingAngle, minRangeRelativeToRadius,
				maxRangeAbsolute, 0, "?");

		this.maxRange = maxRangeAbsolute;
		this.openingAngle = openingAngle;
		this.floor = environment.getFloor();
	}

	@Override
	public int getDimension() {
		return 6;
	}

	@Override
	public double getRealWorldInterpretation(double[] measurement, int index) {
		if (index == 1) {
			double range[] = getShape().referenceValues();
			return range[0] + measurement[index] * (range[1] - range[0]);
		}
		if (index == 2) {
			return measurement[index] * openingAngle / 2;
		}
		return measurement[index];
	}

	@Override
	public String getRealWorldMeaning(int index) {
		if(index == 0) {
			return "Differently colored ground found? [bool]";
		} else if (index == 1) {
			return "Distance to colored ground";
		} else if(index == 2) {
			return "Angle to colored ground";
		}
		String color = index == 3? "Red " : (index == 4? "Green " : "Blue ");
		return color + "part of the ground's color";
	}

	@Override
	protected void doMeasurement(double[] values) {

		double centerx = getDisk().getX();
		double centery = getDisk().getY();
		double direction = getDisk().getAngle();
		Color colorUnderDisk = floor.getType(centerx, centery)
				.getDisplayColor();
		
		// set all measurements to 0
		for (int i = 0; i < values.length; i++) {
			values[i] = 0;
		}

		//can't believe I'm doing this.. will change it when I find the time
		//search the shortest distance to a colored ground that has a different 
		//color than the ground underneath the disk
		distloop: for (int d = 1; d <= maxRange; d++) {
			//divide the sensor's range in 8 parts
			int i = -4;
			double angle = i * (openingAngle / 8);
			
			//loop over the 8 parts
			do {
				double x = Math.cos(angle) * d;
				double y = Math.sin(angle) * d;

				// transform the coordinates from the canonical shape based x and 
				// y values to the position and direction of the disk
				double transformedX = Math.cos(direction) * x
						- Math.sin(direction) * y + centerx;
				double transformedY = Math.sin(direction) * x
						+ Math.cos(direction) * y + centery;

				Color color = floor.getType(transformedX, transformedY)
						.getDisplayColor();

				// if the color is different than the color underneath the disk
				if (color != colorUnderDisk) {

					// color difference found
					values[0] = 1;

					// set distance and angle to closest floorType with
					// differing color
					double range[] = getShape().referenceValues();
					values[1] = Utils.clip_01((d - range[0])
							/ (range[1] - range[0]));

					values[2] = Utils.clip_pm1(2*angle / openingAngle);

					// set color values (r,g,b)
					values[3] = color.getRed() / 255.d;
					values[4] = color.getGreen() / 255.d;
					values[5] = color.getBlue() / 255.d;

					break distloop;
				}
				i++;
				angle = i * (openingAngle / 8);
				

			} while (i <= 4);
		}

	}

}
