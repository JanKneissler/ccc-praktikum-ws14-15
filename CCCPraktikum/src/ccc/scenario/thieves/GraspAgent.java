package ccc.scenario.thieves;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actions.JointActionType.ControlType;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.Sensor;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.sensors.CompassSensor;
import diskworld.sensors.WallSensor;

/**
 * AgentConstructor to construct agents with 2 arms
 * The arms can be opened or closed with joint action
 * Agents have a mover and three sensors:
 * ColoredGroundSensor to detect the color of the ground beneath them (rgb)
 * WallSensor to detect walls
 * ClosestDiskSensor that ignores the arms and detects the enemies (max. 2)
 */
public class GraspAgent extends AgentConstructor {
	
	// speed parameters
	private final double MAX_DISPLACEMENT_SPEED = 1.0;
	private final double MAX_ROTATION_SPEED = 2*Math.PI;
	private final double JOINT_ROTATION_SPEED = 2;
	
	// size parameters
	private final double ROOT_RADIUS = 1.7;
	private final double ARM_RADIUS_MIN = 0.5;
	private final double ARM_RADIUS_MAX = 0.7;
	private final double FACTOR_MIN = 0.85;
	private final double FACTOR_MAX = 0.90;
	
	// params to define the arm
	private final int NUM_SEGMENTS = 5;	// max. 11
	private final int NUM_ARMS = 2;		// must be 2 or code has to be altered
	
	// opening angle of the sensors
	private final double SENSOR_RADIUS = 0.5*Math.PI;
	private final double SENSOR_RANGE = 30;

	Actuator mover;
	ColoredGroundSensor groundSensor;
	WallSensor wallSensor;
	ClosestDiskSensor diskSensor;
	DiskMaterial agentMaterial;
	DistantColorSensor cs;
	CompassSensor compass;


	public GraspAgent(Environment env, DiskMaterial agentMaterial, DiskMaterial enemyMaterial, Random aRandom) {
		super(env);
		
		this.agentMaterial = agentMaterial;
		Set<DiskMaterial> ignoredMaterials = new HashSet<DiskMaterial>();
		ignoredMaterials.add(agentMaterial);
		
		// initialize Mover and Sensors
		// backward speed is 0.5 slower
		mover = new Mover(MAX_DISPLACEMENT_SPEED -0.5, MAX_DISPLACEMENT_SPEED, MAX_ROTATION_SPEED, 0.001, 0.001);
		groundSensor = new ColoredGroundSensor(env);
		diskSensor = new ClosestDiskSensor(env, 0, SENSOR_RADIUS, 1.5, SENSOR_RANGE, 
				ignoredMaterials, true, true, false, null, 2);
		wallSensor = new WallSensor(env, 0, SENSOR_RADIUS, 1.5, SENSOR_RANGE, "wallSensor");
		cs = new DistantColorSensor(env, 0, SENSOR_RADIUS, 1.5, SENSOR_RANGE);
		compass = new CompassSensor(env);

		// diskTypes: root gets sensors and mover, shoulders get a joint action
		DiskType root = new DiskType(agentMaterial, mover, new Sensor[]{diskSensor,wallSensor,groundSensor, cs, compass});
		DiskType shoulder = new DiskType(agentMaterial);
		DiskType arm = new DiskType(agentMaterial);
		shoulder.addJointAction(JOINT_ROTATION_SPEED, ControlType.CHANGE, ActionType.SPIN);

		this.setRoot(ROOT_RADIUS, true, root);

		// decide sizes
		double armradius = ScenarioUtils.uniform(aRandom, ARM_RADIUS_MIN, ARM_RADIUS_MAX); // radius of the shoulders
		double factor = ScenarioUtils.uniform(aRandom, FACTOR_MIN, FACTOR_MAX);

		// add arms
		for (int i = 0; i < NUM_ARMS; i++) {
			double radius = armradius;
			if(i == 0) {
				this.addItem(0, Math.toRadians(30), 0, radius, shoulder);
			}
			else {
				this.addItem(0, -Math.toRadians(30), 0, radius, shoulder);
			}
			// next segment is always a bit smaller
			radius *= factor;
			for (int j = 0; j < NUM_SEGMENTS; j++) {
				if (i == 0) {
					this.addItem(-1, Math.toRadians((1 - 2 * j) * 4), 0, radius, arm);
				} else {
					this.addItem(-1, -Math.toRadians((1 - 2 * j) * 4), 0, radius, arm);
				}

				radius *= factor;
			}
		}
	}


}
