package ccc.scenario.thieves;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actions.JointActionType.ControlType;

/**
 * constructs octopus-like NPCs
 * every segment has a joint action, so its pretty slow but
 * works when the number of segments is not too high
 */
public class Octopus extends AgentConstructor {
	
	// size parameters
	private final double ROOT_RADIUS = 3;
	private final double ARM_RADIUS_MIN = 1.2;
	private final double ARM_RADIUS_MAX = 2;

	// number of segments for the arms
	// if too high, performance suffers
	private int NUM_SEGMENTS = 3;
	private int NUM_ARMS = 3;
	
	// movement parameters
	private double JOINT_ROTATION_SPEED = 2;
	private double MAX_SPIN_ANGLE = Math.toRadians(30);

	/**
	 * constructor for the octopus creator
	 * @param env: environment
	 * @param octopusMaterial: material for the NPC
	 * @param aRandom
	 */
	public Octopus(Environment env, DiskMaterial octopusMaterial, Random aRandom) {
		super(env);

		DiskType root = new DiskType(octopusMaterial);
		DiskType shoulder = new DiskType(octopusMaterial);
		DiskType arm = new DiskType(octopusMaterial);
		
		shoulder.addJointAction(JOINT_ROTATION_SPEED , ControlType.CHANGE, ActionType.SLIDE);
		arm.addJointAction(JOINT_ROTATION_SPEED, ControlType.CHANGE, ActionType.SPIN);

		this.setRoot(ROOT_RADIUS, true, root);

		// decide sizes
		double armradius = ScenarioUtils.uniform(aRandom, ARM_RADIUS_MIN, ARM_RADIUS_MAX);
		double factor = 0.9;
		
		// add arms
		for(int i = 0; i < NUM_ARMS ; i++) {
			double radius = armradius;
			this.addItem(0,Math.PI/NUM_ARMS*i-Math.PI/3, 0, radius, shoulder);
			for(int j = 0; j < NUM_SEGMENTS ; j++) {
				radius*=factor;
				int index = this.addItem(-1,0, 0, radius, arm);
				this.setActionRange(index, ActionType.SPIN, -MAX_SPIN_ANGLE , MAX_SPIN_ANGLE);
			}
		}
	}

}
