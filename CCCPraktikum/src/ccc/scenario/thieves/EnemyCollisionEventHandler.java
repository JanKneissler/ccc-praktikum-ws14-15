package ccc.scenario.thieves;

import java.util.ArrayList;

import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.Environment;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;

/**
 * Collision handler for the agent
 * Handles its collision with other disks
 * If the agent collides with a bomb, the bomb is destroyed and the agent takes harm
 * If the agent collides with the "octopi" arms, it just gets hurt
 */
public class EnemyCollisionEventHandler implements CollisionEventHandler {
	protected AgentState agentState;
	protected Environment environment;
	protected DiskMaterial bombMaterial;
	private DiskMaterial octopusMaterial;
	
	private DiskComplex agentComplex;
	
	// currently ignored diskComplexes
	private ArrayList<DiskComplex> ignoredDisks = new ArrayList<DiskComplex>();
	
	// harm that will be inflicted if agent collides with enemy
	final double HARM = 0.07;
	
	public EnemyCollisionEventHandler(AgentState agentState, Environment environment, DiskComplex agentComplex,
			DiskMaterial bombMaterial, DiskMaterial octopusMaterial) {
		this.agentState = agentState;
		this.environment = environment;
		this.bombMaterial = bombMaterial;
		this.octopusMaterial = octopusMaterial;
		this.agentComplex = agentComplex;
	}

	@Override
	public void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse) {
		if (collidableObject instanceof Disk) {
			
			Disk disk = (Disk) collidableObject;
			
			if(!ignoredDisks.contains(disk.getDiskComplex())) {
				
				// if collided disk is a bomb-NPC
				if (disk.getDiskType().getMaterial() == bombMaterial) {
	
					agentState.incHarmInflicted(HARM);
					
					// destroy bomb
					environment.getDiskComplexesEnsemble().removeDiskComplex(disk.getDiskComplex());
				
				} else if (disk.getDiskType().getMaterial() == octopusMaterial) {
					
					agentState.incHarmInflicted(HARM/2);
					
					// collisions with this specific enemy will be ignored for 3 seconds
					ignoreCollision(disk, 3000);
				}
			} else {
				// if diskComplex is currently ignored, teleport the agent a few steps backwards
			    double dx = -0.5*agentComplex.getSpeedx();
			    double dy = -0.5*agentComplex.getSpeedy();
			    double offset = 4;	// so that agent doesn't get stuck in the walls. I found 4 to work well
			    // if it is possible, teleport the agent backwards
			    if( this.agentComplex.getCenterx() + dx < environment.getMaxX() - offset &&
			    	this.agentComplex.getCentery() + dy < environment.getMaxY() - offset &&
			    	this.agentComplex.getCenterx() + dx > offset   					     &&
			    	this.agentComplex.getCentery() + dy > offset) {
			    		agentComplex.teleport(dx, dy, 0);
			    } else {
			    	// else teleport him back to the start
			    	environment.canTeleportCenterOfMass(agentComplex, environment.getMaxX() / 2, 5, 0);
			    }
			}
		}
	}
	
	/**
	 * ignore collisions with given diskComplex
	 * @param disk: this disk's diskColmplex will be ignored
	 * @param milliSeconds: collisions will be possible after this time
	 */
	private void ignoreCollision(final Disk disk, int milliSeconds) {
		// add diskComplex to the arrayList
		ignoredDisks.add(disk.getDiskComplex());	
		
		// after specified time, remove diskComplex from the arrayList
		new java.util.Timer().schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
		                ignoredDisks.remove(disk.getDiskComplex());
		            }
		        }, 
		        milliSeconds 
		);
	}
}