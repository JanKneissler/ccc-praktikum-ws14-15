package ccc.scenario.thieves;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.Sensor;
import diskworld.sensors.ClosestDiskSensor;


/**
 * Constructor for "bomb"-like NPC-agents
 * They have a mover and a ClosestDiskSensor that detects only the agent
 */
public class EnemyAgent extends AgentConstructor {
	
	// speed parameters
	private final double MAX_DISPLACEMENT_SPEED = 1.0;
	private final double MAX_ROTATION_SPEED = 2*Math.PI;
	
	// size parameters
	private final double ROOT_RADIUS = 1.0;
	private final double ARM_RADIUS_MIN = 0.4;
	private final double ARM_RADIUS_MAX = 0.6;
	
	// opening angle of the sensor
	private final double SENSOR_RADIUS = 2*Math.PI;

	Actuator mover;
	ClosestDiskSensor diskSensor;
	DiskMaterial agentMaterial;


	/**
	 * @param env: environment
	 * @param enemyMaterial: the enemy material (so the material that will be used for this NPC)
	 * @param agentMaterial: the agent's material (will be detected by the sensor)
	 * @param ballMaterial: the ball's material (will be ignored)
	 * @param octopusMaterial: will be ignored
	 * @param aRandom
	 */
	public EnemyAgent(Environment env, DiskMaterial enemyMaterial, DiskMaterial agentMaterial, DiskMaterial ballMaterial, DiskMaterial octopusMaterial, Random aRandom) {
		super(env);
		
		// ignore enemy material, octopus material and the ball material
		Set<DiskMaterial> ignoredMaterials = new HashSet<DiskMaterial>();
		ignoredMaterials.add(ballMaterial);
		ignoredMaterials.add(enemyMaterial);
		ignoredMaterials.add(octopusMaterial);
		
		// initialize mover and diskSensor
		mover = new Mover(MAX_DISPLACEMENT_SPEED -0.5, MAX_DISPLACEMENT_SPEED, MAX_ROTATION_SPEED, 0.1, 0.1);
		diskSensor = new ClosestDiskSensor(env, 0, SENSOR_RADIUS, 
				1.5, 20, ignoredMaterials, true, true, false, null);
		
		// diskTypes: root gets mover and sensor
		DiskType bombRoot = new DiskType(enemyMaterial, mover, new Sensor[]{diskSensor});
		DiskType bombThorns = new DiskType(enemyMaterial);

		this.setRoot(ROOT_RADIUS, true, bombRoot);

		// decide sizes
		double armradius = ScenarioUtils.uniform(aRandom, ARM_RADIUS_MIN, ARM_RADIUS_MAX);
		
		// add "arms" (they just look cool and have no function :) )
		for(int i = 0; i < 6; i++) {
			this.addItem(0,Math.toRadians(60*i), 0, armradius, bombThorns);
			this.addItem(-1,0, 0, armradius/2, bombThorns);
		}
	}


}
