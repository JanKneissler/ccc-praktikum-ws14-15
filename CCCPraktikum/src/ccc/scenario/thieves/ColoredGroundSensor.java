package ccc.scenario.thieves;

import java.awt.Color;

import diskworld.Disk;
import diskworld.Environment;
import diskworld.environment.Floor;
import diskworld.interfaces.Sensor;
import diskworld.visualization.VisualisationItem;

/**
 * Measures the color on the ground underneath the disk to which
 * the sensor is attached
 * 
 * @author paulina
 * 
 */
public class ColoredGroundSensor implements Sensor {

	Floor floor;

	//default constructor
	public ColoredGroundSensor(Environment environment) {
		environment.getMaxX();
		environment.getMaxY();

		this.floor = environment.getFloor();
	}

	@Override
	public int getDimension() {
		return 3;
	}

	@Override
	/**
	 * @see diskworld.interfaces.Sensor#getMeasurement(diskworld.diskcomplexes.Disk, double[])
	 * double[] color is interpreted as Array of color-values (r,g,b) in the range [0,1]
	 */
	public void doMeasurement(Disk disk, double color[]) {
		double xPos = disk.getX();
		double yPos = disk.getY();

		//get Color at disk's position and convert it to double in range [0,1]
		Color posColor = floor.getType(xPos, yPos).getDisplayColor();
		color[0] = (double) posColor.getRed() / 255.d;
		color[1] = (double) posColor.getGreen() / 255.d;
		color[2] = (double) posColor.getBlue() / 255.d;
	}

	@Override
	public VisualisationItem getVisualisationItem() {
		return null;
	}

	@Override
	public double getRealWorldInterpretation(double[] measurement, int index) {
		return measurement[index] * (255);
	}

	@Override
	public String getRealWorldMeaning(int index) {
		String meaning = "";
		switch (index) {
		case 0:
			meaning += "red ";
			break;
		case 1:
			meaning += "green ";
			break;
		case 2:
			meaning += "blue ";
			break;
		}
		return meaning += "value on floor at disk's position";
	}

}