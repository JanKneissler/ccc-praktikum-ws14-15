package ccc.scenario.thieves;

import java.awt.Color;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import ccc.scenarioTools.optimization.ParametrizedObject;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.linalg2D.Line;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class ThiefScenario extends DiskWorldScenario {

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Constants */
	// /////////////////////////////////////////////////////////////////////////////////////////

	// Minimum and maximum sizes of the environment
	private static final int ENV_SIZE_X = 100;
	private static final int ENV_SIZE_Y = 100;

	// maximum simulation time
	private static final double MAX_SIMULATION_TIME = 1000.0; // Maximum
																// duration of a
																// simulation

	protected static double INITIAL_ENERGY_LEVEL = 1000;
	protected static double MAX_ENERGY_CONSUMPTION_FACTOR = 0.001; // Energy
																	// consumption
																	// factor
																	// (per
																	// displacement
																	// resistance)
																	// at
																	// maximum
																	// hardness
																	// level
	protected static double METABOLIC_ENERGY_CONSUMPTION_PER_MASS_AND_TIME_UNIT = 0.0005; // how
																							// much
																							// energy
																							// consumed
																							// by
																							// metabolism
																							// (depends
																							// on
																							// the
																							// agents
																							// mass)

	// size range of the agent
	private static final double AGENT_MIN_SCALE = 0.9;
	private static final double AGENT_MAX_SCALE = 1.3;
	private static final double BALL_MIN_SIZE = 0.8;
	private static final double BALL_MAX_SIZE = 1.0;

	// metabolic energy consumption

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Fields */
	// /////////////////////////////////////////////////////////////////////////////////////////

	protected Environment environment;
	protected Random eRandom;
	private double time;
	public static double hardness;
	private DiskComplex agentDiskComplex;
	private ArrayList<DiskComplex> ballsOnOutside = new ArrayList<DiskComplex>(); // all
																					// balls
																					// that
																					// are
																					// outside
																					// of
																					// the
																					// home
	private ArrayList<DiskComplex> ballsOnInside = new ArrayList<DiskComplex>(); // all
																					// balls
																					// inside
																					// the
																					// home

	private double agentMass;
	private DiskMaterial agentMaterial = DiskMaterial.METAL
			.withColor(Color.BLUE);
	private DiskMaterial enemyMaterial = DiskMaterial.METAL
			.withColor(Color.RED);
	private DiskMaterial octopusMaterial = enemyMaterial
			.withColor(Color.ORANGE);
	private DiskMaterial ballMaterial = new DiskMaterial(5.0, 0.3, 5.0, 5.0,
			Color.magenta);

	private double[][] sinTable; // due to performance problems, actuator values
									// of the octopus arms are precalculated

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "Thief";
	}

	@Override
	public String getDescription() {
		return "Steal the balls on top, get them in your home and beware of the bombfish and octopi!";
	}

	@Override
	public String getAuthors() {
		return "Paulina Friemann";
	}

	@Override
	public String getContactEmail() {
		return "paulina-malin-jantine.friemann@student.uni-tuebingen.de";
	}

	@Override
	public String getVersion() {
		return "2.0";
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return new EnvironmentParameters(ENV_SIZE_X, ENV_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex,
			double sizey) {
		// add bounding walls
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);

		// add walls for the home
		res.add(new Wall(
				new Line(2 * ENV_SIZE_X / 7, 0, 2 * ENV_SIZE_X / 7, 15), 1));
		res.add(new Wall(
				new Line(5 * ENV_SIZE_X / 7, 0, 5 * ENV_SIZE_X / 7, 15), 1));
		res.add(new Wall(new Line(2 * ENV_SIZE_X / 7, 15, 2.5 * ENV_SIZE_X / 7,
				15), 1));
		res.add(new Wall(new Line(4.5 * ENV_SIZE_X / 7, 15, 5 * ENV_SIZE_X / 7,
				15), 1));

		return res;
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		// this scenario has no limitation of number of time steps, set a value
		// > 0 to enable time limitation
		return (int) Math.floor(MAX_SIMULATION_TIME
				/ DiskWorldScenario.DEFAULT_DT);
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid
		// red...
		res.getColorScheme().gridColor = Color.RED;
		// enable/disable options if needed: disable skeleton display...
		res.getOptions()
				.getOption(VisualizationOptions.GROUP_GENERAL,
						VisualizationOptions.OPTION_SKELETON).setEnabled(true);
		res.getOptions()
				.getOption(VisualizationOptions.GROUP_GENERAL,
						VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Initialize the environment, create controlled and npc agents */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		// store environment for later use
		this.environment = environment;
		this.eRandom = random;

		// fill ground with a color
		FloorCellType floorType = new FloorCellType(
				FloorCellType.WATER.getFrictionCoefficient(), Color.cyan);
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, floorType);
			}
		}

		// fill the goals' ground with a color
		FloorCellType goalFloorType = new FloorCellType(
				FloorCellType.WATER.getFrictionCoefficient(), Color.magenta);
		for (int x = (int) (2 * environment.getMaxX() / 7); x < 5 * environment
				.getMaxX() / 7; x++) {
			for (int y = (int) (environment.getMaxY() - 13); y < environment
					.getMaxY(); y++) {

				floor.setType(x, y, goalFloorType);
			}
		}

		// fill homes' ground with color
		for (int x = (int) (2 * environment.getMaxX() / 7); x < 5 * environment
				.getMaxX() / 7; x++) {
			for (int y = 0; y < 15; y++) {

				floor.setType(x, y, FloorCellType.ICE);
			}
		}

		createNewBall();

		initializeSinTable();
	};

	private void initializeSinTable() {
		// arm can have max. 8 segments!
		sinTable = new double[27][40];

		double PERIOD = 40;
		double WAVELENGTH = 10;

		for (int i = 0; i < 27; i++) {
			for (int counter = 0; counter < 40; counter++) {
				sinTable[i][counter] = Math.sin((counter / PERIOD + i
						/ WAVELENGTH)
						* 2 * Math.PI);
			}
		}

	}

	private void createNewBall() {
		// create one valuable at the goal position

		DiskType valuableType = new DiskType(ballMaterial);
		ObjectConstructor ballConstructor = environment
				.createObjectConstructor();
		ballConstructor.setRoot(1.0, valuableType);

		double size = ScenarioUtils.uniform(eRandom, BALL_MIN_SIZE,
				BALL_MAX_SIZE);
		this.ballsOnOutside.add(ballConstructor.createDiskComplex(
				environment.getMaxX() / 2, environment.getMaxY() - 5, 0, size));
	}

	protected AgentConstructor getAgent(Random aRandom, double scaleFactor) {
		return new GraspAgent(environment, agentMaterial, enemyMaterial,
				aRandom);
	}

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom,
			Random eRandom, AgentState[] agentStates) {
		double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE,
				AGENT_MAX_SCALE);
		AgentConstructor ac = getAgent(aRandom, scaleFactor);
		ControlledAgentData agent;
		// agent = ac.createControlledAgent(environment.getMaxX()/2, 5, 0.0,
		// scaleFactor);
		do {
			double posx = ScenarioUtils.uniform(eRandom, 0.0,
					environment.getMaxX());
			double posy = ScenarioUtils.uniform(eRandom, 0.0,
					environment.getMaxY());
			agent = ac.createControlledAgent(posx, posy, 0.0, scaleFactor);

		} while (agent == null);
		this.agentDiskComplex = agent.getDiskComplex();
		agentMass = agentDiskComplex.getMass();

		// set event handler for every disk of the agent
		for (Disk disk : agent.getDisks()) {
			disk.addEventHandler(new EnemyCollisionEventHandler(agentStates[0],
					environment, agentDiskComplex, enemyMaterial,
					octopusMaterial));
		}

		this.eRandom = eRandom;
		teleportToStart();

		return new ControlledAgentData[] { agent };
	}

	protected void teleportToStart() {
		double newOrientation = ScenarioUtils.uniform(eRandom, 0,
				Math.toRadians(360));
		environment.canTeleportCenterOfMass(agentDiskComplex,
				environment.getMaxX() / 2, 5, newOrientation);
	}

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		/**
		 * since the dynamic creation of NPC-Agents hasn't been implemented yet,
		 * all NPCs have to be created at the beginning. At least, at the
		 * beginning they are very slow.
		 */

		//EnemyAgent ac = new EnemyAgent(environment, enemyMaterial,
		//		agentMaterial, ballMaterial, octopusMaterial, random);

		// reference controller cannot deal with npcs at the moment

		// Octopus oc = new Octopus(environment, octopusMaterial, random);
		//
		// NPCAgentData[] enemies = new NPCAgentData[8];
		// for(int i = environment.getDiskComplexes().size() - 1; i <
		// enemies.length; i++) {
		// if(i % 2 == 0 && i != 6) {
		// NPCAgentData octopus;
		// do {
		// double posx = (i%4) == 0? 3.8 : environment.getMaxX() - 3.8;
		// double posy = ScenarioUtils.uniform(random,0, environment.getMaxY());
		// double orientation = (i%4) == 0? 0 : Math.toRadians(180);
		// octopus = oc.createNPCAgent(posx, posy, orientation, 1.0,
		// getOctopusController());
		// } while (octopus == null);
		// enemies[i] = octopus;
		// } else {
		// NPCAgentData enemy;
		// do {
		// double posx = ScenarioUtils.uniform(random, 0.0,
		// environment.getMaxX());
		// double posy = ScenarioUtils.uniform(random,environment.getMaxY() -
		// 2*environment.getMaxY()/3, environment.getMaxY());
		// double orientation = ScenarioUtils.uniform(random,
		// Math.toRadians(250),Math.toRadians(300));
		// enemy = ac.createNPCAgent(posx, posy, orientation, 1.0,
		// getBombController());
		// } while (enemy == null);
		// enemies[i] = enemy;
		// }
		// }
		// return enemies;

		return null;
	}

	/*private AgentController getBombController() {
		AgentController controller = new AgentController() {

			@Override
			public void doTimeStep(double[] sensorValues,
					double[] actuatorValues) {

				followAgent(sensorValues, actuatorValues);

			}

			private void followAgent(double[] sensorValues,
					double[] actuatorValues) {

				// they get gradually faster with simulation time
				double speed = ThiefScenario.getHardness();

				if (sensorValues[0] == 1) {

					// enemy found, follow
					double dir = sensorValues[1];

					actuatorValues[0] = speed;
					actuatorValues[1] = dir * speed;

					if (Math.abs(sensorValues[2]) > 0.75) {
						actuatorValues[0] = -speed;
						actuatorValues[1] = -dir * speed;
					}

				} else {

					// no enemy in range
					actuatorValues[0] = 0;
					actuatorValues[1] = 0;
				}

			}
		};
		return controller;
	}

	private AgentController getOctopusController() {
		AgentController controller = new AgentController() {
			private int counter = 0;

			@Override
			public void doTimeStep(double[] sensorValues,
					double[] actuatorValues) {
				counter++;
				for (int i = 0; i < actuatorValues.length; i++) {
					actuatorValues[i] = sinTable[i][counter % 40];
				}
			}

		};
		return controller;
	}*/

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Provides the time dependent hardness level. */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected double getHardnessLevel(double time) {
		this.time = time;
		ThiefScenario.hardness = ScenarioUtils.sigmoidHardnessFunction(time,
				200, 0.02);
		return hardness;
	}

	static protected double getHardness() {
		return hardness;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Called every time step, update necessary (besides the DiskWorldPhysics
	 * here)
	 */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {

		for (int i = 0; i < ballsOnOutside.size(); i++) {
			DiskComplex ball = ballsOnOutside.get(i);
			if (ball.getCenterx() <= 5 * environment.getMaxX() / 7
					&& ball.getCenterx() >= 2 * environment.getMaxX() / 7
					&& ball.getCentery() < 15) {
				agentState[0].resetHarmLevel(0);
				agentState[0].resetEnergyLevel(INITIAL_ENERGY_LEVEL);
				ballsOnInside.add(ball);
				ballsOnOutside.remove(i);
				// getNPCAgents(eRandom); //doesn't work at the moment (no
				// dynamic creation of npcs)
				createNewBall();
				log().println("You successfully brought one home");
			}
		}
		for (int i = 0; i < ballsOnInside.size(); i++) {
			DiskComplex ball = ballsOnInside.get(i);
			if (!(ball.getCenterx() <= 5 * environment.getMaxX() / 7
					&& ball.getCenterx() >= 2 * environment.getMaxX() / 7 && ball
					.getCentery() < 15)) {
				ballsOnOutside.add(ball);
				ballsOnInside.remove(i);
			}
		}
		agentState[0].incMetabolicEnergyConsumption((1 + 0.5 * getHardness())
				* agentMass
				* METABOLIC_ENERGY_CONSUMPTION_PER_MASS_AND_TIME_UNIT
				* DiskWorldScenario.DEFAULT_DT);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		// the score is the elapsed time (normalized)
		return time / 2 * MAX_SIMULATION_TIME + ballsOnInside.size() / 2
				* (ballsOnInside.size() + ballsOnOutside.size());
	}

	public static class InteractiveController extends
			KeyStrokeInteractiveController {

		@Override
		public void addKeyEvents() {
			// we create a keyboard controller (for qwertz):
			// when key is pressed: increment first value in the actuator array
			// (index 0) by 0.01
			// when key '-' is pressed: decrement it by 0.01
			// when key x is pressed: set it to 0.0

			final boolean returnAngleToZero = true;
			final double turningAngle = returnAngleToZero ? 0.02 : 0.001;
			final double speedChange = 0.02;
			final double armSpeed = 0.3;

			if (returnAngleToZero)
				always(3, 0.0);

			// Actuators:
			// 0, 1 : arms
			// 2 : forward/backward
			// 3 : angular movement
			onKey('W', 2, speedChange, true);
			onKey('S', 2, -speedChange, true);
			onKey('A', 3, turningAngle, true);
			onKey('D', 3, -turningAngle, true);

			onKey('Q', 0, -armSpeed, false);
			onKey('Q', 1, armSpeed, false);
			onKey('E', 0, armSpeed, false);
			onKey('E', 1, -armSpeed, false);

			onKey('X', 0, 0, false);
			onKey('X', 1, 0, false);
			onKey('X', 2, 0, false);
			onKey('X', 3, 0, false);

			setLogMessage("Use the following keys to control the agent:\n"
					+ "w,a,s,d.... -> move forward/angular\n"
					+ "q,e.... -> open/close arms\n"
					+ "x -> stop all actuators");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return ReferenceController.class;
	}

	public static class ReferenceController implements CognitiveController,
			ParametrizedObject {

		private PrintStream log;
		private int grabbingTime = 0;
		private boolean grabbed = false;

		// default parameter values
		private static final double[] DEFAULT_PARAMS = new double[] {
				8.338414040598652, 0.14022927257827023, 2.2973920523899785,
				-2.3784420491000415, 0.1 };

		public ReferenceController() {
			init(DEFAULT_PARAMS);
		}

		@Override
		public int getNumParameters() {
			// adapt number returned to the actual number of double parameters
			return 5;
		}

		@Override
		public void init(double[] parameters) {

		}

		@Override
		public String getName() {
			return "Reference controller for Snail Self Propulsion";
		}

		@Override
		public String getDescription() {
			return "Moves growing and shrinking activity along the body";
		}

		@Override
		public String getAuthors() {
			return "Jan Kneissler";
		}

		@Override
		public String getVersion() {
			return "1.0";
		}

		@Override
		public void provideLogStream(PrintStream logStream) {
			log = logStream;
		}

		@Override
		public void doTimeStep(double energyLevel, double harmLevel,
				double[] sensorValues, double[] actuatorValues) {

			boolean onWhite = sensorValues[8] == 1 && sensorValues[9] == 1
					&& sensorValues[10] == 1;

			boolean onBlue = sensorValues[8] == 1 && sensorValues[9] == 0
					&& sensorValues[10] == 1;

			boolean onGreen = !onWhite && !onBlue;
			// set actuator values based on energy/harm level and sensor values
			// (and their history)...
			// set all actuator values to 1, just to see some action
			// 0:left arm outwards
			// 1:right arm inwards
			// 2:forward
			// 3:turn left

			// sensors: 0-18
			// 17-18: compass
			// 11-16: colorsensor
			// 8-10: groundColor
			// 5-7: walls
			// 0-6: diskSensor

			if (onWhite && !grabbed) {
				escapeHome(sensorValues, actuatorValues);
			}

			if (onGreen && !grabbed) {
				lookForGoal(sensorValues, actuatorValues);

			}

			if (onBlue && !grabbed) {
				catchBall(sensorValues, actuatorValues);
			}

			if (grabbed) {
				takeHome(sensorValues, actuatorValues, onBlue, onWhite);
			}

		}

		private void takeHome(double[] sensorValues, double[] actuatorValues,
				boolean onBlue, boolean onWhite) {
			// turn around
			if (onBlue) {
				if (sensorValues[18] < 0 || sensorValues[17] > 0) {
					actuatorValues[3] = 0.01;
				} else {
					actuatorValues[2] = 1;
				}
				// on blue or white
				// take the way home or release the disk
			} else {
				if (!onWhite) {
					actuatorValues[2] = 1;

					if (sensorValues[18] < 0) {

						actuatorValues[3] = 0.005;
					} else if (sensorValues[18] > 0) {
						actuatorValues[3] = -0.005;
					}
					// else the agent is on the white area and can let go
					// of the disk
				} else {
					grabbingTime--;
					// actuatorValues[0] = 1;
					// actuatorValues[1] = -1;
					if (grabbingTime == 0) {
						grabbed = false;
					}
				}
			}

		}

		private void catchBall(double[] sensorValues, double[] actuatorValues) {
			System.out.println("catch ball");
			if (sensorValues[2] > 0.06) {
				actuatorValues[2] = 0.5;
				actuatorValues[0] = 0.1;
				actuatorValues[1] = -0.1;
			} else {
				grabbingTime++;
				// unfortunately, this doesn't work, because if you close the
				// arms
				// around the other disk, it sometimes gets added to the agent's
				// disk complex
				// actuatorValues[0] = -1;
				// actuatorValues[1] = 1;
				if (grabbingTime == 8) {
					grabbed = true;
				}
			}

		}

		private void lookForGoal(double[] sensorValues, double[] actuatorValues) {
			System.out.println("lookForGoal");
			// sensors: 0-18
			// 17-18: compass
			// 11-16: colorsensor
			// 8-10: groundColor
			// 5-7: walls
			// 0-6: diskSensor

			//on green floor, no other color in sight, no disk in sight
			if (sensorValues[11] == 0 && sensorValues[0] == 0) {
				actuatorValues[2] = 1;
				//go straight
				if (sensorValues[18] < 0) {

					actuatorValues[3] = -0.0005;
				} else if (sensorValues[18] > 0) {
					actuatorValues[3] = 0.0005;
				}
				//on green and blue floor in sight
			} else {
				actuatorValues[2] = 0.5;
				if (sensorValues[0] != 0) {
					if (sensorValues[1] < 0) {
						actuatorValues[3] = -0.005;
					} else if (sensorValues[1] > 0) {
						actuatorValues[3] = 0.005;
					}
				}
			}

		}

		private void escapeHome(double sensorValues[], double[] actuatorValues) {
			//turn around if directed to the bottom
			if (sensorValues[17] < -0.8) {
				actuatorValues[2] = -1;
				actuatorValues[3] = 0.5;
			} else {

				if (sensorValues[11] == 1
						&& (sensorValues[13] == 0 || sensorValues[13] == 0.25 || sensorValues[13] == -0.25)) {
					actuatorValues[2] = 1;
					actuatorValues[3] = 0;

				} else if (sensorValues[5] == 0) {
					actuatorValues[2] = 1;
					actuatorValues[3] = 0;
				} else if (sensorValues[5] == 1 && sensorValues[12] != 0) {
					System.out.println(sensorValues[7]);
					if (sensorValues[18] < 0) {

						actuatorValues[3] = -0.05;
					} else {
						actuatorValues[3] = 0.05;
					}

				} else if (sensorValues[5] == 0) {
					sensorValues[2] = 1;

				}
			}
		}

		@Override
		public void nextEpisode(String[] sensors, String[] actuators) {
			// a new episode has started, we log this to the log stream
			log.println("New Episode started");
		}

		@Override
		public void terminated() {
			// TODO Auto-generated method stub

		}
	}

	public static void main(String args[]) {
		boolean useReferenceController = true;
		boolean useKeyboardController = false;
		boolean useGUIController = false;
		Main.addScenario(ThiefScenario.class, useReferenceController,
				useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		return 1;
	}

}
