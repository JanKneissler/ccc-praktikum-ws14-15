package ccc.scenario.thieves;

import java.util.Set;
import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.Environment;
import diskworld.linalg2D.AngleUtils;
import diskworld.linalg2D.Utils;
import diskworld.sensors.ClosestDiskSensor;

public class MaterialDetectingSensor extends ClosestDiskSensor {
	
	double minRange, maxRange;
	double openingAngle;
	Environment environment;
	DiskMaterial agentMaterial;
	private final Set<DiskMaterial> invisibleMaterials;
	
	int dim, distIndex, posIndex, widthIndex;

	public MaterialDetectingSensor(Environment environment, double openingAngle, double minRangeRelative, double maxRangeAbsolute, 
			DiskMaterial agentMaterial, Set<DiskMaterial> invisibleMaterials) {
		super(environment, 0, openingAngle, minRangeRelative,
				maxRangeAbsolute, null, true, true,
				false, null);
		
		this.minRange = minRangeRelative;
		this.maxRange = maxRangeAbsolute;
		this.openingAngle = openingAngle;
		this.environment = environment;
		
		this.agentMaterial = agentMaterial;
		this.invisibleMaterials = invisibleMaterials;
		
		dim = 2;
		distIndex = dim++;
		posIndex = dim++;
		widthIndex = dim++;
		
	}
	
	//TODO 
	
	@Override
	public int getDimension() {
		return dim;
	}
	
	@Override
	protected void doMeasurement(double measurement[]) {
		double centerx = getDisk().getX();
		double centery = getDisk().getY();
		double direction = getDisk().getAngle();
		double min = maxRange;
		Disk closestDisk = null;
		//System.out.println(invisibleMaterials.toString());
		for (Disk d : getDisksInShape()) {
			DiskMaterial material = d.getDiskType().getMaterial();
			if ((invisibleMaterials == null) || (!invisibleMaterials.contains(material))) {
				double dx = d.getX() - centerx;
				double dy = d.getY() - centery;
				double dist = Math.sqrt(dx * dx + dy * dy) - d.getRadius();
				if (dist <= min) {
					min = dist;
					closestDisk = d;
				}
			}
			
		}
		if (closestDisk == null) {
			//if no disk was found, set all measurements to 0
			for (int i = 0; i < measurement.length; i++) {
				measurement[i] = 0.0;
			}
		} else {
			//if disk was found, set first bool to 1
			measurement[0] = 1.0;
			//check if material is agentMaterial
			DiskMaterial material = closestDisk.getDiskType().getMaterial();
			if(material == agentMaterial) {
				//if so, set second boolean to 1
				measurement[1] = 1;
			} else {
				measurement[1] = 0;
			}
			double dx = closestDisk.getX() - centerx;
			double dy = closestDisk.getY() - centery;
			

			//calculate distance
			double cdist = Math.sqrt(dx * dx + dy * dy);
			measurement[distIndex] = Utils.clip_01(cdist/maxRange);
			// TODO: this is not correct if the center of the disk is outside the cone!
			
			//calculate angle			
			double angle = Math.atan2(dy, dx);
			measurement[posIndex] = Utils.clip_pm1(AngleUtils.mod2PI(angle - direction) / openingAngle * 2);
			// TODO: this is not correct if the center of the disk is outside the cone!
			
			double visAngle = 2 * Math.atan2(closestDisk.getRadius(), cdist);
			measurement[widthIndex] = Utils.clip_01(visAngle / openingAngle * 2);
			// TODO: this is not correct if the center of the disk is outside the cone!
			}
	}

	@Override
	public double getRealWorldInterpretation(double measurement[], int index) {
		if (index == distIndex) {
			return measurement[index] * maxRange;
		}
		if (index == posIndex) {
			return measurement[index] * openingAngle / 2;
		}
		if (index == widthIndex) {
			return measurement[index] * openingAngle / 2;
		}
		return measurement[index];
	}

	@Override
	public String getRealWorldMeaning(int index) {
		if (index == 0) {
			return "disk in range [bool]";
		}
		if(index == 1) {
			return "which type of disk [bool]. 1 for enemies, 0 for all other types";
		}
		if (index == distIndex) {
			return "distance to closest point";
		}
		if (index == posIndex) {
			return "relative angle to closest point [rad]";
		}
		if (index == widthIndex) {
			return "visible width angle [rad]";
		}
		return null;
	}

}
