package ccc.scenario.maze;

import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.demoScenarios.ExampleReferenceController;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actuators.Mover;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.interfaces.Sensor;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class Maze extends DiskWorldScenario {

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Constants */
	// /////////////////////////////////////////////////////////////////////////////////////////

	// Minimum and maximum sizes of the environment
	private static final int ENV_MIN_SIZE_X = 90;
	private static final int ENV_MAX_SIZE_X = 90;
	private static final int ENV_MIN_SIZE_Y = 90;
	private static final int ENV_MAX_SIZE_Y = 90;
	private static final double AGENT_MIN_SCALE = 1;
	private static final double AGENT_MAX_SCALE = 1;

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Fields */
	private DiskType teleporterType1;
	private DiskType teleporterType2;
	private DiskType teleporterType3;
	private DiskType teleporterType4;
	private DiskType teleporterType5;
	private boolean teleportation;
	private boolean taskSolved;
	private DiskType body;
	private DiskType goalType;
	private boolean[][] isWall = new boolean[30][30];
	private boolean[][] singleWall = new boolean[30][30];
	private int posxGoal;
	private int posyGoal;
	private ControlledAgentData agent;
	private int posxAgent;
	private int posyAgent;
	private int counterEpisodes = 0;
	private double scoreArray[] = new double[4];

	// /////////////////////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "TeleportMaze";
	}

	@Override
	public String getDescription() {
		return "Find the most effective Way to the Goal.";
	}

	@Override
	public String getAuthors() {
		return "Darlene Bauer";
	}

	@Override
	public String getContactEmail() {
		return "darlene.bauer@student.uni-tuebingen.de";
	}

	@Override
	public String getVersion() {
		return "0.2";
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Fields */
	// /////////////////////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X,
				ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex,
			double sizey) {
		// create bounding walls at the 4 sides of the environment
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);

		// Add more walls here if needed
		return res;
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		// this scenario has no limitation of number of time steps, set a value
		// > 0 to enable time limitation
		return 100000;// UNLIMITED_TIME;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid
		// red...
		res.getColorScheme().gridColor = Color.CYAN;
		res.getColorScheme().wallColor = Color.LIGHT_GRAY;
		// enable/disable options if needed: disable skeleton display...
		res.getOptions()
				.getOption(VisualizationOptions.GROUP_GENERAL,
						VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL,
				VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		// always one controlled agent
		return 1;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents */
	// /////////////////////////////////////////////////////////////////////////////////////////

	protected boolean isNotSurrounded(int x, int y) {
		if (isWall[x][y] || isWall[x + 1][y] || isWall[x][y + 1] || isWall[x + 1][y + 1]
				|| isWall[x - 1][y] || isWall[x][y - 1] || isWall[x - 1][y - 1]
				|| isWall[x - 1][y + 1] || isWall[x + 1][y - 1]) {
			return false;
		}
		else {
			return true;
		}
	}

	protected void createTeleporter(Environment environment, Random random) {

		boolean freeStartpoints = false;

		int posx1 = 0;
		int posy1 = 0;
		int posx2 = 0;
		int posy2 = 0;
		int posx3 = 0;
		int posy3 = 0;
		int posx4 = 0;
		int posy4 = 0;
		int posx5 = 0;
		int posy5 = 0;

		while (!freeStartpoints) {
			posx1 = (int) (Math.random() * 28) + 1;
			posy1 = (int) (Math.random() * 28) + 1;
			posx2 = (int) (Math.random() * 28) + 1;
			posy2 = (int) (Math.random() * 28) + 1;
			posx3 = (int) (Math.random() * 28) + 1;
			posy3 = (int) (Math.random() * 28) + 1;
			posx4 = (int) (Math.random() * 28) + 1;
			posy4 = (int) (Math.random() * 28) + 1;
			posx5 = (int) (Math.random() * 28) + 1;
			posy5 = (int) (Math.random() * 28) + 1;

			if (isNotSurrounded(posx1, posy1) && isNotSurrounded(posx2, posy2) &&
					isNotSurrounded(posx3, posy3) && isNotSurrounded(posx4, posy4) && isNotSurrounded(posx5, posy5)) {
				freeStartpoints = true;

			} else {
				continue;
			}
		}

		teleporterType1 = new DiskType(DiskMaterial.RUBBER.withColor(Color.BLACK));
		teleporterType1.getMaterial().setFrictionCoefficient(1000000.0);
		teleporterType2 = new DiskType(DiskMaterial.RUBBER.withColor(Color.BLACK));
		teleporterType2.getMaterial().setFrictionCoefficient(1000000.0);
		teleporterType3 = new DiskType(DiskMaterial.RUBBER.withColor(Color.BLACK));
		teleporterType3.getMaterial().setFrictionCoefficient(1000000.0);
		teleporterType4 = new DiskType(DiskMaterial.RUBBER.withColor(Color.BLACK));
		teleporterType4.getMaterial().setFrictionCoefficient(1000000.0);
		teleporterType5 = new DiskType(DiskMaterial.RUBBER.withColor(Color.BLACK));
		teleporterType5.getMaterial().setFrictionCoefficient(1000000.0);

		ObjectConstructor blackHole1 = environment.createObjectConstructor();
		ObjectConstructor blackHole2 = environment.createObjectConstructor();
		ObjectConstructor blackHole3 = environment.createObjectConstructor();
		ObjectConstructor blackHole4 = environment.createObjectConstructor();
		ObjectConstructor blackHole5 = environment.createObjectConstructor();
		blackHole1.setRoot(1.0, teleporterType1);
		blackHole2.setRoot(1.0, teleporterType2);
		blackHole3.setRoot(1.0, teleporterType3);
		blackHole4.setRoot(1.0, teleporterType4);
		blackHole5.setRoot(1.0, teleporterType5);
		blackHole1.createObject((posx1 * 3) + 0.5, (posy1 * 3), 0.0, 1);
		blackHole2.createObject((posx2 * 3) + 0.5, (posy2 * 3), 0.0, 1);
		blackHole3.createObject((posx3 * 3) + 0.5, (posy3 * 3), 0.0, 1);
		blackHole4.createObject((posx4 * 3) + 0.5, (posy4 * 3), 0.0, 1);
		blackHole5.createObject((posx5 * 3) + 0.5, (posy5 * 3), 0.0, 1);

	}

	protected void createGoal(Environment environment, Random random) {
		boolean canSetGoal = false;
		goalType = new DiskType(DiskMaterial.METAL.withColor(Color.YELLOW));

		while (!canSetGoal) {
			posxGoal = (int) (Math.random() * 7) + 1;
			posyGoal = (int) (Math.random() * 7) + 1;
			if (!isNotSurrounded(posxGoal, posyGoal)) {
				System.out.println("goal umgeben");
				continue;
			} else {
				environment.newFixedRoot((posxGoal * 3 + 0.5), (posyGoal * 3 + 0.5), 1.0, Math.toRadians(0), goalType);
				canSetGoal = true;
				System.out.println("ziel erstellt");
				taskSolved = false;
			}
		}
	}

	protected void createWalls(double x, double y) {

		Point point1 = new Point(x, y);
		Point point2 = new Point(x + 2, y);
		Point point3 = new Point(x + 2, y + 2);
		Point point4 = new Point(x, y + 2);
		Line wallLine1 = new Line(point1, point2);
		Line wallLine2 = new Line(point2, point3);
		Line wallLine3 = new Line(point3, point4);
		Line wallLine4 = new Line(point4, point1);
		Line wallLine5 = new Line(point1, point3);
		Line wallLine6 = new Line(point2, point4);
		Wall wall1 = new Wall(wallLine1, 1);
		Wall wall2 = new Wall(wallLine2, 1);
		Wall wall3 = new Wall(wallLine3, 1);
		Wall wall4 = new Wall(wallLine4, 1);
		Wall wall5 = new Wall(wallLine5, 1);
		Wall wall6 = new Wall(wallLine6, 1);
		if (getEnvironment().canAddWall(wall1)) {
			System.out.println("Added a wall");
		}
		if (getEnvironment().canAddWall(wall2)) {
			System.out.println("Added a wall");
		}
		if (getEnvironment().canAddWall(wall3)) {
			System.out.println("Added a wall");
		}
		if (getEnvironment().canAddWall(wall4)) {
			System.out.println("Added a wall");
		}
		if (getEnvironment().canAddWall(wall5)) {
			System.out.println("Added a wall");
		}
		if (getEnvironment().canAddWall(wall6)) {
			System.out.println("Added a wall");
		}

	}

	protected void createSingleWall() {

		boolean validStartpoint = false;

		//startpoint cannot be surrounded by an other wall
		int startpoint_x = 0;
		int startpoint_y = 0;
		while (!validStartpoint) {
			validStartpoint = true;
			startpoint_x = (int) (Math.random() * 29);
			startpoint_y = (int) (Math.random() * 29);

			// if startpoint lies on bounding walls
			if (startpoint_x == 0 || startpoint_x == 29 ||
					startpoint_y == 0 || startpoint_y == 29) {
				validStartpoint = false;
				continue;
			}
			if (!isNotSurrounded(startpoint_x, startpoint_y)) {
				validStartpoint = false;
				continue;
			}
			System.out.println(startpoint_x + " " + startpoint_y);
		}
		int lastDirection = (int) (Math.random() * 2);

		int direction;
		boolean EndOfWall = false;
		while (!EndOfWall) {
			int length = (int) (Math.random() * (8) + 3);
			direction = (int) (Math.random() * 4);
			if (((lastDirection + direction) % 2) != 0) {
				switch (direction) {

				//NORTH
				case 0:
					for (int i = 0; i <= length && !EndOfWall; i++) {

						// if current Wall touches BoundingWalls
						if (startpoint_y + i == 29) {
							singleWall[startpoint_x][startpoint_y + i] = true;
							realizeWall(singleWall);
							EndOfWall = true;
							continue;
						}
						//if current Wall touches itself, delete it
						if (singleWall[startpoint_x][startpoint_y + i + 1] || singleWall[startpoint_x + 1][startpoint_y + i + 1]
								|| singleWall[startpoint_x - 1][startpoint_y + i + 1]) {
							for (int x = 0; x <= 29; x++) {
								for (int y = 0; y <= 29; y++) {
									singleWall[x][y] = false;
								}
							}
							EndOfWall = true;
						}
						// if current Wall touches existing Wall
						if (isWall[startpoint_x][startpoint_y + i + 1] || isWall[startpoint_x + 1][startpoint_y + i + 1]
								|| isWall[startpoint_x - 1][startpoint_y + i + 1]) {
							singleWall[startpoint_x][startpoint_y + i] = true; // leave Space between current
							realizeWall(singleWall); // and existing Wall
							EndOfWall = true;
						}
						else {
							singleWall[startpoint_x][startpoint_y + i] = true;
							if (i == length) {
								startpoint_y = startpoint_y + length;
							}
						}
					}
					lastDirection = 0;
					break;

				//EAST	
				case 1:
					for (int i = 0; i <= length && !EndOfWall; i++) {

						if (startpoint_x + i == 29) {
							singleWall[startpoint_x + i][startpoint_y] = true;
							realizeWall(singleWall);
							EndOfWall = true;
							continue;
						}
						if (singleWall[startpoint_x + i + 1][startpoint_y] || singleWall[startpoint_x + i + 1][startpoint_y + 1]
								|| singleWall[startpoint_x + i + 1][startpoint_y - 1]) {
							for (int x = 0; x <= 29; x++) {
								for (int y = 0; y <= 29; y++) {
									singleWall[x][y] = false;
								}
							}
							EndOfWall = true;
						}
						if (isWall[startpoint_x + i + 1][startpoint_y] || isWall[startpoint_x + i + 1][startpoint_y + 1]
								|| isWall[startpoint_x + i + 1][startpoint_y - 1]) {
							singleWall[startpoint_x + i][startpoint_y] = true;
							realizeWall(singleWall);
							EndOfWall = true;
						}
						else {
							singleWall[startpoint_x + i][startpoint_y] = true;
							if (i == length) {
								startpoint_x = startpoint_x + length;
							}
						}
					}
					lastDirection = 1;
					break;

				//SOUTH	
				case 2:
					for (int i = 0; i <= length && !EndOfWall; i++) {

						if (startpoint_y - i == 0) {
							singleWall[startpoint_x][startpoint_y - i] = true;
							realizeWall(singleWall);
							EndOfWall = true;
							continue;
						}
						if (singleWall[startpoint_x][startpoint_y - i - 1] || singleWall[startpoint_x + 1][startpoint_y - i - 1]
								|| singleWall[startpoint_x - 1][startpoint_y - i - 1]) {
							for (int x = 0; x <= 29; x++) {
								for (int y = 0; y <= 29; y++) {
									singleWall[x][y] = false;
								}
							}
							EndOfWall = true;
						}

						if (isWall[startpoint_x][startpoint_y - i - 1] || isWall[startpoint_x + 1][startpoint_y - i - 1]
								|| isWall[startpoint_x - 1][startpoint_y - i - 1]) {
							singleWall[startpoint_x][startpoint_y - i] = true;
							realizeWall(singleWall);
							EndOfWall = true;
						}
						else {
							singleWall[startpoint_x][startpoint_y - i] = true;
							if (i == length) {
								startpoint_y = startpoint_y - length;
							}
						}
					}
					lastDirection = 2;
					break;

				//WEST
				case 3:
					for (int i = 0; i <= length && !EndOfWall; i++) {

						if (startpoint_x - i == 0) {
							singleWall[startpoint_x - i][startpoint_y] = true;
							realizeWall(singleWall);
							EndOfWall = true;
							continue;
						}

						if (singleWall[startpoint_x - i - 1][startpoint_y] || singleWall[startpoint_x - i - 1][startpoint_y + 1]
								|| singleWall[startpoint_x - i - 1][startpoint_y - 1]) {
							for (int x = 0; x <= 29; x++) {
								for (int y = 0; y <= 29; y++) {
									singleWall[x][y] = false;
								}
							}
							EndOfWall = true;
						}
						if (isWall[startpoint_x - i - 1][startpoint_y] || isWall[startpoint_x - i - 1][startpoint_y + 1]
								|| isWall[startpoint_x - i - 1][startpoint_y - 1]) {
							singleWall[startpoint_x - i][startpoint_y] = true;
							realizeWall(singleWall);
							EndOfWall = true;
						}
						else {
							singleWall[startpoint_x - i][startpoint_y] = true;
							if (i == length) {
								startpoint_x = startpoint_x - length;
							}
						}
					}
					lastDirection = 3;
					break;
				}
			} else {
				direction = (int) (Math.random() * 4);
			}
		}
	}

	private void realizeWall(boolean[][] singleWall) {
		//this.singleWall = singleWall;
		for (int i = 0; i <= 29; i++) {
			for (int j = 0; j <= 29; j++) {
				if (singleWall[i][j]) {
					isWall[i][j] = true;
					singleWall[i][j] = false;
				}
			}
		}
	}

	private void drawMaze() { //stimmt so!
		for (int i = 0; i <= 29; i++) {
			for (int j = 0; j <= 29; j++) {
				if (isWall[i][j]) {
					createWalls(((i * 3) + 0.5), ((j * 3) + 0.5));
				}
			}
		}
	}

	protected void createWholeMaze() {

		// no Walls except bounding walls
		for (int i = 0; i < 30; i++) { //bleibt so
			for (int j = 0; j < 30; j++) {
				isWall[i][j] = false;
				singleWall[i][j] = false;
			}
		}

		for (int i = 0; i < 25; i++) {
			createSingleWall();
		}
		// visualization
		drawMaze();

	}

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom,
			Random eRandom, AgentState[] controlledAgentStates) {

		// sensors and actuators
		Sensor sensor = ClosestDiskSensor.getDistanceSensor(getEnvironment(),
				Math.toRadians(200), 20);
		//Sensor sensor2= new PlaceSensor(environment);

		Actuator actuator = new Mover(1.0, 0.75, 0.05, 0.001, 0.01);

		// create disk types
		body = new DiskType(DiskMaterial.RUBBER.withColor(Color.YELLOW), actuator, new Sensor[] { sensor });
		// now create constructor specifying relative arrangement of disks
		AgentConstructor ac = new AgentConstructor(getEnvironment());
		ac.setRoot(1.0, body);

		// now repeat creating the agent at a random place
		agent = null;
		// random scale factor controlling the size (using the random object for
		// agent)
		double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE,
				AGENT_MAX_SCALE);
		double initialEnergyLevel = 1.0;

		controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
		do {
			// random position and orientation (using the random object for
			// environment)

			posxAgent = (int) ((Math.random() * 7) + 21);
			posyAgent = (int) ((Math.random() * 7) + 21);

			//double orientation = ScenarioUtils.uniform(eRandom, 0.0,
			//	2 * Math.PI);
			agent = ac.createControlledAgent((posxAgent * 3 + 0.5), (posyAgent * 3 + 0.5), Math.PI,
					scaleFactor);
			isWall[28][28] = true;
		} while (agent == null);

		final DiskComplex agentDC = agent.getDiskComplex();

		boolean freeDestination = false;

		int tele1x = 0;
		int tele1y = 0;
		int tele2x = 0;
		int tele2y = 0;
		int tele3x = 0;
		int tele3y = 0;
		int tele4x = 0;
		int tele4y = 0;
		int tele5x = 0;
		int tele5y = 0;

		while (!freeDestination) {
			tele1x = (int) (Math.random() * 28) + 1;
			tele1y = (int) (Math.random() * 28) + 1;
			tele2x = (int) (Math.random() * 28) + 1;
			tele2y = (int) (Math.random() * 28) + 1;
			tele3x = (int) (Math.random() * 28) + 1;
			tele3y = (int) (Math.random() * 28) + 1;
			tele4x = (int) (Math.random() * 28) + 1;
			tele4y = (int) (Math.random() * 28) + 1;
			tele5x = (int) (Math.random() * 28) + 1;
			tele5y = (int) (Math.random() * 28) + 1;
			if (isNotSurrounded(tele1x, tele1y) && isNotSurrounded(tele2x, tele2y) &&
					isNotSurrounded(tele3x, tele3y) && isNotSurrounded(tele4x, tele4y) &&
					isNotSurrounded(tele5x, tele5y)) {
				freeDestination = true;
			} else {
				continue;
			}

		}
		final int x1 = tele1x;
		final int y1 = tele1y;
		final int x2 = tele2x;
		final int y2 = tele2y;
		final int x3 = tele3x;
		final int y3 = tele3y;
		final int x4 = tele4x;
		final int y4 = tele4y;
		final int x5 = tele5x;
		final int y5 = tele5y;

		agent.getDisks().get(0).addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse) {

				if (collidableObject instanceof Disk) {

					System.out.println(collidableObject);
					if (((Disk) collidableObject).getDiskType().equals(teleporterType1)) {
						//if(((Object) collidableObject).equals(blackHole1)){	
						getEnvironment().canTeleportCenterOfMass(agentDC, x1 * 3 + 0.5, y1 * 3 + 0.5, 0);
						log("Agent ist teleportiert!");
						teleportation = true;
					}
					if (((Disk) collidableObject).getDiskType().equals(teleporterType2)) {
						getEnvironment().canTeleportCenterOfMass(agentDC, x2 * 3 + 0.5, y2 * 3 + 0.5, 0);
						log("Agent ist teleportiert!");
						teleportation = true;
					}
					if (((Disk) collidableObject).getDiskType().equals(teleporterType3)) {
						getEnvironment().canTeleportCenterOfMass(agentDC, x3 * 3 + 0.5, y3 * 3 + 0.5, 0);
						log("Agent ist teleportiert!");
						teleportation = true;
					}
					if (((Disk) collidableObject).getDiskType().equals(teleporterType4)) {
						getEnvironment().canTeleportCenterOfMass(agentDC, x4 * 3 + 0.5, y4 * 3 + 0.5, 0);
						log("Agent ist teleportiert!");
						teleportation = true;
					}
					if (((Disk) collidableObject).getDiskType().equals(teleporterType5)) {
						getEnvironment().canTeleportCenterOfMass(agentDC, x5 * 3 + 0.5, y5 * 3 + 0.5, 0);
						log("Agent ist teleportiert!");
						teleportation = true;
					}

					if (((Disk) collidableObject).getDiskType().equals(goalType)) {
						getEnvironment().canTeleportCenterOfMass(agentDC, posxAgent * 3 + 0.5, posyAgent * 3 + 0.5, 0);
						taskSolved = true;
					}
				}

			}
		});

		return new ControlledAgentData[] { agent };
	}

	@Override
	protected void initialize(Environment environment, Random random) {

		// Initialize the floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, FloorCellType.GRASS);
			}
		}

		createWholeMaze();
		// create objects (DiskComplexes that are not agents)
		createTeleporter(getEnvironment(), random);
		createGoal(getEnvironment(), random);
	};

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		// no npc (neutral) agents
		return new NPCAgentData[0];
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Called every time step, put necessary update code (besides the
	 * DiskWorldPhysics) here)
	 */
	// /////////////////////////////////////////////////////////////////////////////////////////
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {

		agentState[0].incEnergyConsumption(0.0005);
		if (teleportation) {
			agentState[0].incHarmInflicted(0.1);
			teleportation = false;
		}

		while (counterEpisodes <= 2 && taskSolved) {

			scoreArray[counterEpisodes] = agentState[0].getEnergyLevel() - agentState[0].getHarmLevel();
			log().println("Score: " + scoreArray[counterEpisodes]);
			agentState[0].resetEnergyLevel(1);
			agentState[0].resetHarmLevel(0);
			nextEpisode();
			taskSolved = false;
			counterEpisodes++;
		}

		if (counterEpisodes == 3 && taskSolved) {

			scoreArray[3] = agentState[0].getEnergyLevel() - agentState[0].getHarmLevel();
			log().println("Score: " + scoreArray[3]);
			if (scoreArray[3] > scoreArray[0] && scoreArray[3] > scoreArray[1]
					&& scoreArray[3] > scoreArray[2]) {
				log().println("Aufgabe erf�llt!");
				taskSolved = false;
				taskSolved();
			}
			else if (scoreArray[3] < scoreArray[0] && scoreArray[3] < scoreArray[1]
					&& scoreArray[3] < scoreArray[2]) {
				log().println("Aufgabe nicht erf�llt!");
				taskSolved = false;
				taskFailed();
			}

		}

	}

	@Override
	public void nextEpisode() {
		log().println("Ziel erreicht! Episode" + " " + (counterEpisodes + 1) + " " + "geschafft!");
		log().println(" ");

	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Provides the time dependent hardness level. */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected double getHardnessLevel(double time) {
		return 0; // we are merciful in the demo scenario, death is not possible
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		//score = (double)NUM_OF_TIME_STEPS/this.getMaxNumTimeSteps();
		// TODO Auto-generated method stub
		return 0;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but */
	/* dedicated to this scenario are implemented here */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return ExampleReferenceController.class;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends
			KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			// we create a keyboard controller with three actions:
			// when key '+' is pressed: increment first value in the actuator
			// array (index 0) by 0.01
			// when key '-' is pressed: decrement it by 0.01
			// when key 0 is pressed: set it to 0.0

			always(0, 0.0);
			always(1, 0.0);
			onKey('W', 0, 1.0, false);
			onKey('S', 0, -1.0, false);
			onKey('D', 1, -1.0, false);
			onKey('A', 1, 1.0, false);

			onKey('0', 2, 0.0, false);
			onKey('0', 3, 0.0, false);
			setLogMessage("Use the following keys to control the agent:\n" +
					"w -> move forward\n" +
					"s -> move backward\n" +
					"d -> turn right \n" +
					"a -> turn left\n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = true;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(Maze.class, useReferenceController,
				useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}