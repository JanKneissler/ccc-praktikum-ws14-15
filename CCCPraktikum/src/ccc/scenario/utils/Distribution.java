package ccc.scenario.utils;

import java.util.Random;

public class Distribution {

	/**
	 * Generate pseudo random normally distributed numbers with given mean and variation. 
	 * @param rand Pseudo random number generator object.
	 * @param mean Mean value of desired normal distribution.
	 * @param sd Standard deviation of desired normal distribution.
	 */
	public static double normal(Random rand, double mean, double sd) {
		return mean + rand.nextGaussian() * sd;
	}
	
	/**
	 * Cumulative distribution function for exponential distribution.
	 * @param x
	 * @param rate Rate parameter lambda of distribution
	 */
	public static double exponentialCDF(double x, double rate) {
		if (x < 0)
			return 0;
		return 1 - Math.exp(-(rate * x));
	}
	
	/**
	 * Random variable following exponential distribution.
	 * @param rand Pseudo random number generator object.
	 * @param rate Rate parameter lambda
	 */
	public static double exponential(Random rand, double rate) {
		return -Math.log(rand.nextDouble()) / rate;
	}
	
	/**
	 * Logistic function
	 * @param x Current value
	 * @param x0 Mean value (where result is 0.5)
	 * @param k Steepness
	 */
	public static double logistic(double x, double x0, double rate) {
		return 1 / (1 + Math.exp(-rate * (x - x0)));
	}


}
