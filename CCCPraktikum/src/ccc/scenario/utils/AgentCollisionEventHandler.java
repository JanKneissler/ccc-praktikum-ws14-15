package ccc.scenario.utils;

import ccc.interfaces.AgentState;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;

public abstract class AgentCollisionEventHandler implements CollisionEventHandler {
	protected AgentState agentState;
	
	public AgentCollisionEventHandler(AgentState agentState) {
		this.agentState = agentState;
	}
	
	public abstract void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse);
}