package ccc.scenario.utils;

import ccc.interfaces.AgentState;
import ccc.scenario.utils.ConsumableDiskMaterial;
import diskworld.Disk;
import diskworld.Environment;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;

public class ConsumableCollisionEventHandler implements CollisionEventHandler {
	protected AgentState agentState;
	protected Environment environment;
	
	public ConsumableCollisionEventHandler(AgentState agentState, Environment environment) {
		this.agentState = agentState;
		this.environment = environment;
	}

	@Override
	public void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse) {
		if (collidableObject instanceof Disk) {
			if (((Disk) collidableObject).getDiskType().getMaterial() instanceof ConsumableDiskMaterial) {
				double effect = ((ConsumableDiskMaterial) ((Disk) collidableObject).getDiskType().getMaterial()).getHealthEffect();
				if (effect > 0) {
					agentState.incEnergyGained(effect);
				} else if (effect < 0) {
					agentState.incHarmInflicted(Math.abs(effect));
				}
				environment.getDiskComplexesEnsemble().removeDiskComplex(((Disk) collidableObject).getDiskComplex());
			}
		}
	}
}