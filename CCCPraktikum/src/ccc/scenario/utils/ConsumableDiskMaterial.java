package ccc.scenario.utils;

import java.awt.Color;
import ccc.scenario.utils.PerishableDiskMaterial;

public class ConsumableDiskMaterial extends PerishableDiskMaterial {
	
	protected double healthEffect; // Influence of consumption on health (+/-).

	/**
	 * Predefined material: Apple
	 */
	public final static ConsumableDiskMaterial APPLE = 
			new ConsumableDiskMaterial(0.5, 0.9, 0.7, 0.8, Color.GREEN, 0.02);
	
	/**
	 * Predefined material: Fly agaric (German: Fliegenpilz)
	 */
	public final static ConsumableDiskMaterial FLY_AGARIC = 
			new ConsumableDiskMaterial(0.5, 0.9, 0.7, 0.8, Color.RED, -0.2);

	
	public ConsumableDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor) {
		this(density, elasticity, frictionCoefficient, gripCoefficient,
				displayColor, 0, -1);
	}

	public ConsumableDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor, double healthEffect) {
		this(density, elasticity, frictionCoefficient, gripCoefficient,
				displayColor, healthEffect, -1);
	}

	public ConsumableDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor, double healthEffect, double endOfLifeTime) {
		super(density, elasticity, frictionCoefficient, gripCoefficient,
				displayColor, endOfLifeTime);
		this.healthEffect = healthEffect;
	}
	
	public double getHealthEffect() {
		return healthEffect;
	}
	
	public void setHealthEffect(double newHealthEffect) {
		this.healthEffect = newHealthEffect;
	}

}
