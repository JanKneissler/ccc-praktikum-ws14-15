package ccc.scenario.utils;

import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.Environment;
import diskworld.actuators.Consumer.ConsumptionHandler;

public class ConsumableConsumptionHandler implements ConsumptionHandler {
	protected AgentState agentState;
	protected Environment environment;

	public ConsumableConsumptionHandler(AgentState agentState, Environment environment) {
		this.agentState = agentState;
		this.environment = environment;
	}

	@Override
	public boolean consumes(Disk d) {
		if (d.getDiskType().getMaterial() instanceof ConsumableDiskMaterial) {
			double effect = ((ConsumableDiskMaterial) d.getDiskType().getMaterial()).getHealthEffect();
			if (effect > 0) {
				agentState.incEnergyGained(effect);
			} else if (effect < 0) {
				agentState.incHarmInflicted(Math.abs(effect));
			}
			environment.deleteDisk(d);
			return true;
		}
		return false;
	}

}
