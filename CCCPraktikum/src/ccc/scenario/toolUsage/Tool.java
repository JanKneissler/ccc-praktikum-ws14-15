package ccc.scenario.toolUsage;

import java.awt.Color;

import diskworld.DiskMaterial;

/**
 * Basic class for a tool.
 * 
 * @author marcel
 * 
 */
public abstract class Tool extends DiskMaterial {

	public Tool(Color color) {
		super(1, 0, 0.5, 0.5, color);
	}

	/**
	 * @return cost of the usage of this tool.
	 */
	public abstract double use();

	/**
	 * @return true if this tool should be delete after being used.
	 */
	public abstract boolean isConsumable();

}
