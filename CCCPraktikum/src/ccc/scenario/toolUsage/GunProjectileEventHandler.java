package ccc.scenario.toolUsage;

import ccc.DiskWorldScenario.ControlledAgentData;
import diskworld.Disk;
import diskworld.Environment;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;

/**
 * Event Handler for a gun projectile.
 * @author marcel
 *
 */
class GunProjectileEventHandler implements CollisionEventHandler {
	Disk disk;
	Environment environment;
	ControlledAgentData agent;

	public GunProjectileEventHandler(Disk disk, Environment environment, ControlledAgentData agent) {
		this.disk = disk;
		this.environment = environment;
		this.agent = agent;
	}

	/**
	 * On collsion destroy both objects.
	 */
	@Override
	public void collision(CollidableObject collidableObject,
			Point collisionPoint, double exchangedImpulse) {

		if (collidableObject instanceof Disk) {
			if (!(collidableObject.equals(agent.getDiskComplex().getDisks().get(0)))) {
				environment.getDiskComplexesEnsemble().removeDiskComplex(
						((Disk) collidableObject).getDiskComplex());
			}
			

			environment.getDiskComplexesEnsemble().removeDiskComplex(
					disk.getDiskComplex());

		} else {
			environment.getDiskComplexesEnsemble().removeDiskComplex(
					disk.getDiskComplex());
		}
	}
}