package ccc.scenario.toolUsage;

import java.awt.Color;

/**
 * Implementation of gun ammunition.
 * @author marcel
 *
 */

public class GunAmmunition extends Charges{


	public GunAmmunition() {
		super(Color.BLACK);
	}

	@Override
	public Tool belongsTo() {
		return new Gun();
	}

	@Override
	public void addCharges(Tool tool) {
		((Gun)tool).incAmmunition(20);
		
	}

}
