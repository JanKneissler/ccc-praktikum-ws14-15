package ccc.scenario.toolUsage;

import java.awt.Color;

import ccc.evaluation.AgentStateImpl;
import diskworld.Environment;
/**
 * Implementation of a Energy Reverter.
 * @author marcel
 *
 */
public class EnergyReverter extends TimeConsumableTool{
	AgentStateImpl agentState;

	public EnergyReverter(Environment env, AgentStateImpl agentState) {
		super(5.0, env, Color.MAGENTA);
		this.agentState = agentState;
		
	}
	


	public double use(){
		
	return 0;	
		
	}

}
