package ccc.scenario.toolUsage;

import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.demoScenarios.ExampleReferenceController;
import ccc.evaluation.AgentStateImpl;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actions.DiskModification;
import diskworld.actuators.ActuatorBag;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.interfaces.Sensor;
import diskworld.linalg2D.Point;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

/**
 * 
 * Scenario that implements the usage of tools.
 * 
 * @author marcel
 * 
 */
public class ToolBoxScenario extends DiskWorldScenario {

	// Minimum and maximum sizes of the environment
	private static final int ENV_MIN_SIZE_X = 120;
	private static final int ENV_MAX_SIZE_X = 180;
	private static final int ENV_MIN_SIZE_Y = 120;
	private static final int ENV_MAX_SIZE_Y = 180;
	private static final double AGENT_MIN_SCALE = 3;
	private static final double AGENT_MAX_SCALE = 3;

	// True to show ToolBox visulisation
	private static final boolean SHOW_FRAME = true;
	// values stored for later usage
	private ControlledAgentData agent;
	private AgentStateImpl agentState;
	private Environment environment;
	private Random aRandom;
	private Random eRandom;

	// toolBox related fields	
	private ToolBox toolBox;
	private ToolProbabilityPair[] toolProbabilityPairs;

	// Counter for adding enemies
	private int count = 0;
	private static final int TIME_TO_CREATE_ENEMY = 20;

	// Different floor types
	public static FloorCellType TYPE1 = new FloorCellType(0.003, Color.BLUE);
	public static FloorCellType TYPE2 = new FloorCellType(0.003, Color.WHITE);
	public static FloorCellType TYPE3 = new FloorCellType(0.003, Color.GRAY);

	@Override
	public String getName() {
		return "Tools";
	}

	@Override
	public String getDescription() {
		return "Scenario to test the ToolBox functions.";
	}

	@Override
	public String getAuthors() {
		return "Marcel Binz";
	}

	@Override
	public String getContactEmail() {
		return "marcel.binz@student.uni-tuebingen.de";
	}

	@Override
	public String getVersion() {
		return "3.4";
	}

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X,
				ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex,
			double sizey) {
		// create bounding walls at the 4 sides of the environment
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);
		return res;
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		return UNLIMITED_TIME;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		res.getOptions()
				.getOption(VisualizationOptions.GROUP_GENERAL,
						VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected void initialize(Environment environment, Random random) {
		// store environment for later use
		this.environment = environment;

		// Initialize the floor
		Floor floor = environment.getFloor();
		double centerX = environment.getMaxX() / 2;
		double centerY = environment.getMaxY() / 2;
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				double distance = Math.sqrt((x - centerX) * (x - centerX)
						+ (y - centerY) * (y - centerY));
				if (distance < environment.getMaxX() / 5) {
					floor.setType(x, y, ToolBoxScenario.TYPE1);
				} else {
					if (distance < environment.getMaxX() / 2) {
						floor.setType(x, y, ToolBoxScenario.TYPE2);
					} else {
						floor.setType(x, y, ToolBoxScenario.TYPE3);
					}
				}
			}
		}

	};

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom,
			Random eRandom, AgentState[] controlledAgentStates) {

		this.agentState = (AgentStateImpl) controlledAgentStates[0];

		// store randoms for later usage
		this.aRandom = aRandom;
		this.eRandom = eRandom;

		// sensors and actuators

		// create Actuators and Sensors 
		ClosestDiskSensor sensor = new ClosestDiskSensor(this.environment, 0,
				Math.toRadians(60), 1, 50, null, true, true, false,
				null, 10);

		this.toolBox = new ToolBox(environment, this.eRandom, SHOW_FRAME);
		MyMover mover = new MyMover(0.5, 0.5, Math.PI / 2, 0.000001, 0.0001);
		ActuatorBag actuators = new ActuatorBag(
				new Actuator[] { mover, toolBox });

		// create disk types
		DiskType body = new DiskType(DiskMaterial.RUBBER.withColor(Color.BLUE),
				actuators, new Sensor[] { sensor });

		// now create constructor specifying relative arrangement of disks
		AgentConstructor ac = new AgentConstructor(environment);
		ac.setRoot(2.0, body);

		// now create the agent at a random place
		double scaleFactor = ScenarioUtils.uniform(this.aRandom,
				AGENT_MIN_SCALE, AGENT_MAX_SCALE);
		double posx = ScenarioUtils
				.uniform(eRandom, 0.0, environment.getMaxX());
		double posy = ScenarioUtils
				.uniform(eRandom, 0.0, environment.getMaxY());
		double orientation = ScenarioUtils.uniform(eRandom, 0.0, 2 * Math.PI);
		this.agent = ac.createControlledAgent(posx, posy, orientation,
				scaleFactor);

		// create EventHandler for the agent
		Disk disk = agent.getDiskComplex().getDisks().get(0);
		ToolBoxCollisionEventHandler handler = new ToolBoxCollisionEventHandler(
				disk, toolBox, this.agentState);
		disk.addEventHandler(handler);

		// initialize the tools with probabilities for this scenario
		this.toolProbabilityPairs = new ToolProbabilityPair[] {
				new ToolProbabilityPair(new Gun(this.environment, this.agent),
						0.5),
				new ToolProbabilityPair(new HealthRegenarator(this.agentState),
						0.3),
				new ToolProbabilityPair(new GunAmmunition(), 0.0),
				new ToolProbabilityPair(new EnergyReverter(this.environment,
						this.agentState), 0.1),
				new ToolProbabilityPair(new Shield(this.environment), 0.1) };
		toolBox.init(this.agent, this.agentState, this.toolProbabilityPairs);

		return new ControlledAgentData[] { agent };

	}

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		return new NPCAgentData[0];
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {

		// Score increases the longer the agent lives
		return Math.tanh(environment.getTime() / 50.0);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but */
	/* dedicated to this scenario are implemented here */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return ExampleReferenceController.class;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends
			KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			final boolean returnAngleToZero = true;
			final double turningAngle = returnAngleToZero ? 0.05 : 0.02;
			final double speedChange = 0.05;

			if (returnAngleToZero)
				always(1, 0.0);

			// Move forwards
			add(java.awt.event.KeyEvent.VK_NUMPAD8, 0, speedChange, true);
			add(java.awt.event.KeyEvent.VK_W, 0, speedChange, true);

			// Move backwards
			add(java.awt.event.KeyEvent.VK_NUMPAD5, 0, -speedChange, true);
			add(java.awt.event.KeyEvent.VK_S, 0, -speedChange, true);

			// Rotate left
			add(java.awt.event.KeyEvent.VK_NUMPAD4, 1, turningAngle,
					!returnAngleToZero);
			add(java.awt.event.KeyEvent.VK_A, 1, turningAngle,
					!returnAngleToZero);

			// Rotate right
			add(java.awt.event.KeyEvent.VK_NUMPAD6, 1, -turningAngle,
					!returnAngleToZero);
			add(java.awt.event.KeyEvent.VK_D, 1, -turningAngle,
					!returnAngleToZero);

			// switch Tool
			always(2, 0);
			add(java.awt.event.KeyEvent.VK_T, 2, 1, false);

			// use Tool
			always(3, 0);
			add(java.awt.event.KeyEvent.VK_SPACE, 3, 1, false);

			// Return speed and angle to zero
			add(java.awt.event.KeyEvent.VK_NUMPAD0, 0, 0.0, false);
			add(java.awt.event.KeyEvent.VK_NUMPAD0, 1, 0.0, false);
			add(java.awt.event.KeyEvent.VK_0, 0, 0.0, false);
			add(java.awt.event.KeyEvent.VK_0, 1, 0.0, false);

			setLogMessage("Use the following keys to control the agent:\n"
					+ "a / numpad4 -> rotate left \n"
					+ "d / numpad6 -> rotate right\n"
					+ "w / numpad8 -> increase speed\n"
					+ "s / numpad5 -> reduce speed\n" + "0 / numpad0 -> stop\n"
					+ "t -> switch tool\n" + "space -> use active tool\n\n"
					+ "Red: Gun\n" + "Green: Health Regenerator\n"
					+ "Black: Gun Ammunition\n" + "Pink: Energy Reverter\n"
					+ "Yellow: Shield\n\n" + "Blue: Enemies");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = true;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(ToolBoxScenario.class, useReferenceController,
				useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

	@Override
	protected double getHardnessLevel(double time) {
		return 0;
	}

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {
		// spawn enemies
		this.spawnEnemy();

		// changes probabilities if you already have a gun
		if (toolBox.containsTool(new Gun())) {
			toolBox.getToolProbabilityPairs()[0].setProbability(0.0);
			toolBox.getToolProbabilityPairs()[2].setProbability(0.5);
		}
		// spawn tools
		toolBox.spawnNewTool();

		// Workaround: Check if EnergyReverter is active and applies its effect
		if (toolBox.isActive(new EnergyReverter(environment, agentState))) {
			double currentEnergyGain = this.agentState.getEnergyGained();
			this.agentState.incEnergyGained(2 * agentState.getEnergyConsumedInActions());
			this.agentState.incEnergyConsumption(2 * currentEnergyGain);

		}

	}

	/**
	 * Creates enemy discs.
	 */

	private void spawnEnemy() {
		DiskComplex dc = null;

		// Attempt to create a enemy very 2
		if (count > TIME_TO_CREATE_ENEMY) {

			// create objectConstructor
			DiskType ballType = new DiskType(DiskMaterial.RUBBER);
			ObjectConstructor ballConstructor = environment
					.createObjectConstructor();
			ballConstructor.setRoot(1.0, ballType);

			// random a position
			double y = ScenarioUtils.uniform(eRandom, 0.0,
					environment.getMaxY());
			double x = ScenarioUtils.uniform(eRandom, 0.0,
					environment.getMaxX());

			// Made decision if enemy is actually created. Based on the floor below it.
			environment.getFloor().getType(x, y);
			double random = eRandom.nextDouble();
			FloorCellType floorType = environment.getFloor().getType(x, y);
			if (floorType == ToolBoxScenario.TYPE1) {
				dc = ballConstructor.createDiskComplex(x, y, 0.0, 1);
			} else {
				if (floorType == ToolBoxScenario.TYPE2) {
					if (random < 0.66) {
						dc = ballConstructor.createDiskComplex(x, y, 0.0, 1);
					}
				} else {
					if (random < 0.33) {
						dc = ballConstructor.createDiskComplex(x, y, 0.0, 1);
					}
				}
			}

			// apply movement
			if (dc != null) {
				dc.applyImpulse(2 * eRandom.nextDouble() - 0.5,
						2 * eRandom.nextDouble() - 0.5, dc.getCenterx(),
						dc.getCentery());
			}

			count = 0;
		}
		count++;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		return 1;
	}

	/**
	 * CollisionEventHandler for the agent in ToolBoxScenario.
	 * 
	 * @author marcel
	 * 
	 */
	class ToolBoxCollisionEventHandler implements CollisionEventHandler {
		private Disk disk;
		private ToolBox toolBox;
		private AgentStateImpl agentState;
		private Shield shield;

		public ToolBoxCollisionEventHandler(Disk disk, ToolBox toolBox,
				AgentStateImpl agentState) {
			this.agentState = agentState;
			this.disk = disk;
			this.toolBox = toolBox;
			this.shield = new Shield(environment);
		}

		@Override
		public void collision(CollidableObject collidableObject,
				Point collisionPoint, double exchangedImpulse) {

			// increase harm if collision with wall
			if (collidableObject instanceof Wall) {
				this.agentState.incHarmInflicted(0.01);
			}

			// increase size of agent if collision with disk, that is not a
			// projectile or tool
			// doesn't increase if shield is active
			if (collidableObject instanceof Disk) {
				if (!(isToolOrProjectile((Disk) collidableObject))
						&& !toolBox.isActive(shield)) {

					DiskModification dm = new DiskModification(disk,
							disk.getRadius() + 0.5);
					disk.getDiskComplex().performModification(dm);

				}

				// remove the DiskComplex
				environment.getDiskComplexesEnsemble().removeDiskComplex(
						((Disk) collidableObject).getDiskComplex());

				DiskMaterial type = ((Disk) collidableObject).getDiskType()
						.getMaterial();

				// adds a tool to the ToolBox if collidableObject is a tool
				if (type instanceof Tool) {
					toolBox.addTool((Tool) type);

				}

			}

		}

		/**
		 * @param collidableObject
		 * @return checks if a disk is a tool or projectile
		 */
		private boolean isToolOrProjectile(Disk collidableObject) {
			if (collidableObject.getDiskType().getMaterial() instanceof Tool)
				return true;
			if (collidableObject.getDiskType().getMaterial() instanceof GunProjectile)
				return true;
			return false;
		}

	}

	public AgentStateImpl getAgentState() {
		return agentState;
	}

}
