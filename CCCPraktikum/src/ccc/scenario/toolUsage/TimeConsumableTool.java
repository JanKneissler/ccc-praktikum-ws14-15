package ccc.scenario.toolUsage;

import java.awt.Color;

import diskworld.Environment;

/**
 * Class for tools, which are deleted on use, but their effects last a specific duration.
 * @author marcel
 *
 */
public abstract class TimeConsumableTool extends Tool {
	public double duration;
	protected Environment env;
	double startTime;

	public TimeConsumableTool(double duration, Environment env, Color color) {
		super(color);
		this.duration = duration;
		this.env = env;
		this.startTime = -1;

	}

	@Override
	public boolean isConsumable() {
		return true;

	}

	/**
	 * @return true if this tool is still active.
	 */
	public boolean isActive() {
		
		// case tool not used yet
		if (startTime < 0) {
			return false;
		}
		
		// still running
		if (env.getTime() < this.duration + this.startTime) {
			return true;
		} else {
			return false;
		}
	}

}
