package ccc.scenario.toolUsage;

import diskworld.Disk;
import diskworld.Environment;
import diskworld.actuators.Mover;
import diskworld.environment.FloorCellType;

/**
 * Mover, which cost changes on specific grounds.
 * @author marcel
 *
 */
public class MyMover extends Mover {

	public MyMover(double maxBackwardDistance, double maxForwardDistance,
			double maxRotationValue, double rotationEnergyConsumption,
			double moveEnergyConsumption) {
		super(maxBackwardDistance, maxForwardDistance, maxRotationValue,
				rotationEnergyConsumption, moveEnergyConsumption);
	}

	public double evaluateEffect(Disk disk, Environment environment,
			double[] activity, double partial_dt, double total_dt,
			boolean firstSlice) {
		double totalCost = super.evaluateEffect(disk, environment, activity,
				partial_dt, total_dt, firstSlice) + 0.00001;
		
		// check ground type and adapt values
		FloorCellType floorType = environment.getFloorCellTypeAtPosition(
				disk.getX(), disk.getY());
		if (floorType == ToolBoxScenario.TYPE1) {
			totalCost *= 0.5;
		} else {
			if (floorType == ToolBoxScenario.TYPE3) {
				totalCost *= 1.5;
			} else {

			}
		}
		return totalCost;
	}
}
