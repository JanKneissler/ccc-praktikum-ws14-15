package ccc.scenario.toolUsage;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;

import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.AgentStateImpl;
import diskworld.Disk;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actuators.ActuatorVisualisationVariant;
import diskworld.actuators.ActuatorWithVisualisation;
import diskworld.visualization.VisualisationItem;

/**
 * Class that captures all the tool features. This includes using tools,
 * managing and spawning them. This class is a Actuator with 2 binary values for
 * using and switching tools.
 * 
 * @author marcel
 * 
 */
public class ToolBox extends ActuatorWithVisualisation {

	private ArrayList<Tool> toolList;
	private ArrayList<TimeConsumableTool> currentTimeConsumableTools;
	private int selectedToolIndex;
	private int numTools;
	private Environment env;
	private JFrame frame;
	private JLabel label;
	private ToolProbabilityPair[] toolProbabilityPairs;
	private Random eRandom;
	private boolean showFrame;

	/**
	 * @param env
	 *            Environment the toolBox is in.
	 * @param eRandom
	 *            Random instance.
	 * @param showFrame
	 *            true if visualization should be shown.
	 */

	public ToolBox(Environment env, Random eRandom, boolean showFrame) {
		super("ToolBox");
		this.toolList = new ArrayList<Tool>();
		this.currentTimeConsumableTools = new ArrayList<TimeConsumableTool>();
		this.env = env;
		this.eRandom = eRandom;
		this.showFrame = showFrame;

		// create Frame for debugging
		if (showFrame) {
			frame = new JFrame("ToolBox");
			frame.setLocation(1100, 282);
			label = new JLabel("");
			frame.add(label);
			frame.setSize(235, 220);
			frame.setVisible(true);
		}
	}

	public ToolProbabilityPair[] getToolProbabilityPairs() {
		return toolProbabilityPairs;
	}

	public void setToolProbabilityPairs(
			ToolProbabilityPair[] toolProbabilityPairs) {
		this.toolProbabilityPairs = toolProbabilityPairs;
	}

	/**
	 * Adds a tool to this toolBox.
	 * 
	 * @param toolToAdd
	 *            Tool to add.
	 */
	public void addTool(Tool toolToAdd) {

		if (!(toolToAdd instanceof Charges)) {

			// adds if it is not in already
			if (!this.containsTool(toolToAdd)) {
				toolList.add(toolToAdd);
				numTools++;
				System.out.println("Tool added: "
						+ toolToAdd.getClass().getSimpleName());
			}
		} else {
			// add Charges if corresponding tool is in this toolBox
			Tool tool = ((Charges) toolToAdd).belongsTo();
			if (this.containsTool(tool)) {
				((Charges) toolToAdd).addCharges(this.getTool(tool));
			}
		}

	}

	/**
	 * @param toolToAdd
	 * @return true if tool is in this toolBox
	 */
	public boolean containsTool(Tool toolToAdd) {
		for (Tool t : toolList) {
			if (t.getClass() == toolToAdd.getClass()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param tool
	 * @return tool if it's in the toolBox, else null.
	 */
	private Tool getTool(Tool tool) {
		for (Tool t : toolList) {
			if (t.getClass() == tool.getClass()) {
				return t;
			}
		}
		return null;
	}

	/**
	 * Uses the active tool.
	 */
	public void useActiveTool() {
		if (!toolList.isEmpty()) {

			// use the selected tool
			toolList.get(selectedToolIndex).use();

			// add to list of currently active TimeConsumableTools
			if (toolList.get(selectedToolIndex) instanceof TimeConsumableTool) {
				((TimeConsumableTool) toolList.get(selectedToolIndex)).startTime = env
						.getTime();
				currentTimeConsumableTools.add((TimeConsumableTool) toolList
						.get(selectedToolIndex));

			}

			// remove if consumable
			if (toolList.get(selectedToolIndex).isConsumable()) {
				toolList.remove(selectedToolIndex);
				numTools--;
				this.switchToNextTool();
			}

		}
	}

	/**
	 * Switch to the next tool.
	 */
	public void switchToNextTool() {
		if (numTools != 0) {
			selectedToolIndex = ++selectedToolIndex % numTools;

		}
	}

	@Override
	public int getDim() {
		return 2;
	}

	@Override
	public boolean isAlwaysNonNegative(int index) {
		return false;
	}

	@Override
	public boolean isBoolean(int index) {
		return true;
	}

	@Override
	public double getActivityFromRealWorldData(double realWorldValue, int index) {
		return 0;
	}

	@Override
	public String getRealWorldMeaning(int index) {
		return null;
	}

	@Override
	public double evaluateEffect(Disk disk, Environment environment,
			double[] activity, double partial_dt, double total_dt,
			boolean firstSlice) {

		// update text in debugging frame
		if (this.showFrame)
			label.setText(this.toString());
		double cost = 0;
		int size = currentTimeConsumableTools.size();

		// sum up over all TimeConsumbleTools
		for (int i = 0; i < size; i++) {
			TimeConsumableTool tool = currentTimeConsumableTools.get(i);
			if (tool.isActive()) {
				cost += tool.use();
			} else {

				currentTimeConsumableTools.remove(i);
				i--;
				size--;
			}
		}

		// check activities
		if (activity[0] > 0) {
			this.switchToNextTool();
		}
		if (activity[1] > 0) {
			this.useActiveTool();
		}
		return cost;
	}

	@Override
	public VisualisationItem getVisualisationItem() {
		return null;
	}

	@Override
	protected ActuatorVisualisationVariant[] getVisualisationVariants() {
		return null;
	}

	/**
	 * @param tool
	 * @return true if a TimeConsumableTool is currently active.
	 */

	public boolean isActive(TimeConsumableTool tool) {
		for (Tool t : currentTimeConsumableTools) {
			if (t.getClass() == tool.getClass()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * String for this class, used for debugging frame.
	 */
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("<html>");
		for (int i = 0; i < toolList.size(); i++) {
			Tool tool = toolList.get(i);
			if (i == selectedToolIndex) {
				builder.append("<font color='red'>");
			}
			builder.append(i + ": ");
			builder.append(tool.getClass().getSimpleName() + " ");
			if (tool instanceof Gun) {
				builder.append(", Ammunition: " + ((Gun) tool).ammunition);
			}
			if (i == selectedToolIndex) {
				builder.append("</font>");
			}
			builder.append("<br>");
		}

		if (!currentTimeConsumableTools.isEmpty())
			builder.append("<br> Current ticking tools: <br>");
		for (TimeConsumableTool tool : currentTimeConsumableTools) {
			String dur = String.format("%.2f",
					(tool.duration - (env.getTime() - tool.startTime)));
			builder.append(tool.getClass().getSimpleName() + " Duration: "
					+ dur + "<br>");
		}
		builder.append("</html>");
		return builder.toString();
	}

	/**
	 * Spawn new tools.
	 */
	void spawnNewTool() {
		if (this.eRandom.nextDouble() < 0.98 * Math.exp(-this.env.getTime()) + 0.02) {
			// create ObjectConstructor
			ObjectConstructor ballConstructor = env.createObjectConstructor();
			// choose place for tool
			double x = ScenarioUtils.uniform(this.eRandom, 0.0, env.getMaxX());
			double y = ScenarioUtils.uniform(this.eRandom, 0.0, env.getMaxY());

			// select a tool
			double toolNumber = this.eRandom.nextDouble();
			int index = toolProbabilityPairs.length - 1;
			double sum = 0;
			for (int i = 0; i < toolProbabilityPairs.length; i++) {
				sum += toolProbabilityPairs[i].getProbability();
				if (sum >= toolNumber) {
					index = i;
					break;
				}
			}

			// create corresponding tool
			ballConstructor.setRoot(1, true, new DiskType(
					this.toolProbabilityPairs[index].getTool()));
			ballConstructor.createDiskComplex(x, y, 0.0, 1);

		}
	}

	/**
	 * Initializing of different fields of this class. Should be called after agent is created.
	 * 
	 * @param agent
	 * @param agentState
	 * @param toolProbabilityPairs
	 */
	public void init(ControlledAgentData agent, AgentStateImpl agentState,
			ToolProbabilityPair[] toolProbabilityPairs) {
		this.toolProbabilityPairs = toolProbabilityPairs;

	}

}
