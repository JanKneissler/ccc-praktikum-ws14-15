package ccc.scenario.toolUsage;

import java.awt.Color;

import diskworld.Environment;

/**
 * Implementation of a shield.
 * 
 * @author marcel
 * 
 */
public class Shield extends TimeConsumableTool {

	public Shield(Environment env) {
		super(10.0, env, Color.YELLOW);
	}

	public double use() {
		return 0;

	}
}
