package ccc.scenario.toolUsage;

import java.awt.Color;

/**
 * Basic class for charges for specific tools.
 * @author marcel
 *
 */

public abstract class Charges extends Tool{

	public Charges(Color color) {
		super(color);
	}

	@Override
	public double use() {
		return 0;
		
	}

	@Override
	public boolean isConsumable() {
		return false;
	}
	
	public abstract Tool belongsTo();

	public abstract void addCharges(Tool tool);

}
