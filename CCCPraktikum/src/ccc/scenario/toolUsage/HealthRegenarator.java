package ccc.scenario.toolUsage;

import java.awt.Color;

import ccc.evaluation.AgentStateImpl;

/**
 * Implemenatation of a Health Regenerator.
 * 
 * @author marcel
 * 
 */
public class HealthRegenarator extends Tool {
	AgentStateImpl agentState;

	public HealthRegenarator(AgentStateImpl agentState) {
		super(Color.GREEN);
		this.agentState = agentState;

	}

	@Override
	public double use() {
		agentState.incEnergyGained(0.25);
		return 0;
	}

	@Override
	public boolean isConsumable() {
		return true;
	}

}
