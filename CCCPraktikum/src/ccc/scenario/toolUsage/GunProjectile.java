package ccc.scenario.toolUsage;

import java.awt.Color;

import diskworld.DiskMaterial;

/**
 * Implemenation of a gun projectile.
 * 
 * @author marcel
 * 
 */
public class GunProjectile extends DiskMaterial {

	public GunProjectile() {
		super(0.5, 0.8, 0.5, 0.5, Color.PINK);
	}
}