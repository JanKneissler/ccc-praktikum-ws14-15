package ccc.scenario.toolUsage;

import java.awt.Color;

import ccc.DiskWorldScenario.ControlledAgentData;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;

/**
 * Implementation of a gun.
 * 
 * @author marcel
 * 
 */
public class Gun extends Tool {
	ControlledAgentData agent;
	Environment env;
	int ammunition;

	public Gun(Environment env, ControlledAgentData agent) {
		super(Color.RED);
		this.agent = agent;
		this.env = env;
		this.ammunition = 20;
	}

	public Gun() {
		super(null);
	}

	@Override
	public double use() {
		if (this.ammunition > 0) {
			// create a gun projectile
			DiskType ballType = new DiskType(new GunProjectile());
			ObjectConstructor ballConstructor = env.createObjectConstructor();
			ballConstructor.setRoot(1.0, ballType);
			double radiusAgent = agent.getDiskComplex().getDisks().get(0)
					.getRadius();
			double radiusBullet = 0.5;

			// determine the projectiles place
			double angle = agent.getDiskComplex().getCoordinates().getAngle();
			DiskComplex bulletComplex = ballConstructor.createDiskComplex(
					agent.getDiskComplex().getCenterx() + radiusBullet
							+ Math.cos(angle) * radiusAgent * 1.3, agent
							.getDiskComplex().getCentery()
							+ radiusBullet
							+ Math.sin(angle) * radiusAgent * 1.3, 0.0,
					radiusBullet);

			// apply impuls to it
			if (bulletComplex != null) {
				Disk bullet = bulletComplex.getDisks().get(0);
				GunProjectileEventHandler projectileHandler = new GunProjectileEventHandler(
						bullet, env, agent);
				bullet.addEventHandler(projectileHandler);
				bulletComplex.applyImpulse(5 * Math.cos(angle) * radiusAgent, 5
						* Math.sin(angle) * radiusAgent,
						bulletComplex.getCenterx(), bulletComplex.getCentery());

				// decrease ammunition
				this.ammunition--;

			}
		}
		return 0;
	}

	@Override
	public boolean isConsumable() {
		return false;
	}

	public void incAmmunition(int value) {
		this.ammunition += value;
	}

}
