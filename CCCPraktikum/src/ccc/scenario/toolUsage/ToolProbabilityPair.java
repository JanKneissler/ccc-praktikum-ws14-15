package ccc.scenario.toolUsage;

/**
 * Class that combines a tool with a probability. Used to spawn tools.
 * 
 * @author marcel
 * 
 */

public class ToolProbabilityPair {
	private Tool tool;
	private double probability;
	
	public ToolProbabilityPair(Tool tool, double probability){
		this.tool = tool;
		this.probability = probability;
	}
	
	public double getProbability() {
		return probability;
	}
	public void setProbability(double probability) {
		this.probability = probability;
	}
	public Tool getTool() {
		return tool;
	}
}
