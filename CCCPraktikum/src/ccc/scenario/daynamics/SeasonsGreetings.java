package ccc.scenario.daynamics;

import java.awt.Color;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import ccc.scenario.utils.ConsumableConsumptionHandler;
import ccc.scenario.utils.Distribution;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actuators.Consumer.ConsumptionEventType;
import diskworld.actuators.Mover;
import diskworld.actuators.Consumer;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.Sensor;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class SeasonsGreetings extends DiskWorldScenario {

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Constants 						 													 */
	///////////////////////////////////////////////////////////////////////////////////////////

	// The maximum range of the disk sensor (i.e. the range under perfect lighting conditions).
	private static final int MAXIMAL_VISIBILITY_RANGE = 20;
	
	// Minimum and maximum sizes of the environment
	private static final int ENV_MIN_SIZE_X = 50;
	private static final int ENV_MAX_SIZE_X = 100;
	private static final int ENV_MIN_SIZE_Y = 50;
	private static final int ENV_MAX_SIZE_Y = 100;
	
	// Rock sizes and counts
	private static final double ROCK_MIN_SIZE = 0.6;
	private static final double ROCK_MAX_SIZE = 1.5;
	private static final int ROCK_MIN_COUNT = 40;
	private static final int ROCK_MAX_COUNT = 60;
	
	// Agent size
	private static final double AGENT_MIN_SCALE = 0.8;
	private static final double AGENT_MAX_SCALE = 1.3;
	
	// Color of the floor cells at twilight and night.
	private static final FloorCellType GRASS_TWILIGHT = new FloorCellType(1.0 * 0.3, Color.GREEN.darker().darker());
	private static final FloorCellType GRASS_NIGHT = new FloorCellType(1.0 * 0.3, Color.GREEN.darker().darker().darker().darker());
	
	// Number of plants
	private static final int APPLE_TREE_MIN_COUNT = 8;
	private static final int APPLE_TREE_MAX_COUNT = 16;
	private static final int CHERRY_TREE_MIN_COUNT = 6;
	private static final int CHERRY_TREE_MAX_COUNT = 12;
	private static final int FIG_TREE_MIN_COUNT = 5;
	private static final int FIG_TREE_MAX_COUNT = 9;
	private static final int BELLADONNA_PLANT_MIN_COUNT = 4;
	private static final int BELLADONNA_PLANT_MAX_COUNT = 8;

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 																				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	private Environment environment;
	private ExternalEnvironment extEnv;
	private double startingTimeStep;
	private double nextLogTimeStep = -1;
	private IlluminationDependentClosestDiskSensor sensor;
	private List<Plant> plants = new LinkedList<Plant>();

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "SeasonsGreetings";
	}

	@Override
	public String getDescription() {
		return "Times change, so does the world.";
	}

	@Override
	public String getAuthors() {
		return "Samuel Bitschnau";
	}

	@Override
	public String getContactEmail() {
		return "samuel.bitschnau@student.uni-tuebingen.de";
	}

	@Override
	public String getVersion() {
		return "0.10";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 												         						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario          						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X, ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex, double sizey) {
		// create bounding walls at the 4 sides of the environment 
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);
		// Add more walls here if needed
		return res;
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		// this scenario has no limitation of number of time steps, set a value > 0 to enable time limitation
		return UNLIMITED_TIME;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid red...
		res.getColorScheme().gridColor = Color.RED;
		// enable/disable options if needed: disable skeleton display...
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		// always one controlled agent
		return 1;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Inserts groups of disks at random positions in the environment.
	 * @param diskType DiskType			The type of the disks to insert.
	 * @param numDisks int				The number of disks to insert.
	 * @param zLevel int				The zLevel at which the disks are located.
	 * @param environment Environment	The environment into which the disks are inserted.
	 * @param random Random				The random number generator object to use.
	 * @param minSize double			The minimum disk size.
	 * @param maxSize double			The maximum disk size.
	 * @param lambdaNumberInRow			The factor lambda in the exponential distribution for calculating
	 * 									how many disks to place into one group.
	 */
	protected void insertDiskGroupsRandomly(DiskType diskType,
			int numDisks, int zLevel, Environment environment, Random random,
			double minSize, double maxSize, double lambdaNumberInRow) {
		ObjectConstructor objConstructor = environment.createObjectConstructor();
		
		int num = 0;
		do {
			int numInRow = (int)Math.ceil(Distribution.exponential(random, lambdaNumberInRow));
			
			for (int i = 0; i < 50; i++) { // try 50 times to create a cluster with numInRow disks
				// random position and size
				double posx = ScenarioUtils.uniform(random, 0.0, environment.getMaxX());
				double posy = ScenarioUtils.uniform(random, 0.0, environment.getMaxY());
				
				objConstructor.setRoot(ScenarioUtils.uniform(random, minSize, maxSize), true, diskType);
				
				DiskComplex complex = objConstructor.createDiskComplex(posx, posy, 0.0, 1.0);
				if (complex != null) {				
					Disk prevDisk = complex.getDisks().get(0);
					for (int j = 1; j < Math.min(numInRow, numDisks-num); j++) {
						double size = ScenarioUtils.uniform(random, minSize, maxSize);
						prevDisk = prevDisk.attachDisk(ScenarioUtils.uniform(random, 0, 2*Math.PI), size, diskType);
					}
					
					if (!environment.withdrawDueToCollisions(prevDisk.getDiskComplex())) {
						for (Disk d : prevDisk.getDiskComplex().getDisks()) {
							d.setZLevel(zLevel);
							num++;
						}
						break;
					}
				}
			}
		} while (num < numDisks);
	}
	
	/**
	 * Floor type for the current timestep.
	 * @return The floor type for the current time of a day.
	 */
	protected FloorCellType getFloorCellTypeForCurrentTime() {
		double inclination = extEnv.sunAngle(environment.getTime());
		
		if (inclination > Math.toRadians(-6)) { // end of civil twilight and
			return FloorCellType.GRASS;         // beginning of nautical twilight
		}
		else if (inclination >  Math.toRadians(-15)) {
			return GRASS_TWILIGHT; // -15°: half-way into astronomical twilight
		} else {
			return GRASS_NIGHT;
		}
	}
	
	
	@Override
	protected void initialize(Environment environment, Random random) {
		//store environment for later use
		this.environment = environment;

		// initialize external environment
		extEnv = new ExternalEnvironment(Math.toRadians(48.52)); // latitude of Tuebingen, Germany
		startingTimeStep = ScenarioUtils.uniform(random, 0, ExternalEnvironment.DAYS_PER_YEAR * ExternalEnvironment.TIMESTEPS_PER_DAY);
		environment.setTime(startingTimeStep);
		
		// Initialize the floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, getFloorCellTypeForCurrentTime());
			}
		}
		
		// create objects (DiskComplexes that are not agents)
		DiskType rock = new DiskType(new DiskMaterial(1.0, 0.7, 0.6, 0.4, Color.DARK_GRAY));
		this.insertDiskGroupsRandomly(rock, ScenarioUtils.uniform(random, ROCK_MIN_COUNT, ROCK_MAX_COUNT), 0, environment, random, ROCK_MIN_SIZE, ROCK_MAX_SIZE, 0.25);
		
		try {
			for (int i = 0; i < ScenarioUtils.uniform(random, APPLE_TREE_MIN_COUNT, APPLE_TREE_MAX_COUNT); i++) {
				plants.add(new AppleTree(ScenarioUtils.uniform(random, 0, ExternalEnvironment.DAYS_PER_YEAR*2), environment, extEnv, random));
			}
			for (int i = 0; i < ScenarioUtils.uniform(random, CHERRY_TREE_MIN_COUNT, CHERRY_TREE_MAX_COUNT); i++) {
				plants.add(new CherryTree(ScenarioUtils.uniform(random, 0, ExternalEnvironment.DAYS_PER_YEAR*4), environment, extEnv, random));
			}
			for (int i = 0; i < ScenarioUtils.uniform(random, FIG_TREE_MIN_COUNT, FIG_TREE_MAX_COUNT); i++) {
				plants.add(new FigTree(ScenarioUtils.uniform(random, 0, ExternalEnvironment.DAYS_PER_YEAR*3), environment, extEnv, random));
			}
			for (int i = 0; i < ScenarioUtils.uniform(random, BELLADONNA_PLANT_MIN_COUNT, BELLADONNA_PLANT_MAX_COUNT); i++) {
				plants.add(new BelladonnaPlant(ScenarioUtils.uniform(random, 0, ExternalEnvironment.DAYS_PER_YEAR*2.5), environment, extEnv, random));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] agentStates) {
		// sensors and actuators
		Set<DiskMaterial> invisibleMaterials = new HashSet<DiskMaterial>();
		invisibleMaterials.add(DiskMaterial.METAL);
		sensor = new IlluminationDependentClosestDiskSensor(environment, 0.0, Math.toRadians(200), 1.5, MAXIMAL_VISIBILITY_RANGE, invisibleMaterials, 
				true, true, false, null, 12);
		Actuator mover = new Mover(0.6, 0.9, 1, 0.002, 0.05);
		Actuator consumer = new Consumer(environment, 1.0, ConsumptionEventType.COLLISION_OR_RANGE, 0.001, new ConsumableConsumptionHandler(agentStates[0], environment));

		// create disk types 
		DiskType body = new DiskType(DiskMaterial.RUBBER.withColor(Color.YELLOW), mover, new Sensor[] {sensor});
		DiskType mouth = new DiskType(DiskMaterial.RUBBER.withColor(Color.ORANGE), consumer, new Sensor[]{});
		//mover.addJointAction(2, ControlType.CHANGE, ActionType.SPIN);

		// now create constructor specifying relative arrangement of disks
		AgentConstructor ac = new AgentConstructor(environment);
		ac.setRoot(1.0, body);
		ac.addItem(0, 0, 0, 0.2, mouth);

		// now repeat creating the agent at a random place
		ControlledAgentData agent = null;
		// random scale factor controlling the size (using the random object for agent)
		double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE, AGENT_MAX_SCALE);
		double initialEnergyLevel = 1.0; // start with full energy
		agentStates[0].resetEnergyLevel(initialEnergyLevel);
		do {
			// random position and orientation (using the random object for environment)
			double posx = ScenarioUtils.uniform(eRandom, 0.0, environment.getMaxX());
			double posy = ScenarioUtils.uniform(eRandom, 0.0, environment.getMaxY());
			double orientation = ScenarioUtils.uniform(eRandom, 0.0, 2 * Math.PI);
			agent = ac.createControlledAgent(posx, posy, orientation, scaleFactor);
		} while (agent == null);
				
		return new ControlledAgentData[] { agent };
	}

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		// no npc (neutral) agents
		return new NPCAgentData[0];
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, update necessary (besides the DiskWorldPhysics here)			 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {
		double now = environment.getTime();
		
		// Update all plants and remove dead ones.
		Iterator<Plant> iter = plants.iterator();
		while (iter.hasNext()) {
			Plant plant = iter.next();
			if (now > plant.endOfLifeTimeStep) {
				plant.removeAllDisks();
				iter.remove();
			} else {
				plant.update();
			}
		}
		
		// Update floor tiles according to the time of the day.
		Floor floor = environment.getFloor();
		if (floor.getType(0, 0) != getFloorCellTypeForCurrentTime()) {
			for (int x = 0; x < floor.getNumX(); x++) {
				for (int y = 0; y < floor.getNumY(); y++) {
					floor.setType(x, y, getFloorCellTypeForCurrentTime());
				}
			}
		}
		
		// Set visibility distance for disk sensor based on the current lighting conditions.
		sensor.setMaxRangeAbsolute(MAXIMAL_VISIBILITY_RANGE*extEnv.visibleDistanceFactor(now));
		
		// Write to log if the equinox or solstice has been reached.
		if (nextLogTimeStep == -1) {
			double mostRecent = extEnv.getMostRecentEquinoxSolsticeTimeStep(startingTimeStep);
			log().println("Starting " + extEnv.getTimespanDays(startingTimeStep, mostRecent) +
					" days after " + extEnv.getMostRecentEquinoxSolsticeName(startingTimeStep));
			nextLogTimeStep = mostRecent + ExternalEnvironment.TIMESTEPS_PER_DAY * ExternalEnvironment.DAYS_PER_YEAR / 4 + 0.1;
		} else if (now >= nextLogTimeStep) {
			log().println("Year " + extEnv.getTimespanYears(now, startingTimeStep) + ": " +
					extEnv.getMostRecentEquinoxSolsticeName(now));
			nextLogTimeStep += ExternalEnvironment.TIMESTEPS_PER_DAY * ExternalEnvironment.DAYS_PER_YEAR / 4;
		}
		
//		log().println(
//				"Day " + extEnv.getDay(now) + ", min " + extEnv.getMinute(now) +
//					": inclination angle = " + Math.toDegrees(extEnv.sunAngle(now))
//		);
//		log().println("  Visible distance factor: " + extEnv.visibleDistanceFactor(now));
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but             */
	/* dedicated to this scenario are implemented here          							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return null; //ExampleReferenceController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI         				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			final boolean returnAngleToZero = true;
			final double turningAngle = returnAngleToZero ? 0.05 : 0.02;
			final double speedChange = 0.05;

			if (returnAngleToZero)
				always(1, 0.0);

			// Move forwards
			add(java.awt.event.KeyEvent.VK_NUMPAD8, 0, speedChange, true);
			add(java.awt.event.KeyEvent.VK_W, 0, speedChange, true);

			// Move backwards
			add(java.awt.event.KeyEvent.VK_NUMPAD5, 0, -speedChange, true);
			add(java.awt.event.KeyEvent.VK_S, 0, -speedChange, true);

			// Rotate left
			add(java.awt.event.KeyEvent.VK_NUMPAD4, 1, turningAngle, !returnAngleToZero);
			add(java.awt.event.KeyEvent.VK_A, 1, turningAngle, !returnAngleToZero);

			// Rotate right
			add(java.awt.event.KeyEvent.VK_NUMPAD6, 1, -turningAngle, !returnAngleToZero);
			add(java.awt.event.KeyEvent.VK_D, 1, -turningAngle, !returnAngleToZero);

			// Return speed and angle to zero
			add(java.awt.event.KeyEvent.VK_NUMPAD0, 0, 0.0, false);
			add(java.awt.event.KeyEvent.VK_NUMPAD0, 1, 0.0, false);
			add(java.awt.event.KeyEvent.VK_0, 0, 0.0, false);
			add(java.awt.event.KeyEvent.VK_0, 1, 0.0, false);
			
			// Consume
			add(java.awt.event.KeyEvent.VK_E, 2, 1, false);
			add(java.awt.event.KeyEvent.VK_Q, 2, 0, false);

			setLogMessage("Use the following keys to control the agent:\n" +
					"a / numpad4 -> rotate left \n" +
					"d / numpad6 -> rotate right\n" +
					"w / numpad8 -> increase speed\n" +
					"s / numpad5 -> reduce speed\n" +
					"0 / numpad0 -> stop\n" +
					"e -> eat (consume disks)\n" +
					"q -> quit eating (don't consume disks)");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(SeasonsGreetings.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

	@Override
	protected double getHardnessLevel(double time) {
		// TODO Auto-generated method stub
		return 0;
	}
}
