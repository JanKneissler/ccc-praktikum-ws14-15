package ccc.scenario.daynamics;

import ccc.scenario.utils.Distribution;

/**
 * Simulation of external environment, in particular seasons, sun angle and irradiance.
 * @author Samuel Bitschnau
 */
public class ExternalEnvironment {
	
	public static final int DAYS_PER_YEAR = 40;
	public static final int HOURS_PER_DAY = 24;
	public static final int MINUTES_PER_DAY = HOURS_PER_DAY*60;
	public static final int TIMESTEPS_PER_DAY = 12;
	
	// Solar radiation per unit area at one astronomical unit from the sun
	public static final double SOLAR_CONSTANT = 1.353;
	// Angle between equatorial plane and orbital plane; 23.45° for earth
	public static final double AXIAL_TILT = Math.toRadians(23.45);
	
	protected int day = 0;
	protected int hour = 0;
	
	protected double latitude;

	/**
	 * Constructor
	 * @param double	Latitude in radians
	 */
	public ExternalEnvironment(double latitude) {
		this.latitude = latitude;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Convenience methods
	
	/**
	 * Day in year (rounded down).
	 * @param timestep double	Time point
	 */
	public int getDay(double timestep) {
		return ((int)Math.floor(timestep/TIMESTEPS_PER_DAY))%DAYS_PER_YEAR;
	}
	
	/**
	 * Day in year exact.
	 * @param timestep double	Time point
	 */
	public double getDayFloat(double timestep) {
		return (timestep/TIMESTEPS_PER_DAY)%DAYS_PER_YEAR;
	}
	
	/**
	 * Year (rounded down).
	 * @param timestep double	Time point
	 */
	public int getYear(double timestep) {
		return (int)Math.floor(timestep/TIMESTEPS_PER_DAY/DAYS_PER_YEAR);
	}
	
	/**
	 * Time step for the last time day occurred.
	 * @param day double			Day in the year to look at
	 * @param timeStepNow double	The current time step
	 */
	public double getTimeStepLastTime(double day, double timeStepNow) {
		double dayNow = getDayFloat(timeStepNow);
		
		if (dayNow >= day) { // day occurred previously this year
			return timeStepNow + (day - dayNow) * TIMESTEPS_PER_DAY;
		} else { // day hasn't occurred this year yet
			return timeStepNow + ((day - DAYS_PER_YEAR) - dayNow) * TIMESTEPS_PER_DAY;
		}
	}
	
	/**
	 * Time step for the next time day will occur.
	 * @param day double			Day in the year to look at
	 * @param timeStepNow double	The current time step
	 */
	public double getTimeStepNextTime(double day, double timeStepNow) {
		double dayNow = getDayFloat(timeStepNow);
		
		if (dayNow <= day) { // day will occur still in the same year
			return timeStepNow + (day - dayNow) * TIMESTEPS_PER_DAY;
		} else { // day will occur the year afterwards
			return timeStepNow + ((day + DAYS_PER_YEAR) - dayNow) * TIMESTEPS_PER_DAY;
		}
	}
	
	/**
	 * Minute in day (rounded down).
	 * @param timestep double	Time point
	 */
	public int getMinute(double timestep) {
		return (int)Math.floor((timestep%TIMESTEPS_PER_DAY)*(MINUTES_PER_DAY/TIMESTEPS_PER_DAY));
	}
	
	/**
	 * Difference in years between the two time points (rounded down and absolute value)
	 * @param timestep1 double	One time point.
	 * @param timestep2 double	Another time point.
	 * @return int				Absolute difference in whole years (rounded down). 
	 */
	public int getTimespanDays(double timestep1, double timestep2) {
		return Math.abs((int)Math.floor((timestep1 - timestep2))/TIMESTEPS_PER_DAY);
	}
	
	/**
	 * Difference in years between the two time points (rounded down and absolute value)
	 * @param timestep1 double	One time point.
	 * @param timestep2 double	Another time point.
	 * @return int				Absolute difference in whole years (rounded down). 
	 */
	public int getTimespanYears(double timestep1, double timestep2) {
		return (int)Math.floor(getTimespanDays(timestep1, timestep2) / DAYS_PER_YEAR);
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Model of seasonal changes due to rotation of the earth around the sun
	//   based on Vyacheslav Khavrus and Ihor Shelevytsky:
	//   Geometry and the physics of seasons
	//   https://sites.google.com/site/khavrus/public-activities/seasons
	//
	// This model assumes a circular rather than an elliptical orbit, as the
	// low eccentricity of the elliptical orbit has no big impact.
	
	/**
	 * Declination angle between sun and the celestial equator plan
	 *
	 * @param day int	The day in the year starting from spring equinox.
	 * @return double	Angle in radians
	 */
	public double declinationAngle(int day) {
		return AXIAL_TILT*Math.sin(2*Math.PI*day/DAYS_PER_YEAR);
	}
	
	/**
	 * Inclination angle between sun and observer located at 'latitude'.
	 * Is 0 at sunrise and sunset, negative at night.
	 * At midday (highest inclination angle for the day):
	 *   = 90° - AXIAL_TILT				for spring equinox
	 *   = 90° - AXIAL_TILT + latitude	for summer solstice
	 *   = 90° - AXIAL_TILT - latitude	for summer solstice
	 * 
	 * @param day int		The day in the year starting from spring equinox.
	 * @param minutes int	Minutes since the start of the day (midnight).
	 * @return double		Angle in radians
	 */
	public double inclinationAngle(int day, int minutes) {
		return Math.asin(Math.sin(latitude)*Math.sin(declinationAngle(day)) -
				(Math.cos(latitude)*Math.cos(declinationAngle(day))*Math.cos(2*Math.PI*minutes/MINUTES_PER_DAY)));
	}
	
	/**
	 * Convenience method for inclinationAngle() with timestep
	 * instead of day and minutes
	 * 
	 * @param timestep double	Current time in the simulation environment
	 * @return double			Angle in radians
	 */
	public double sunAngle(double timestep) {
		return inclinationAngle(getDay(timestep), getMinute(timestep));
	}
	
	/**
	 * Optical air mass (path length of sunlight through atmosphere)
	 * according to the approximation formula by Karsten and Young (1989).
	 * @param inclinationAngle double	Inclination angle in radians.
	 */
	public double opticalAirMass(double inclinationAngle) {
		double delta = Math.toDegrees(inclinationAngle);
		return 1/(Math.sin(delta)+
				0.50572*Math.pow(6.07995+delta, -1.6364));
	}
	
	/**
	 * Direct beam (not scattered or absorbed) irradiance as a
	 * function of optical air mass.
	 * 
	 * @param inclinationAngle double	Inclination angle in radians
	 */
	public double irradianceDirectBeam(double inclinationAngle) {
		double exponent = Math.pow(opticalAirMass(inclinationAngle), 0.678);
		return SOLAR_CONSTANT*Math.pow(0.7, exponent);
	}
	
	/**
	 * Global irradiance (direct and diffuse) depending on weather type
	 * 
	 * The diffuse irradiance is about 10% of the direct beam irradiance
	 * on a sunny day. On a cloudy day, the diffuse irradiance is about
	 * 20% of this direct beam irradiance value, but no direct beam
	 * irradiance occurs.
	 * @param inclinationAngle double	Inclination angle in radians.
	 * @param clearSky boolean			Whether the sky is clear or not (overcast, cloudy).
	 * @return
	 */
	public double irradianceGlobal(double inclinationAngle, boolean clearSky) {
		if (clearSky)
			return irradianceDirectBeam(inclinationAngle) * 1.1;
		else
			return irradianceDirectBeam(inclinationAngle) * 0.2;
	}
	
	/**
	 * Timestep of last equinox or solstice
	 * @param timestep double	Timestep for which the most recent equinox/solstice should be computed
	 * @return double			Timestep of last equinox/solstice
	 */
	public double getMostRecentEquinoxSolsticeTimeStep(double timestep) {
		double dayInYear = getDayFloat(timestep);
		int quarter = (int)Math.floor(dayInYear / (DAYS_PER_YEAR / 4));
		return getTimeStepLastTime(quarter*DAYS_PER_YEAR, timestep);
	}
	
	/**
	 * Name of most recent equinox or solstice
	 * @param timestep double	Timestep for which the most recent equinox/solstice should be computed
	 * @return string 			Name of last equinox/solstice
	 */
	public String getMostRecentEquinoxSolsticeName(double timestep) {
		double dayInYear = getDayFloat(timestep);
		int quarter = (int)Math.floor(dayInYear / (DAYS_PER_YEAR / 4));
		
		switch(quarter) {
		case 0:
			return "spring equinox";
		case 1:
			return "summer solstice";
		case 2:
			return "autumn equinox";
		case 3:
			return "winter solstice";
		default:
			return "something went wrong...";
		}
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Effect of environment on sensors
	
	/**
	 * Effect of light conditions at the given timestep on visibility distance
	 * @param timeStep double	The timestep for which the visibility factor should be computed
	 * @return double			Factor for visibility distance (between 0.2 and 1)
	 */
	public double visibleDistanceFactor(double timeStep) {
		return 0.2 + 0.8*Distribution.logistic(sunAngle(timeStep), Math.toRadians(-9.0), 40.0);
	}
	

}
