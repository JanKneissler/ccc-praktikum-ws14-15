package ccc.scenario.daynamics;

import java.util.Random;
import ccc.scenario.utils.Distribution;

/**
 * Instantiations of a feature.
 * The instances of the feature are derived as random instances of a Feature,
 * based on a normal distribution for
 *   1. the mean value of the feature instances and
 *   2. the specific random feature instance values.
 * @author Samuel Bitschnau
 */
public class FeatureInstance extends Feature {
	
	protected Random random;
	protected final double instanceMinimumValue;
	private double meanInstance;
	
	// Constructors
	
	/**
	 * Creates a FeatureInstance instance, based on the feature description
	 * provided in feature of type Feature using the rng random.
	 * @param feature Feature				The feature description
	 * @param random Random					The random number generator to use
	 * @param instanceMinimumValue double	Minimum value for instance (e.g. to prevent negative or zero values)
	 */
	public FeatureInstance(Feature feature, Random random, double instanceMinimumValue) {
		super(feature.meanPopulation, feature.sdMeanPopulation, feature.sdForTimePoint);
		
		this.random = random;
		this.instanceMinimumValue = instanceMinimumValue;
		this.meanInstance = Math.max(instanceMinimumValue,  Distribution.normal(random, meanPopulation, sdMeanPopulation));
	}
	
	/**
	 * Creates a FeatureInstance instance, based on the feature description
	 * provided in feature of type Feature using the rng random. 
	 * @param feature Feature	The feature description
	 * @param random Random		The random number generator to use
	 */
	public FeatureInstance(Feature feature, Random random) {
		this(feature, random, 0.00001);
	}
	

	// Methods
	
	/**
	 * The mean value of the feature instance normal distribution.
	 * @return double	The mean value of the feature instances.
	 */
	public double getFeatureInstanceMean() {
		return meanInstance;
	}
	
	/**
	 * Set the mean value of the feature instances.
	 * The feature instances are normally distributed around this value.
	 * While the value gets set automatically as a random value based on the
	 * meanPopulation and sdMeanPopulation values of the Feature, setting the
	 * feature instance mean value can be useful when dealing with several
	 * related features, e.g. such that the end date is after the start date.
	 * @param newValue double	The new instance mean value.
	 */
	public void setFeatureInstanceMean(double newValue) {
		meanInstance = newValue;
	}
	
	/**
	 * Get feature instance as normally distributed random variable with
	 * getFeatureInstanceMean()+influence as mean value.
	 * @param influence
	 * @return double	The feature instance value.
	 */
	public double getInstance(double influence) {
		return Math.max(instanceMinimumValue, meanInstance + Distribution.normal(random, influence, sdForTimePoint));
	}
	
	/**
	 * Get feature instance as normally distributed random variable.
	 * @return double	The feature instance value.
	 */
	public double getInstance() {
		return getInstance(0.0);
	}

}