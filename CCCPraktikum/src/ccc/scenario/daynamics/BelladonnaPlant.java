package ccc.scenario.daynamics;

import java.awt.Color;
import java.util.Random;

import ccc.scenario.utils.ConsumableDiskMaterial;
import diskworld.DiskMaterial;
import diskworld.Environment;

public class BelladonnaPlant extends Plant {
	
	final static double year = ExternalEnvironment.DAYS_PER_YEAR;
	
	public final static ConsumableDiskMaterial BELLADONNA = 
			new ConsumableDiskMaterial(0.3, 0.7, 0.5, 0.8, new Color(21, 24, 5), -0.15);

	public BelladonnaPlant(double initialAge, Environment environment, ExternalEnvironment externalEnv, Random random) throws Exception {
		super(BELLADONNA, new DiskMaterial(0.8, 0.7, 0.4, 0.7, new Color(97, 104, 71)),
				initialAge, environment, externalEnv, random,
				new Feature(9, 2), // yield
				new Feature(0.2, 0.02), // initial stem size
				new Feature(0.45, 0.08), // final stem size
				new Feature(0.04, 0.002), // initial fruit size
				new Feature(0.26, 0.005, 0.003), // final fruit size
				new Feature[]{ new Feature(year * 0.28, year * 0.01, year*0.02) }, // fruit start dates - day 0: spring equinox
				new Feature[]{ new Feature(year * 0.47, year * 0.03, year*0.02) }, // fruit ripe dates - day 0: spring equinox
				new Feature(year * 0.15, year * 0.03), // durability attached to plant
				new Feature(year * 0.02, year * 0.005), // durability detached from plant
				new Feature(year * 5, year * 1.8), // maximum age
				new Feature(year * 2.2, year * 0.4)); // growth end of plant
	}
}