package ccc.scenario.daynamics;


/**
 * General class for specifying a feature at the population level.
 * This population feature specification is often used for deriving specific
 * feature instances of type FeatureInstance.
 * @author Samuel Bitschnau
 */
public class Feature {
	
	// Mean value of mean feature instance values.
	protected double meanPopulation; 
	// Standard deviation for generating mean feature instance value (around meanPopulation).
	protected double sdMeanPopulation;
	// Standard deviation for generating feature instance values (around given population mean value).
	protected double sdForTimePoint; 
	
	// Constructors
	
	/**
	 * Feature description, used for generating FeatureInstances.
	 * A FeatureInstance object can be used to generate randomly distributed
	 * feature values around a mean m with standard deviation sdForTimePoint,
	 * whereby m is itself a random value from a normal distribution with a
	 * mean value mean and standard deviation sdMean. 
	 * @param mean double			Mean value of feature instance mean values.
	 * @param sdMean double			Standard deviation of feature instance mean values.
	 * @param sdForTimePoint double	Standard deviation of feature instance values.
	 */
	public Feature(double mean, double sdMean, double sdForTimePoint) {
		this.meanPopulation = mean;
		this.sdMeanPopulation = sdMean;
		this.sdForTimePoint = sdForTimePoint;
	}
	
	/**
	 * Feature description convenience constructor, with common variance for
	 * the feature instance mean value and feature instance value distribution.
	 * @param mean double	Mean value of feature instance mean values.
	 * @param sd double		Standard deviation of feature instance (mean) values.
	 */
	public Feature(double mean, double sd) {
		this(mean, sd, sd);
	}
	
	
}
