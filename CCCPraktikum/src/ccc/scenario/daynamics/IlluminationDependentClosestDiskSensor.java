package ccc.scenario.daynamics;

import java.util.Set;

import diskworld.DiskMaterial;
import diskworld.Environment;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.sensors.DiskMaterialResponse;


public class IlluminationDependentClosestDiskSensor extends ClosestDiskSensor {
	
	private double centerAngle;
	private double openingAngle;
	private double minRangeRelativeToRadius;
	private double maxRangeAbsolute;

	public IlluminationDependentClosestDiskSensor(Environment environment,
			double centerAngle, double openingAngle, double minRangeRelative,
			double maxRangeAbsolute, Set<DiskMaterial> invisibleMaterials,
			boolean measurePosition, boolean measureDistance,
			boolean measureWidth, DiskMaterialResponse materialResponse,
			int maxNumDisks) {
		super(environment, centerAngle, openingAngle, minRangeRelative,
				maxRangeAbsolute, invisibleMaterials, measurePosition, measureDistance,
				measureWidth, materialResponse, maxNumDisks);
		this.centerAngle = centerAngle;
		this.openingAngle= openingAngle;
		this.minRangeRelativeToRadius= minRangeRelative;
		this.maxRangeAbsolute = maxRangeAbsolute;
	}
	
	/**
	 * Change the maximum range 
	 * @param newMaxRange double	Absolute viewing distance of the sensor
	 */
	public void setMaxRangeAbsolute(double newMaxRangeAbsolute) {
		this.maxRangeAbsolute = newMaxRangeAbsolute;
		super.setShape(centerAngle, openingAngle,minRangeRelativeToRadius, maxRangeAbsolute);
	}
	
}
