package ccc.scenario.daynamics;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.scenario.utils.ConsumableDiskMaterial;
import ccc.scenario.utils.Distribution;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actions.DiskModification;

public class Plant {

	class Fruit {
		public boolean attached;
		public double endOfLifeTimeStep;
		public double startSize;
		public double endSize;
		public double fruitingStartTimeStep;
		public double ripeningTimeStep;
		public Disk disk = null;

		Fruit(boolean attached, double startSize, double endSize, double fruitingStartTimeStep,
				double ripeningTimeStep, double endOfLifeTimeStep) {
			this.attached = attached;
			this.startSize = startSize;
			this.endSize = endSize;
			this.fruitingStartTimeStep = fruitingStartTimeStep;
			this.ripeningTimeStep = ripeningTimeStep;
			this.endOfLifeTimeStep = endOfLifeTimeStep;
		}

		public void setDisk(Disk disk) {
			this.disk = disk;
		}
	}

	// Minimum difference in disk size (radius) for considering updating it (stem, fruits).
	protected static final double MINIMUM_RADIUS_DELTA = 0.05;
	// Minimum length of fruiting season (ripening date - fruit start date): 1 day
	protected static final double MINIMUM_FRUIT_SEASON_LENGTH = 1;
	// Probability that disk size of fruit gets updated (when size difference is at least MINIMUM_RADIUS_DELTA)
	protected static final double FRUIT_SIZE_CHANGE_PROBABILITY = 0.3;
	// Lag 1 autocorrelation of yield, i.e. how strongly the current yield is influenced by the previous yield.
	protected static final double YIELD_AUTOCORRELATION = 0.3;

	// General stuff
	protected Environment env;
	protected ExternalEnvironment extEnv;
	protected Random random;
	protected DiskType diskType;

	// Counts and sizes
	protected FeatureInstance yield;
	protected FeatureInstance initialFruitSize;
	protected FeatureInstance finalFruitSize;
	protected double initialStemSize;
	protected double finalStemSize;

	// Timing
	protected FeatureInstance fruitStartDate[]; // can be multiple per year
	protected FeatureInstance ripeningDate[]; // can be multiple per year
	protected FeatureInstance durabilityOnPlant;
	protected FeatureInstance durabilityOffPlant;
	protected double growthEndPlant;

	// General information about plant instance
	protected final double birthTimeStep; // time step when the plant appeared -> to calculate age
	protected final double endOfLifeTimeStep; // time step when the plant dies

	// Plant-wide current seasonal information
	protected int currentYield = 0; // yield in current season - access through getYield()
	protected boolean currentlyInSeason; // season number (when current yield was last changed)

	// The disks and additional information in case of the fruits
	protected Disk stem;
	protected List<Fruit> fruits = new LinkedList<Fruit>();

	/**
	 * Constructor for plant object
	 * 
	 * @param fruitMaterial
	 *            ConsumableDiskMaterial The disk material for the fruits.
	 * @param stemMaterial
	 *            DiskMaterial The disk material for the stem.
	 * @param initialAge
	 *            double The age of the plant at the start (in timesteps).
	 * @param environment
	 *            Environment The environment the plant is placed in.
	 * @param externalEnv
	 *            ExternalEnvironment The external environment (seasonal information).
	 * @param random
	 *            Random The random number generator object to use.
	 * @param yield
	 *            Feature Description of the fruit yield (number of fruits per season).
	 * @param initialStemSize
	 *            Feature Description of the stem size at age 0.
	 * @param finalStemSize
	 *            Feature Description of the stem size at the end of the plant growth.
	 * @param initialFruitSize
	 *            Feature Description of initial fruit sizes.
	 * @param finalFruitSize
	 *            Feature Description of final fruit sizes.
	 * @param fruitStartDate
	 *            Feature[] Descriptions of the fruit starting dates during the year (can be multiple).
	 * @param ripeningDate
	 *            Feature[] Descriptions of the fruit ripening dates (full size) during the year (can be multiple).
	 * @param durabilityOnPlant
	 *            Feature Description of how long the fruits can keep on the plant after the ripening date (in days).
	 * @param durabilityOffPlant
	 *            Feature Description of how long the fruits can keep away from the plant after the ripening date (in days).
	 * @param maximumAge
	 *            Feature Description of how old the plant will get (in days).
	 * @param growthEndPlant
	 *            Feature Description
	 * @throws Exception
	 */
	public Plant(ConsumableDiskMaterial fruitMaterial, DiskMaterial stemMaterial, double initialAge,
			Environment environment, ExternalEnvironment externalEnv, Random random,
			Feature yield, Feature initialStemSize, Feature finalStemSize,
			Feature initialFruitSize, Feature finalFruitSize,
			Feature fruitStartDate[], Feature ripeningDate[],
			Feature durabilityOnPlant, Feature durabilityOffPlant,
			Feature maximumAge, Feature growthEndPlant) throws Exception {
		this.env = environment;
		this.extEnv = externalEnv;
		this.random = random;
		this.diskType = new DiskType(fruitMaterial, null, null);

		this.yield = new FeatureInstance(yield, random);
		this.initialStemSize = new FeatureInstance(initialStemSize, random).getInstance();
		this.finalStemSize = new FeatureInstance(finalStemSize, random).getInstance();
		this.initialFruitSize = new FeatureInstance(initialFruitSize, random);
		this.finalFruitSize = new FeatureInstance(finalFruitSize, random);

		this.fruitStartDate = new FeatureInstance[fruitStartDate.length];
		for (int i = 0; i < fruitStartDate.length; i++)
			this.fruitStartDate[i] = new FeatureInstance(fruitStartDate[i], random);

		this.ripeningDate = new FeatureInstance[ripeningDate.length];
		for (int i = 0; i < ripeningDate.length; i++)
			this.ripeningDate[i] = new FeatureInstance(ripeningDate[i], random);

		this.durabilityOnPlant = new FeatureInstance(durabilityOnPlant, random);
		this.durabilityOffPlant = new FeatureInstance(durabilityOffPlant, random);

		this.growthEndPlant = new FeatureInstance(growthEndPlant, random).getInstance();
		this.birthTimeStep = env.getTime() - initialAge * ExternalEnvironment.TIMESTEPS_PER_DAY;
		this.endOfLifeTimeStep = birthTimeStep + new FeatureInstance(maximumAge, random).getInstance() * ExternalEnvironment.TIMESTEPS_PER_DAY;

		this.currentlyInSeason = inFruitGrowingSeason();

		// Checking input
		if (fruitStartDate.length != ripeningDate.length)
			throw new Exception("fruitStartDate and ripeningDate arrays differ in length!");

		for (int i = 0; i < fruitStartDate.length; i++) {
			if (this.fruitStartDate[i].getFeatureInstanceMean() >= this.ripeningDate[i].getFeatureInstanceMean()) {
				this.ripeningDate[i].setFeatureInstanceMean(this.fruitStartDate[i].getFeatureInstanceMean() + MINIMUM_FRUIT_SEASON_LENGTH);
			}
		}

		if (this.initialStemSize >= this.finalStemSize) {
			this.finalStemSize = this.initialStemSize;
		}

		insertStem(stemMaterial);

		// Insert fruits for the past year. In update() the too old fruits get removed.
		currentYield = (int) Math.floor(this.yield.getInstance()); // initial yield for mean offset
		for (int seasonIdx = 0; seasonIdx < fruitStartDate.length; seasonIdx++) {
			int idxLastSeason = positiveModulo(seasonIdx - 1, ripeningDate.length);
			double endLastSeason = this.ripeningDate[idxLastSeason].getFeatureInstanceMean();
			double baseTimeStep = extEnv.getTimeStepLastTime(endLastSeason, env.getTime());

			currentYield = (int) Math.floor(this.yield.getInstance(getYieldMeanOffset(currentYield)));
			for (int j = 0; j < currentYield; j++) {
				addFruit(seasonIdx, baseTimeStep);
			}
		}
		update();
	}

	/**
	 * Removes all disks, i.e. stem and fruits.
	 */
	public void removeAllDisks() {
		env.getDiskComplexesEnsemble().removeDiskComplex(stem.getDiskComplex());
	}

	/**
	 * Updates the plant disks:
	 * - stem size (if idealized stem size is more than MINIMUM_RADIUS_DELTA larger)
	 * - adding new fruit disks (if fruit start date is reached)
	 * - fruit size (with FRUIT_SIZE_CHANGE_PROBABILITY probability,
	 * if idealized fruit size is more than MINIMUM_RADIUS_DELTA larger)
	 * - removing disks of dead fruits (if end of life timestep is reached)
	 */
	public void update() {
		DiskComplex complex = stem.getDiskComplex();

		// Check stem size
		if ((stem.getRadius() + MINIMUM_RADIUS_DELTA) < getStemSize()) {
			//			System.out.format("%s  Changing stem size: current = %.3f, new = %.3f at plant age %d days%n", this, stem.getRadius(), getStemSize(), getAgeInDays());
			complex.performModification(new DiskModification(stem, getStemSize()));

			// Adjust fruits to new stem
			for (Fruit fruit : this.fruits) {
				if (fruit.disk != null) {
					updateFruitPosition(fruit.disk, complex);
				}
			}
		}

		// Add fruits for the new season. The disks are only inserted once the fruit start date is reached.
		if (seasonJustEnded()) {
			int oldYield = currentYield;
			currentYield = (int) Math.floor(yield.getInstance(getYieldMeanOffset(oldYield)));

			//			System.out.format("%s  End of previous season; adding fruits: new yield = %d, old yield = %d at plant age %d days%n", this, currentYield, oldYield, getAgeInDays());
			for (int i = 0; i < currentYield; i++) {
				addFruit(getFruitingSeasonIndex(), env.getTime());
			}
		}

		// Create disks for fruits that have reached their fruit start date.
		double now = env.getTime();
		for (Fruit fruit : fruits) {
			if (fruit.disk == null && now > fruit.fruitingStartTimeStep) {
				//				System.out.format("%s  Creating fruit disk: fruit start time step = %f, fruit size = %f at plant age %d days%n", this, fruit.fruitingStartTimeStep, getFruitSize(fruit), getAgeInDays());
				Disk newDisk = stem.attachDisk(ScenarioUtils.uniform(random, -Math.PI, Math.PI), fruit.startSize, 0, diskType);
				newDisk.setZLevel(1);
				fruit.setDisk(newDisk);
			}
		}

		// Check fruit size (also outside of mean growing season, as some
		// individual fruits might have longer growing seasons)
		for (Fruit fruit : this.fruits) {
			if (fruit.disk != null) {
				double idealFruitSize = getFruitSize(fruit);
				if ((fruit.disk.getRadius() + MINIMUM_RADIUS_DELTA) < idealFruitSize &&
						random.nextDouble() < FRUIT_SIZE_CHANGE_PROBABILITY) {
					double diff = idealFruitSize - fruit.disk.getRadius();
					double increase = ScenarioUtils.uniform(random, 0.2 * diff, 1.5 * diff);
					//					System.out.format("%s  Changing fruit size: current = %.3f, increase = %.3f at plant age %d days%n", this, fruit.disk.getRadius(), increase, getAgeInDays());
					complex.performModification(new DiskModification(fruit.disk, fruit.disk.getRadius() + increase)); // changing radius
					updateFruitPosition(fruit.disk, complex);
				}
			}
		}

		// Remove fruits past their life time
		Iterator<Fruit> iter = this.fruits.iterator();
		while (iter.hasNext()) {
			Fruit fruit = iter.next();
			if (fruit.endOfLifeTimeStep <= now) {
				if (fruit.disk != null)
					env.deleteDisk(fruit.disk);
				iter.remove();
			}
		}
	}

	/**
	 * Plant age in years.
	 * 
	 * @return int Plant age in years (rounded down).
	 */
	public int getAgeInYears() {
		return extEnv.getTimespanYears(env.getTime(), birthTimeStep);
	}

	/**
	 * Plant age in days.
	 * 
	 * @return int Plant age in days (rounded down).
	 */
	public int getAgeInDays() {
		return extEnv.getTimespanDays(env.getTime(), birthTimeStep);
	}

	/**
	 * Insert plant stem
	 * 
	 * @param material
	 *            DiskMaterial The material of the plant stem.
	 * @return boolean Whether plant stem was successfully created.
	 */
	protected boolean insertStem(DiskMaterial material) {
		ObjectConstructor objConstructor = env.createObjectConstructor();
		objConstructor.setRoot(getStemSize(), true, new DiskType(material, null, null));

		for (int i = 0; i < 20; i++) {
			// random position and size
			double posx = ScenarioUtils.uniform(random, 0.0, env.getMaxX());
			double posy = ScenarioUtils.uniform(random, 0.0, env.getMaxY());
			DiskComplex complex = objConstructor.createDiskComplex(posx, posy, 0.0, 1.0);
			if (complex != null) {
				stem = complex.getDisks().get(0);
				stem.setZLevel(1);//0);
				return true;
			}
		}
		return false;
	}

	/**
	 * Add fruit to plant
	 * 
	 * @param seasonIndex
	 *            int The index for the season (i.e. for fruitStartDate and ripeningDate)
	 * @param baseTimeStep
	 *            double Timestep from which the next upcoming fruit start / ripening timestep is calculated.
	 * @return boolean Whether fruit could be added.
	 */
	protected boolean addFruit(int seasonIndex, double baseTimeStep) {
		double fruitStartDay = fruitStartDate[seasonIndex].getInstance();
		double ripeningDay = ripeningDate[seasonIndex].getInstance();
		if (ripeningDay < (fruitStartDay + MINIMUM_FRUIT_SEASON_LENGTH))
			ripeningDay = fruitStartDay + MINIMUM_FRUIT_SEASON_LENGTH;

		double initialSize = initialFruitSize.getInstance();
		double finalSize = finalFruitSize.getInstance();
		if (finalSize < initialSize)
			finalSize = initialSize;

		double timeStepFruitStart = extEnv.getTimeStepNextTime(fruitStartDay, baseTimeStep);
		double timeStepRipe = extEnv.getTimeStepNextTime(ripeningDay, baseTimeStep);
		double endOfLife = timeStepRipe + durabilityOnPlant.getInstance() * ExternalEnvironment.TIMESTEPS_PER_DAY;

		return this.fruits.add(new Fruit(true, initialSize, finalSize, timeStepFruitStart, timeStepRipe, endOfLife));
	}

	/**
	 * Current fruiting season in the year
	 * 
	 * @return int Fruiting season index (index for fruitStartDate and ripeningDate)
	 */
	protected int getFruitingSeasonIndex() {
		double dayInYear = extEnv.getDayFloat(env.getTime());
		for (int i = 0; i < fruitStartDate.length; i++) {
			if (dayInYear < fruitStartDate[i].getFeatureInstanceMean()) { // season has not started yet
				if (i == 0) // first season in current year has not started yet
					i = fruitStartDate.length; // select last season in (previous) year
				return i - 1;
			}
		}
		return fruitStartDate.length - 1;
	}

	/**
	 * Whether we are currently in a fruit growing season.
	 * 
	 * @param fruitStartDay
	 *            double Day of the year for which the fruits start (plant mean).
	 * @param ripeningDay
	 *            double Day of the year for which the fruits are ripe (plant mean)
	 * @return boolean Whether we are currently in a fruit growing season
	 */
	protected boolean inFruitGrowingSeason(double fruitStartDay, double ripeningDay) {
		// When in fruit growing season: current date between fruit start date and ripening date.
		// When outside fruit growing season: past last ripening date
		// => check if (current or next) ripening date is before (next) fruit start date
		double now = env.getTime();
		return extEnv.getTimeStepNextTime(ripeningDay, now) < extEnv.getTimeStepNextTime(fruitStartDay, now);
	}

	/**
	 * Whether we are currently in a fruit growing season.
	 * 
	 * @return boolean Whether we are currently in a fruit growing season
	 */
	protected boolean inFruitGrowingSeason() {
		int seasonIndex = getFruitingSeasonIndex();
		double fruitStartDay = fruitStartDate[seasonIndex].getFeatureInstanceMean();
		double ripeningDay = ripeningDate[seasonIndex].getFeatureInstanceMean();
		return inFruitGrowingSeason(fruitStartDay, ripeningDay);
	}

	/**
	 * Current idealized stem size.
	 * 
	 * @return double The current idealized stem size (according to exponential CDF).
	 */
	protected double getStemSize() {
		if (initialStemSize == finalStemSize)
			return initialStemSize; // stem size doesn't change

		double relativeAge = getAgeInDays() / growthEndPlant;
		return initialStemSize + Distribution.exponentialCDF(relativeAge * 5, 0.8) * (finalStemSize - initialStemSize);
	}

	/**
	 * Current idealized fruit size.
	 * 
	 * @param f
	 *            Fruit The fruit for which to get the size.
	 * @return double The current idealized fruit size (according to logistic function).
	 */
	protected double getFruitSize(Fruit f) {
		if (f.fruitingStartTimeStep > env.getTime())
			return 0; // fruit hasn't started to grow yet

		if (f.ripeningTimeStep < env.getTime())
			return f.endSize; // fruit has finished growing

		double relativeTimeInSeason = (env.getTime() - f.fruitingStartTimeStep) / (f.ripeningTimeStep - f.fruitingStartTimeStep);
		return f.startSize + Distribution.logistic(relativeTimeInSeason, 0.5, 8) * (f.endSize - f.startSize);
	}

	/**
	 * Whether the previous fruiting season just ended.
	 * 
	 * @return boolean Whether this is the first time this function was called since the fruiting season ended.
	 */
	protected boolean seasonJustEnded() {
		if (currentlyInSeason != inFruitGrowingSeason()) {
			currentlyInSeason = inFruitGrowingSeason();
			return !currentlyInSeason;
		}
		return false;
	}

	/**
	 * Bias for the yield based on previous yield.
	 * 
	 * @param previousYield
	 *            double Yield in the previous season.
	 * @return double Amount for which to shift the mean yield in order to create a autocorrelation of YIELD_AUTOCORRELATION.
	 */
	protected double getYieldMeanOffset(double previousYield) {
		return (previousYield - yield.getFeatureInstanceMean()) * YIELD_AUTOCORRELATION;
	}

	/**
	 * Updates the position of the fruit disk, such that it touches the stem disk exactly.
	 * 
	 * @param disk
	 *            Disk The fruit disk, for which the position should be adjusted.
	 * @param complex
	 *            DiskComplex The plant disk complex.
	 */
	protected void updateFruitPosition(Disk disk, DiskComplex complex) {
		double dist = stem.getRadius() + disk.getRadius();
		double angle = disk.getAngle();
		complex.performModification(new DiskModification(disk, stem.getX() + (dist * Math.cos(angle)), stem.getY() + (dist * Math.sin(angle))));
	}

	private int positiveModulo(int x, int m) {
		return ((x % m) + m) % m;
	}

}
