package ccc.scenario.daynamics;

import java.awt.Color;
import java.util.Random;

import ccc.scenario.utils.ConsumableDiskMaterial;
import diskworld.DiskMaterial;
import diskworld.Environment;

public class AppleTree extends Plant {
	
	final static double year = ExternalEnvironment.DAYS_PER_YEAR;

	public AppleTree(double initialAge, Environment environment, ExternalEnvironment externalEnv, Random random) throws Exception {
		super(ConsumableDiskMaterial.APPLE, new DiskMaterial(0.8, 0.7, 0.4, 0.7, new Color(87, 57, 20)),
				initialAge, environment, externalEnv, random,
				new Feature(5, 2), // yield
				new Feature(0.6, 0.1), // initial stem size
				new Feature(0.8, 0.15), // final stem size
				new Feature(0.05, 0.01), // initial fruit size
				new Feature(0.4, 0.03, 0.08), // final fruit size
				new Feature[]{ new Feature(year * 0.20, year * 0.03) }, // fruit start dates - day 0: spring equinox
				new Feature[]{ new Feature(year * 0.42, year * 0.06) }, // fruit ripe dates - day 0: spring equinox
				new Feature(year * 0.17, year * 0.05), // durability attached to plant
				new Feature(year * 0.12, year * 0.03), // durability detached from plant
				new Feature(year * 8.4, year * 2), // maximum age
				new Feature(year * 3, year * 0.8)); // growth end of plant
	}
}