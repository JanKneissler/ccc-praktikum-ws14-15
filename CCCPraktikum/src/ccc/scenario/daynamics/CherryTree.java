package ccc.scenario.daynamics;

import java.awt.Color;
import java.util.Random;

import ccc.scenario.utils.ConsumableDiskMaterial;
import diskworld.DiskMaterial;
import diskworld.Environment;

public class CherryTree extends Plant {
	
	final static double year = ExternalEnvironment.DAYS_PER_YEAR;
	
	public final static ConsumableDiskMaterial CHERRY = 
			new ConsumableDiskMaterial(0.5, 0.9, 0.7, 0.8, new Color(158, 31, 38), 0.002);

	public CherryTree(double initialAge, Environment environment, ExternalEnvironment externalEnv, Random random) throws Exception {
		super(CHERRY, new DiskMaterial(0.8, 0.7, 0.4, 0.7, new Color(117, 75, 50)),
				initialAge, environment, externalEnv, random,
				new Feature(12, 2), // yield
				new Feature(0.3, 0.05), // initial stem size
				new Feature(0.6, 0.1), // final stem size
				new Feature(0.05, 0.009), // initial fruit size
				new Feature(0.3, 0.015, 0.01), // final fruit size
				new Feature[]{ new Feature(year * 0.06, year * 0.02, year*0.01) }, // fruit start dates - day 0: spring equinox
				new Feature[]{ new Feature(year * 0.17, year * 0.04, year*0.01) }, // fruit ripe dates - day 0: spring equinox
				new Feature(year * 0.1, year * 0.03), // durability attached to plant
				new Feature(year * 0.03, year * 0.01), // durability detached from plant
				new Feature(year * 9.5, year * 3), // maximum age
				new Feature(year * 5, year * 1.5)); // growth end of plant
	}
}