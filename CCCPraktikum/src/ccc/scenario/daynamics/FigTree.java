package ccc.scenario.daynamics;

import java.awt.Color;
import java.util.Random;

import ccc.scenario.utils.ConsumableDiskMaterial;
import diskworld.DiskMaterial;
import diskworld.Environment;

public class FigTree extends Plant {
	
	final static double year = ExternalEnvironment.DAYS_PER_YEAR;
	
	public final static ConsumableDiskMaterial FIG = 
			new ConsumableDiskMaterial(0.4, 0.5, 0.5, 0.85, new Color(76, 54, 57), 0.003);

	public FigTree(double initialAge, Environment environment, ExternalEnvironment externalEnv, Random random) throws Exception {
		super(FIG, new DiskMaterial(0.8, 0.7, 0.4, 0.7, new Color(107, 73, 35)),
				initialAge, environment, externalEnv, random,
				new Feature(5, 2.5), // yield
				new Feature(0.3, 0.08), // initial stem size
				new Feature(0.5, 0.2), // final stem size
				new Feature(0.06, 0.005), // initial fruit size
				new Feature(0.28, 0.01, 0.005), // final fruit size
				new Feature[]{ new Feature(year * 0.16, year * 0.02), new Feature(year * 0.38, year * 0.02)}, // fruit start dates - day 0: spring equinox
				new Feature[]{ new Feature(year * 0.26, year * 0.03), new Feature(year * 0.49, year * 0.018) }, // fruit ripe dates - day 0: spring equinox
				new Feature(year * 0.07, year * 0.03), // durability attached to plant
				new Feature(year * 0.05, year * 0.02), // durability detached from plant
				new Feature(year * 6.5, year * 1.2), // maximum age
				new Feature(year * 5, year * 0.4)); // growth end of plant
	}
}