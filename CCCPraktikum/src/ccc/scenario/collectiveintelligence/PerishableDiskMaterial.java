package ccc.scenario.collectiveintelligence;

import java.awt.Color;

import diskworld.DiskMaterial;

public class PerishableDiskMaterial extends DiskMaterial {

	protected double endOfLifeTimePoint; // Time point at which disk vanishes.

	public PerishableDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor) {
		this(density, elasticity, frictionCoefficient, gripCoefficient,
				displayColor, -1);

	}

	public PerishableDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor, double endOfLifeTime) {
		super(density, elasticity, frictionCoefficient, gripCoefficient,
				displayColor);
		this.endOfLifeTimePoint = endOfLifeTime;

	}

	public double getEndOfLifeTimePoint() {
		return endOfLifeTimePoint;
	}

	public void setEndOfLifeTimePoint(double newEndOfLifeTimePoint) {
		this.endOfLifeTimePoint = newEndOfLifeTimePoint;
	}

}
