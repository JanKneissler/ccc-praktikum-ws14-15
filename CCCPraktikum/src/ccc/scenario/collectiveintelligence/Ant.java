package ccc.scenario.collectiveintelligence;

import java.awt.Color;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.scenario.thieves.ColoredGroundSensor;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.Sensor;
import diskworld.sensors.ClosestDiskSensor;

public class Ant extends AgentConstructor {

	private final double BODY_RADIUS = 1;
	private boolean Food = false;

	Actuator mover;
	ColoredGroundSensor groundSensor;
	ClosestDiskSensor diskSensor;
	DiskMaterial agentMaterial;

	public Ant(Environment env, DiskMaterial agentMaterial, Random aRandom) {
		super(env);

		this.agentMaterial = agentMaterial;
		DiskMaterial Head = agentMaterial.withColor(Color.BLACK);
		DiskMaterial Middle = agentMaterial.withColor(Color.GRAY);
		DiskMaterial Tail = agentMaterial.withColor(Color.DARK_GRAY);

		mover = new Mover(1.5, 1.5, 0.1, 0.001, 0.01);
		groundSensor = new ColoredGroundSensor(env);
		diskSensor = ClosestDiskSensor.getDistanceSensor(env, Math.toRadians(120), 10);

		DiskType head = new DiskType(Head, mover, new Sensor[] { groundSensor });
		DiskType middle = new DiskType(Middle);
		DiskType tail = new DiskType(Tail);

		this.setRoot(BODY_RADIUS, false, head);
		this.addItem(0, Math.toRadians(180), 0, 1, middle);
		this.addItem(1, Math.toRadians(0), 0, 1, tail);
	}

	void setFood(boolean value)
	{
		Food = value;
	}

	boolean getFood()
	{
		return Food;
	}

}
