package ccc.scenario.collectiveintelligence;

import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actions.DiskModification;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class CI_Scenario extends DiskWorldScenario {

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Constants 						 													 */
	///////////////////////////////////////////////////////////////////////////////////////////

	// Minimum and maximum sizes of the environment

	private static final int SIZE = 200;
	private static final int ENV_MIN_SIZE_X = SIZE;
	private static final int ENV_MAX_SIZE_X = SIZE;
	private static final int ENV_MIN_SIZE_Y = SIZE;
	private static final int ENV_MAX_SIZE_Y = SIZE;
	private static final double AGENT_MIN_SCALE = 0.5;
	private static final int NUMBER_OF_AGENTS = 5;
	private static int NUMBER_OF_TIMESTEPS;
	private static int foodcount;
	private static int distancecount;

	//Trail2
	int[][] matrix = new int[ENV_MAX_SIZE_X][ENV_MAX_SIZE_Y];

	//Trail
	int counter = 0;
	int counter2 = 0;

	//Food

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 																				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "CI_Scenario";
	}

	@Override
	public String getDescription() {
		return "A Number of Ants is simulated \n"
				+ "the red dots are food that has to be collected,\n"
				+ "the white ground shows the trail of pheromones\n"
				+ " for other ants to smell and also find food"
				+ "The maximum Score is 1 \n"
				+ "depending on how many time steps were survived\n\n\n\n\n\n\n\n"

				+ "Use the following keys to control the agent:\n" +
				"w -> move forward\n" +
				"s -> move backward\n" +
				"d -> turn right \n" +
				"a -> turn left\n";
	}

	@Override
	public String getAuthors() {
		return "Fabian Klar";
	}

	@Override
	public String getContactEmail() {
		return "";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 												         						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario          						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X, ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex, double sizey) {
		// create bounding walls at the 4 sides of the environment 
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);
		// Add more walls here if needed
		return res;
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		// this scenario has no limitation of number of time steps, set a value > 0 to enable time limitation
		return 5000;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid red...
		res.getColorScheme().gridColor = Color.RED;
		// enable/disable options if needed: disable skeleton display...
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		return NUMBER_OF_AGENTS;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	//not needed
	protected void createDisk(ObjectConstructor objConstructor,
			Environment environment, Random random,
			double fromX, double endX, double fromY, double endY,
			double minSize, double maxSize)
	{
		double posx = ScenarioUtils.uniform(random, fromX, endX);
		double posy = ScenarioUtils.uniform(random, fromY, endY);
		double size = ScenarioUtils.uniform(random, minSize, maxSize);
		objConstructor.createObject(posx, posy, 0.0, size);
	}

	protected void trail(ControlledAgentData agent)
	{
		int x, y;
		x = (int) agent.getDisks().get(2).getX();

		y = (int) agent.getDisks().get(2).getY();

		if (matrix[x][y] < 500)
		{
			matrix[x][y] = matrix[x][y] + 200;
		}
	}

	protected void matrixFades()
	{
		for (int i = 1; i < (int) getEnvironment().getMaxX(); i++)
		{
			for (int j = 1; j < (int) getEnvironment().getMaxY(); j++)
			{

				if (matrix[i][j] > 0)
				{
					getEnvironment().getFloor().setType(i, j, FloorCellType.ICE);
					matrix[i][j]--;
				}
				if (matrix[i][j] <= 0)
				{
					getEnvironment().getFloor().setType(i, j, FloorCellType.GRASS);
				}

			}
		}
	}

	//creating random Food
	public void createFood(Random rnd, double min, double max)
	{
		int i = 0;
		double randomSize = rnd.nextDouble();
		SizedDiskMaterial foodRnd = new SizedDiskMaterial(0.5, 0.1, 0.7, 0.8, Color.RED, 0, -1, randomSize * 3 + 1);
		DiskType foodRndDisk = new DiskType(foodRnd);

		double xpos = getEnvironment().getMaxX() * rnd.nextDouble();
		double ypos = getEnvironment().getMaxY() * rnd.nextDouble();
		while (i == 0)
		{
			if ((xpos < (getEnvironment().getMaxX() / 2) - min || xpos > (getEnvironment().getMaxX() / 2) + min || ypos < (getEnvironment().getMaxY() / 2) - min || ypos > (getEnvironment().getMaxY() / 2)
					+ min)
					&& (xpos > (getEnvironment().getMaxX() / 2) - max && xpos < (getEnvironment().getMaxX() / 2) + max)
					&& (ypos > (getEnvironment().getMaxY() / 2) - max && ypos < (getEnvironment().getMaxY() / 2) + max))

			{
				/*Disk food = */getEnvironment().newFixedRoot(xpos, ypos, randomSize * 3 + 1, Math.toRadians(0), foodRndDisk);
				i = 1;
			}
			else
			{
				xpos = getEnvironment().getMaxX() * rnd.nextDouble();
				ypos = getEnvironment().getMaxY() * rnd.nextDouble();
			}
		}

	}

	@Override
	protected void initialize(Environment environment, Random random) {
		// Initialize the floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, FloorCellType.GRASS);
			}
		}

		// create Hive 
		/*Disk hive = */environment.newFixedRoot(environment.getMaxX() / 2, environment.getMaxY() / 2, 3, Math.toRadians(0), new DiskType(new DiskMaterial(0.5, 0.1, 0.7, 0.8, Color.GRAY)));

		for (int i = 0; i < 2; i++)
		{
			createFood(random, 30, 35);
		}

		NUMBER_OF_TIMESTEPS = 0;
		foodcount = 0;
		distancecount = 0;

	};

	protected AgentConstructor getAgent(Random aRandom, double scaleFactor) {
		return new Ant(getEnvironment(), DiskMaterial.METAL.withColor(Color.BLACK), aRandom);
	}

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {
		double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE, AGENT_MIN_SCALE);
		AgentConstructor ac = getAgent(aRandom, scaleFactor);
		// now repeat creating the agent at a random place
		ControlledAgentData[] agents = new ControlledAgentData[NUMBER_OF_AGENTS];
		for (int i = 0; i < NUMBER_OF_AGENTS; i++) {
			double initialEnergyLevel = 0.6; // start with half energy
			controlledAgentStates[i].resetEnergyLevel(initialEnergyLevel);

			do {
				// position near the hive
				double maxDist = 10;
				double posx = ScenarioUtils.uniform(eRandom, (getEnvironment().getMaxX() / 2) - maxDist, (getEnvironment().getMaxX() / 2) + maxDist);
				double posy = ScenarioUtils.uniform(eRandom, (getEnvironment().getMaxY() / 2) - maxDist, (getEnvironment().getMaxY() / 2) + maxDist);
				double orientation = ScenarioUtils.uniform(eRandom, 0.0, 2 * Math.PI);
				agents[i] = ac.createControlledAgent(posx, posy, orientation, scaleFactor);
			} while (agents[i] == null);
			CollisionEventHandler myHandler = createCollisionHandler(i);
			agents[i].getDisks().get(0).addEventHandler(myHandler);
		}
		return agents;
	}

	private CollisionEventHandler createCollisionHandler(final int i) {
		return new CollisionEventHandler()
		{
			@Override
			public void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse)
			{
				//Case1: Wall
				if (collidableObject instanceof Wall)
				{
					System.out.println("wall");
				}
				//Case2: Hive
				if (collidableObject instanceof Disk &&
						(((Disk) collidableObject).getDiskType().getMaterial().getDisplayColor()) == Color.GRAY)
				{
					if (isAgentFull(i)) //when carrying food
					{
						foodcount++;
						setAgentFull(i, false);
						System.out.println("Food brought Home");
					}
					else //no food
					{
						//System.out.println("No Food on me");
					}
				}
				//Case3: Food
				else if (collidableObject instanceof Disk &&
						(((Disk) collidableObject).getDiskType().getMaterial().getDisplayColor()) == Color.RED)
				{
					//when carrying food
					if (isAgentFull(i))
					{
						//System.out.println("already full!");
					}
					//else food gets smaller 
					else
					{
						System.out.println("Food Collected");
						setAgentFull(i, true); //now carrying food

						//getControlledAgents()[i].getDisks().get(1).getDiskComplex();     --color with food ?

						((SizedDiskMaterial) ((Disk) collidableObject).getDiskType().getMaterial()).setNumberOfUnits(((SizedDiskMaterial) ((Disk) collidableObject).getDiskType().getMaterial())
								.getNumberOfUnits() - 0.2);

						((Disk) collidableObject).getDiskComplex().performModification(new DiskModification(((Disk) collidableObject),
								((Disk) collidableObject).getX(),
								((Disk) collidableObject).getY(),
								0.0,
								((SizedDiskMaterial) ((Disk) collidableObject).getDiskType().getMaterial()).getNumberOfUnits()));

						//if food is too small it vanishes
						if (((SizedDiskMaterial) ((Disk) collidableObject).getDiskType().getMaterial()).getNumberOfUnits() <= 0.5)
						{
							getEnvironment().getDiskComplexesEnsemble().removeDiskComplex(((Disk) collidableObject).getDiskComplex());
						}
					}
				}
			}
		};
	}

	private boolean isAgentFull(int agentIndex) {
		ControlledAgentData agentState = getControlledAgents()[agentIndex];
		Object userData = agentState.getUserData();
		return userData == null ? false : (Boolean) userData;
	}

	private void setAgentFull(int agentIndex, boolean full) {
		ControlledAgentData agentState = getControlledAgents()[agentIndex];
		agentState.setUserData(full);
		/*if(full = true)
			agentState.getDisks().get(1).getDiskType().getMaterial().withColor(Color.red);
		else
			agentState.getDisks().get(1).getDiskType().getMaterial().withColor(Color.GRAY);
			*/
	}

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		// no npc (neutral) agents
		return new NPCAgentData[0];
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics) here)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState)
	{
		NUMBER_OF_TIMESTEPS++;
		//System.out.println(NUMBER_OF_TIMESTEPS);
		//HealthGain and MetabolicEnergyConsumption
		agentState[0].incMetabolicEnergyConsumption(0.0006 * NUMBER_OF_AGENTS * NUMBER_OF_TIMESTEPS / 1000);
		if (foodcount > 0)
		{
			agentState[0].incEnergyGained(0.013);
			foodcount--;
		}
		for (int i = 1; i < NUMBER_OF_AGENTS; i++)
		{
			agentState[i].resetEnergyLevel(agentState[0].getEnergyLevel());
		}

		//leaves a Trail behind each agent
		for (int i = 0; i < agentState.length; i++) {
			trail(getControlledAgents()[i]);
		}
		//lets the trails fade away
		matrixFades();

		//creating Random Food
		distancecount++;
		Random rnd = getERandom();
		if (rnd.nextDouble() < 0.005)
		{
			createFood(rnd, 35 + distancecount / 20, 40 + distancecount / 10);
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provides the time dependent hardness level. 											 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected double getHardnessLevel(double time) {
		return 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {

		return (double) NUMBER_OF_TIMESTEPS / this.getMaxNumTimeSteps();
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but             */
	/* dedicated to this scenario are implemented here          							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return ExampleReferenceController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI         				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			//w,s,a,d mover

			always(0, 0.0);
			always(1, 0.0);
			onKey('W', 0, 1.0, false);
			onKey('S', 0, -1.0, false);
			onKey('D', 1, -1.0, false);
			onKey('A', 1, 1.0, false);

			onKey('0', 2, 0.0, false);
			onKey('0', 3, 0.0, false);

		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(CI_Scenario.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
