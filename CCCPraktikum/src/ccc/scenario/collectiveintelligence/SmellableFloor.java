package ccc.scenario.collectiveintelligence;

import diskworld.environment.FloorCellType;

import java.awt.Color;

public class SmellableFloor extends FloorCellType
{
	private final static double FRICTION_FACTOR = 0.3;
	protected int counter;
	
	public static final SmellableFloor Smell01 = new SmellableFloor(1.0 * FRICTION_FACTOR, Color.darkGray, 0 );
	public static final SmellableFloor Smell02 = new SmellableFloor(1.0 * FRICTION_FACTOR, Color.lightGray, 0 );
	
	
	public SmellableFloor(double frictionCoefficient, Color displayColor) {
		this(frictionCoefficient, displayColor, 0);

	}
	public SmellableFloor(double frictionCoefficient, Color displayColor, int counter) {
		
		super(frictionCoefficient, displayColor);
		this.counter = counter;
	}
	
	public int getFloorCount()
	{
		return counter;
	}
	
	public void setFloorCount(int counter)
	{
		this.counter =  counter;
	}

	
	
	
	
	
	
	
	
}