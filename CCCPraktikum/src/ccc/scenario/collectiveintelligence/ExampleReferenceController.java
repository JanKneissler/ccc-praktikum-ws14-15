package ccc.scenario.collectiveintelligence;

import java.io.PrintStream;

import ccc.interfaces.CognitiveController;

public class ExampleReferenceController implements CognitiveController {

	private PrintStream log;

	@Override
	public String getName() {
		return "Reference controller for ExampleScenario";
	}

	@Override
	public String getDescription() {
		return "This is to illustrate reference controller usage. Does nothing interesting so far...";
	}

	@Override
	public String getAuthors() {
		return "Jan Kneissler";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
	public void provideLogStream(PrintStream logStream) {
		log = logStream;
	}

	@Override
	public void doTimeStep(double energyLevel, double harmLevel, double[] sensorValues, double[] actuatorValues) {
		// set actuator values based on energy/harm level and sensor values (and their history)...
		// set all actuator values to 1, just to see some action
		for (int i = 0; i < actuatorValues.length; i++) {
			actuatorValues[i] = 1;
		}
	}

	@Override
	public void nextEpisode(String[] sensors, String[] actuators) {
		// a new episode has started, we log this to the log stream
		log.println("New Episode started");
	}

	@Override
	public void terminated() {
	}

}
