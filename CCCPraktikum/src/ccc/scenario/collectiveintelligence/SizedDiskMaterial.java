package ccc.scenario.collectiveintelligence;

import java.awt.Color;
import ccc.scenario.collectiveintelligence.ConsumableDiskMaterial;

public class SizedDiskMaterial extends ConsumableDiskMaterial
{
	
	protected double numberOfUnits;
	
	public final static SizedDiskMaterial SizableFood = 
			new SizedDiskMaterial(0.5, 0.9, 0.7, 0.8, Color.RED);
	

	public SizedDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor) 
			{
				this(density, elasticity, frictionCoefficient, gripCoefficient,
						displayColor, 0, -1, 1);
			}

	public SizedDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor, double healthEffect) 
			{
				this(density, elasticity, frictionCoefficient, gripCoefficient,
						displayColor, healthEffect, -1, 1);
			}

	public SizedDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor, double healthEffect, double endOfLifeTime) 
			{
				super(density, elasticity, frictionCoefficient, gripCoefficient,
						displayColor, healthEffect, endOfLifeTime);
			}
	
	public SizedDiskMaterial(double density, double elasticity,
			double frictionCoefficient, double gripCoefficient,
			Color displayColor, double healthEffect, double endOfLifeTime, double numberOfUnits) 
			{
				this(density, elasticity, frictionCoefficient, gripCoefficient,
						displayColor, healthEffect, endOfLifeTime);
				this.numberOfUnits = numberOfUnits;
			}
	
	public double getNumberOfUnits() 
	{
		return numberOfUnits;
	}
	
	public void setNumberOfUnits(double newNumberOfUnits) 
	{
		this.numberOfUnits = newNumberOfUnits;
	}

}
