package ccc.scenario.shepherd;

import java.io.PrintStream;

import ccc.interfaces.CognitiveController;


/**
 * @author NoeF
 * This controller works in a very simple way it searches randomly for sheep and tries to drive on the left side of it
 * So that it most of the time circles the flock and gets some pretty got results.
 */
public class DogController implements CognitiveController {
	double sheepAngle = -1.1;
	
	@Override
	public void doTimeStep(double energyLevel, double harmLevel, double[] sensorValues, double[] actuatorValues) {
						
		for(int i = 1; i < sensorValues.length; i +=2){
			if(sensorValues[i] > sheepAngle && sensorValues[i] != 0.0) sheepAngle = sensorValues[i];
		}
		
		actuatorValues[0] = 0.5;
		if(sheepAngle != (-1.1)){
			actuatorValues[1] = 0.2 +  sheepAngle;
			System.out.println(sheepAngle + " | " + actuatorValues[1]);
		}else {
			actuatorValues[1] = Math.random()*2 -1;
		}
				
		sheepAngle = -1.1;
	}

	@Override
	public void nextEpisode(String[] sensors, String[] actuators) {
		// TODO Auto-generated method stub		
	}

	@Override
	public String getName() {
		return "Dog";
	}

	@Override
	public String getDescription() {
		return "Simple Controller which tries to circle arround the flock";
	}

	@Override
	public String getAuthors() {
		return "Daniel Lux";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
	public void provideLogStream(PrintStream logStream) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void terminated() {
		// TODO Auto-generated method stub	
	}

}
