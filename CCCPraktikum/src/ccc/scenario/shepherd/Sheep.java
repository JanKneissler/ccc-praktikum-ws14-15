package ccc.scenario.shepherd;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import ccc.DiskWorldScenario.AgentConstructor;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Mover;
import diskworld.interfaces.Sensor;
import diskworld.sensors.ClosestDiskSensor;

public class Sheep extends AgentConstructor {

	private static final double MAX_FORWARD_SPEED = 1.0;
	private static final double MAX_BACKWARD_SPEED = 1.0;
	private static final double MAX_ROTATION_SPEED = 1.0;
	private static final double ENERGY_CONSUMPTION = 0;
	
	public Sheep(Environment environment, DiskMaterial dogMaterial) {
		super(environment);
		
		//create Aktuator and SheepMaterial
		Mover mover = new Mover(MAX_BACKWARD_SPEED, MAX_FORWARD_SPEED, MAX_ROTATION_SPEED, ENERGY_CONSUMPTION, ENERGY_CONSUMPTION);
		DiskMaterial wool = new DiskMaterial(0.3, 1.0, 0.8, 0.9, Color.WHITE);
		
		//add Ignored Materials
		Set<DiskMaterial> ignoreSheep = new HashSet<DiskMaterial>();
		Set<DiskMaterial> ignoreDog = new HashSet<DiskMaterial>();
		ignoreDog.add(dogMaterial);
		ignoreSheep.add(wool);
		
		//create Sensors for Sheep and Dogs
		Sensor sensorForDog = new ClosestDiskSensor(environment, 0, Math.toRadians(360), 1.5, 7, ignoreSheep, true, true, false, null);
		Sensor sensorForSheep = new ClosestDiskSensor(environment, 0, Math.toRadians(360), 1.5, 10, ignoreDog, true, true, false, null);
		
		//create the Sheep
		DiskType body = new DiskType(wool, mover, new Sensor[] {sensorForDog, sensorForSheep});
		this.setRoot(0.5, body);
	}

}
