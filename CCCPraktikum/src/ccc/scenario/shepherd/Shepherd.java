/**
 * 
 */
package ccc.scenario.shepherd;

import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Mover;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.AgentController;
import diskworld.interfaces.Sensor;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class Shepherd extends DiskWorldScenario {

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Constants 						 													 */
	///////////////////////////////////////////////////////////////////////////////////////////

	// Minimum and maximum sizes of the environment and the Agent
	private static final int ENV_MIN_SIZE_X = 100;
	private static final int ENV_MAX_SIZE_X = 100;
	private static final int ENV_MIN_SIZE_Y = 100;
	private static final int ENV_MAX_SIZE_Y = 100;
	private static final double AGENT_MIN_SCALE = 1;
	private static final double AGENT_MAX_SCALE = 1;

	/**
	 * Scenario specific Constants
	 * set ENV_SHEEP_DISTRIBUTION > 0 if you want the sheep to be more centered at the beginning
	 * the number is the displacement from the edge
	 * winTimesSteps has to be bigger 0 if it is higher it gets harder to achieve 1 as score
	 * WIN_PERCENTAGE defines how close the sheep have to be together to win. has to be between 0 and 1!
	 */
	private static final int NUMBER_OF_SHEEP = 30;
	private static final int ENV_SHEEP_DISTRIBUTION_X_MIN = 20;
	private static final int ENV_SHEEP_DISTRIBUTION_X_MAX = 20;
	private static final int ENV_SHEEP_DISTRIBUTION_Y_MIN = 20;
	private static final int ENV_SHEEP_DISTRIBUTION_Y_MAX = 20;
	private static final int winTimeSteps = 10;
	private static final double WIN_PERCENTAGE = 0.85;

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 																				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	private Environment environment;
	private double energyLastTimeStep = 0.3;
	private int winCounter = 0;
	DiskMaterial dogMaterial = DiskMaterial.RUBBER.withColor(Color.YELLOW);

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "Shepherd";
	}

	@Override
	public String getDescription() {
		return "In this scenario the task is to controll a german shepherd and keep \nthe sheep herd together and probably protect them from enemys";
	}

	@Override
	public String getAuthors() {
		return "Daniel Lux";
	}

	@Override
	public String getContactEmail() {
		return "daniel.lux@student.uni-tuebingen.de";
	}

	@Override
	public String getVersion() {
		return "1.2";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 												         						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario          						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X, ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	protected Collection<Wall> createWalls(Random random, double sizex, double sizey) {
		// create bounding walls at the 4 sides of the environment 
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);
		// Add more walls here if needed
		return res;
	}

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		// this scenario has no limitation of number of time steps, set a value > 0 to enable time limitation
		return 100000;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid red...
		res.getColorScheme().gridColor = Color.RED;
		// enable/disable options if needed: disable skeleton display...
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		// always one controlled agent
		return 1;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		//store environment for later use
		this.environment = environment;

		// Initialize the floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, FloorCellType.WATER);
			}
		}
	};

	//
	//	Environment environment,
	//	double centerAngle,
	//	double openingAngle,
	//	double minRangeRelative,
	//	double maxRangeAbsolute,
	//	Set<DiskMaterial> invisibleMaterials,
	//	boolean measurePosition,
	//	boolean measureDistance,
	//	boolean measureWidth,
	//	DiskMaterialResponse materialResponse,
	//	int maxNumDisks)

	/**
	 * Create Agent with Sensor and Actuator at a random position
	 */
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] agentStates) {
		// Create Disk-sensor which can see 25 Disks and a normal Mover
		Sensor sensor = new ClosestDiskSensor(environment, 0, Math.toRadians(200), 1.3, 15, null, true, true, false, null, 25);
		Actuator actuator = new Mover(0.6, 0.9, 1, 0, 0);
		// create disk types 
		DiskType body = new DiskType(dogMaterial, actuator, new Sensor[] { sensor });

		// now create constructor specifying relative arrangement of disks
		AgentConstructor ac = new AgentConstructor(environment);
		ac.setRoot(1.0, body);

		// now repeat creating the agent at a random place
		ControlledAgentData agent = null;
		// random scale factor controlling the size (using the random object for agent)
		double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE, AGENT_MAX_SCALE);
		double initialEnergyLevel = 0.3; // start with full energy
		agentStates[0].resetEnergyLevel(initialEnergyLevel);
		do {
			// random position and orientation (using the random object for environment)
			double posx = ScenarioUtils.uniform(eRandom, 0.0, environment.getMaxX());
			double posy = ScenarioUtils.uniform(eRandom, 0.0, environment.getMaxY());
			double orientation = ScenarioUtils.uniform(eRandom, 0.0, 2 * Math.PI);
			agent = ac.createControlledAgent(posx, posy, orientation, scaleFactor);
		} while (agent == null);
		return new ControlledAgentData[] { agent };
	}

	/**
	 * creates the flock with NUMBER_OF_SHEEP size
	 */
	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		//create sheeps 
		NPCAgentData[] flock = new NPCAgentData[NUMBER_OF_SHEEP];
		Sheep sheepNpc = new Sheep(environment, dogMaterial);

		//check if sheep distribution is valid
		int sheepXMIN = 0;
		int sheepXMAX = 0;
		int sheepYMIN = 0;
		int sheepYMAX = 0;
		if ((ENV_SHEEP_DISTRIBUTION_X_MAX + ENV_SHEEP_DISTRIBUTION_X_MIN) < ENV_MAX_SIZE_X) {
			sheepXMIN = ENV_SHEEP_DISTRIBUTION_X_MIN;
			sheepXMAX = ENV_SHEEP_DISTRIBUTION_X_MAX;
		}
		if ((ENV_SHEEP_DISTRIBUTION_Y_MAX + ENV_SHEEP_DISTRIBUTION_Y_MIN) < ENV_MAX_SIZE_Y) {
			sheepYMIN = ENV_SHEEP_DISTRIBUTION_Y_MIN;
			sheepYMAX = ENV_SHEEP_DISTRIBUTION_Y_MAX;
		}

		//place sheep according to distribution
		NPCAgentData sheep;
		for (int i = 0; i < NUMBER_OF_SHEEP; i++) {
			sheep = null;
			do {
				double posX = ScenarioUtils.uniform(random, (0 + sheepXMIN), (environment.getMaxX() - sheepXMAX));
				double posY = ScenarioUtils.uniform(random, (0 + sheepYMIN), (environment.getMaxY() - sheepYMAX));
				sheep = sheepNpc.createNPCAgent(posX, posY, 0.0, 1.0, getNpcController(random));
			} while (sheep == null);
			flock[i] = sheep;
		}

		//return flock
		return flock;
	}

	/**
	 * Agent controller for sheep
	 * if dog is in reach they run away from him in a straight line,
	 * if not they walk around randomly
	 * 
	 * @param random
	 * @return
	 */
	private AgentController getNpcController(final Random random) {
		AgentController controller = new AgentController() {
			@Override
			public void doTimeStep(double[] sensorValues, double[] actuatorValues) {
				//if dog is in reach run away from it
				if (sensorValues[0] == 1) {
					actuatorValues[0] = 0.3;
					if (sensorValues[1] >= 0)
					{
						actuatorValues[1] = sensorValues[1] - 1;
					} else {
						actuatorValues[1] = 1 - sensorValues[1];
					}
				}
				//if no dog is near eat gras and walk randomly around
				else {
					actuatorValues[0] = 0.1;
					actuatorValues[1] = ScenarioUtils.uniform(random, -1, 1);
				}
			}
		};
		return controller;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics) here)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	/**
	 * Map average distance between sheeps to the energy level of the controller
	 */
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		NPCAgentData[] npcAgents = getNPCAgents();
		double energy = 0;

		//add the mean distance of each sheep together
		for (NPCAgentData a : npcAgents) {
			double[] sensor = a.getAgentMapping().getSensorValues();
			if (sensor[3] != 0)
				energy += sensor[5];
			else
				energy += 1;
		}

		//map the mean distance on the energy level
		energy = energy / NUMBER_OF_SHEEP;
		energy = 1 - energy;

		//change the Energy-level accordingly
		double difference = energy - energyLastTimeStep;
		if (difference < 0) {
			agentState[0].incMetabolicEnergyConsumption(-1 * difference);
		} else {
			agentState[0].incEnergyGained(Math.abs(difference));
		}

		energyLastTimeStep = energy;
		if (energy > WIN_PERCENTAGE) {
			winCounter++;
		} else {
			winCounter = 0;
		}

	}

	@Override
	/**
	 * if sheeps are for winTimeSteps time-steps close enough together, the scenario finishes
	 */
	public SimulationState doTimeStep() {
		if (winCounter > winTimeSteps) {
			return SimulationState.SOLVED;
		}
		return super.doTimeStep();
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provides the time dependent hardness level. 											 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected double getHardnessLevel(double time) {
		return 0; // we are merciful in the demo scenario, death is not possible
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * returns the mean distance of the sheep or if win-condition is met 1
	 */
	@Override
	public double getScore() {
		if (winCounter >= winTimeSteps) {
			return 1;
		} else {
			return energyLastTimeStep;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but             */
	/* dedicated to this scenario are implemented here          							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		//for Keyboard COntroller 
		//return ExampleReferenceController.class;

		//exampleController
		return DogController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI         				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		/**
		 * Keyboard-controler to test Scenario
		 */
		@Override
		public void addKeyEvents() {

			final boolean returnAngleToZero = true;
			final double turningAngle = returnAngleToZero ? 0.05 : 0.02;
			final double speedChange = 0.05;

			if (returnAngleToZero)
				always(1, 0.0);

			// Move forwards
			add(java.awt.event.KeyEvent.VK_NUMPAD8, 0, speedChange, true);
			add(java.awt.event.KeyEvent.VK_W, 0, speedChange, true);

			// Move backwards
			add(java.awt.event.KeyEvent.VK_NUMPAD5, 0, -speedChange, true);
			add(java.awt.event.KeyEvent.VK_S, 0, -speedChange, true);

			// Rotate left
			add(java.awt.event.KeyEvent.VK_NUMPAD4, 1, turningAngle, !returnAngleToZero);
			add(java.awt.event.KeyEvent.VK_A, 1, turningAngle, !returnAngleToZero);

			// Rotate right
			add(java.awt.event.KeyEvent.VK_NUMPAD6, 1, -turningAngle, !returnAngleToZero);
			add(java.awt.event.KeyEvent.VK_D, 1, -turningAngle, !returnAngleToZero);

			// Return speed and angle to zero
			add(java.awt.event.KeyEvent.VK_NUMPAD0, 0, 0.0, false);
			add(java.awt.event.KeyEvent.VK_NUMPAD0, 1, 0.0, false);
			add(java.awt.event.KeyEvent.VK_0, 0, 0.0, false);
			add(java.awt.event.KeyEvent.VK_0, 1, 0.0, false);

			setLogMessage("Use the following keys to control the agent:\n" +
					"a / numpad4 -> rotate left \n" +
					"d / numpad6 -> rotate right\n" +
					"w / numpad8 -> increase speed\n" +
					"s / numpad5 -> reduce speed\n" +
					"0 / numpad0 -> stop");

		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = true;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(Shepherd.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
