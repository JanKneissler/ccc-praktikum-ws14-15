package ccc.scenario.foodSearch;

import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.DiskWorldScenario;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.NPCAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.demoScenarios.ExampleReferenceController;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actuators.Mover;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.interfaces.Sensor;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.sensors.ClosestDiskSensor;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class FoodSearch extends DiskWorldScenario {

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Constants 						 													 */
	///////////////////////////////////////////////////////////////////////////////////////////

	// Minimum and maximum sizes of the environment
	private static final int ENV_MIN_SIZE_X = 50;
	private static final int ENV_MAX_SIZE_X = 100;
	private static final int ENV_MIN_SIZE_Y = 50;
	private static final int ENV_MAX_SIZE_Y = 100;
	private static final double BALL_MIN_SIZE = 0.5;
	private static int NUM_OF_TIME_STEPS = 0; //counts the number of time steps the agent surives
	//private static final double BALL_MAX_SIZE = 2.0;
	private static final double AGENT_MIN_SCALE = 1;
	private static final double AGENT_MAX_SCALE = 3;

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 																				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	private Environment environment;
	private boolean food;
	private boolean hitWall;
	private ObjectConstructor bush1;
	private ObjectConstructor bush2;
	private int counterLeftBush;
	private int counterRightBush;
	private DiskType bush2Type;
	private boolean bool_b1 = true;
	private boolean bool_b2 = true;
	private double posy_b1_start;
	private double posy_b2_start;
	private int[][] wall_array= new int[ENV_MAX_SIZE_X][ENV_MAX_SIZE_Y]; //stores all the existing walls
	private double score; //the end score
	private int count;
	public static final FloorCellType BUSH = new FloorCellType(0.3, Color.GREEN.darker().darker().darker()); //Color of the Bush
	private int wall_hardness_level = 6; // normal hardness
	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "FoodSearch";
	}

	@Override
	public String getDescription() {
		return "The agent has to find the food in order to survive.";
	}

	@Override
	public String getAuthors() {
		return "Hannah Seippel";
	}

	@Override
	public String getContactEmail() {
		return "hannah.seippel@t-online.de";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Fields 												         						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Determine the elementary properties of the scenario          						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected EnvironmentParameters determineEnvironmentParameters(Random random) {
		// determine the size of the environment
		return ScenarioUtils.getRandomEnvironmentSize(random, ENV_MIN_SIZE_X, ENV_MAX_SIZE_X, ENV_MIN_SIZE_Y, ENV_MAX_SIZE_Y);
	}

	@Override
	// creates the 4 bounding walls
	protected Collection<Wall> createWalls(Random random, double sizex, double sizey) {
		// create bounding walls at the 4 sides of the environment 
		List<Wall> res = ScenarioUtils.getBoundingWalls(sizex, sizey);
		// Add more walls here if needed
		return res;
	}
	
	//creates the horizontal walls
	protected void createHorizontalWall(){
		//The two start points and the x-coordinate for the endpoint
		double posx_wall = Math.random() * environment.getMaxX() -1;
		double posy_wall = Math.random() *environment.getMaxY() -1;
		double posx_2_wall;
		
		
		if(posx_wall+20 >environment.getMaxX())//Wall would be two long for the environment
		{
			posx_2_wall =  posx_wall;
			posx_wall = posx_2_wall-20;//makes a south wall
		}else{
			posx_2_wall =  posx_wall+20;//makes a north wall
		}
		
		//Generates start and end point
		Point start = new Point (posx_wall,posy_wall);
		Point end = new Point (posx_2_wall,posy_wall);
		
		//Tests if it can add an other horizontal wall
		if(allowedToAddHorizontalWall((int)posx_wall,(int)posy_wall) ==true){
			
			// generates the wall
			Line wallLine = new Line(start , end);
			Wall wall = new Wall(wallLine, 0.5);
			if (getEnvironment().canAddWall(wall)) {
				log("Added a wall");
			}
			//adds the wall to the wall_array
			for (int x = (int)posx_wall ; x < (int)posx_2_wall; x++){
				wall_array[x][(int)posy_wall] = 1;
			}
			
		}else{//tries 20 times to add the wall, if it can't add a wall after 20 tries it stops
			count++;
			if(count < 20){
				createHorizontalWall();
			}else{ log("Horizontal wall couldn't be added");}
			
		}
	}
	
	//Tries to add a vertical wall
	protected void createVerticalWall(){
		
		// an x point, and two y points for the start and the end
		double posx_wall = Math.random() * environment.getMaxX() -1;
		double posy_wall = Math.random() *environment.getMaxY() -1;
		double posy_2_wall;
		
		
		if(posy_wall+20 > environment.getMaxY())//wall would be to long
		{
			posy_2_wall = posy_wall;
			posy_wall = posy_2_wall-20;//makes a west wall
		}else{
			posy_2_wall = posy_wall+20;//makes a east wall
		}
		
		//Creates start and endpoints
		Point start = new Point (posx_wall,posy_wall);
		Point end = new Point (posx_wall,posy_2_wall);
		
		//checks if the wall can be added
		if(allowedToAddVerticalWall((int)posx_wall,(int)posy_wall) == true){
			
			//generates the wall
			Line wallLine = new Line(start , end);
			Wall wall = new Wall(wallLine, 0.5);
			
			//adds the wall to the wall_array
			for (int y = (int)posy_wall ; y < (int)posy_2_wall; y++){
				wall_array[(int)posx_wall][y] = 1;
			}
			if (getEnvironment().canAddWall(wall)) {
				log("Added a wall");
			}
		}else{// tries 20 times to add a wall, and stop after 20 trials
			count++;
			if(count < 20){
				createVerticalWall();
			}else{log("Vertical wall couldn't be added");}
			
		}	
	}
	
	//creates the bush on the left
	protected void createLeftBush(Environment environment, Random random){
		//create Bush1
		counterLeftBush = 0;
		int max_x = 0;
		int max_y = 0;
		int min_x = (int)environment.getMaxX();
		int min_y = (int)environment.getMaxY();
		
		DiskType ballType = new DiskType(DiskMaterial.METAL.withColor(Color.RED));
		bush1 = environment.createObjectConstructor();
		bush1.setRoot(1.0, ballType);
		
		double posx_start = 2;
		if(bool_b1){
			posy_b1_start = ScenarioUtils.uniform(random, 2, environment.getMaxY()-2);
			bool_b1 = false;
		}
		double size = ScenarioUtils.uniform(random, BALL_MIN_SIZE, BALL_MIN_SIZE);
		
		if (bush1.createObject(posx_start, posy_b1_start, 0.0 , size)) { }
		int num = 0;
		double posx;
		double posy;
		do {
			posx = posx_start+((Math.random()) * 4 + 1);
			if(posy_b1_start+5 < environment.getMaxY()){ posy = posy_b1_start+((Math.random()) * 4 + 1);
			}
			else{posy = posy_b1_start-((Math.random()) * 4 + 1);
			}
			size = ScenarioUtils.uniform(random, BALL_MIN_SIZE, BALL_MIN_SIZE);
			if (bush1.createObject(posx, posy, 0.0 , size)) {
				num++;
				counterLeftBush++;
			}	
			//if cases find the x and y values of the berry with the min values and the berry with the max values
			if(posx > max_x){
				max_x = (int)posx;
			}
			if(posx < min_x){
				min_x = (int)posx;
			}
			if(posy > max_y){
				max_y = (int)posy;
			}
			if(posy < min_y){
				min_y = (int)posy;
			}
		} while (num < 10);	//creates 10 berries of the bush
		
		//Adds the dark green bush that surrounds the berries.
		Floor floor = environment.getFloor();
		for (int x = min_x-1; x <= max_x+1; x++) {
			for (int y = min_y-1; y <= max_y+1; y++) {
				if(x > 0 && y > 0 && x < floor.getMaxX() && y < floor.getMaxY()) {
				floor.setType(x, y, BUSH);
				}
			}
		}
		for (int x = min_x-2; x <= max_x+2; x++) {
			for (int y = min_y-1; y <= max_y+1; y++) {
				if(y%2 == 0 && x > 0 && y > 0 && x < floor.getMaxX() && y < floor.getMaxY() ){
				floor.setType(x, y, BUSH);	}
			}
		}
		for (int x = min_x-1; x <= max_x+1; x++) {
			for (int y = min_y-2; y <= max_y+2; y++) {
				if(x%2 == 0 && x > 0 && y > 0 && x < floor.getMaxX() && y < floor.getMaxY() ){
				floor.setType(x, y, BUSH);	}
			}
		}	
	}
	
	//creates the right bush
	protected void createRightBush(Environment environment, Random random){
		//Create Bush2
		int max_x = 0;
		int max_y = 0;
		int min_x = (int)environment.getMaxX();
		int min_y = (int)environment.getMaxY();
		counterRightBush = 0;
		
		bush2Type = new DiskType(DiskMaterial.RUBBER.withColor(Color.RED));
		bush2 = environment.createObjectConstructor();
		bush2.setRoot(1.0, bush2Type);
		
		double posx_start = environment.getMaxX()-2;
		if(bool_b2){
		posy_b2_start = ScenarioUtils.uniform(random, 2, environment.getMaxY()-2);
		bool_b2 = false;
		}
		double size = ScenarioUtils.uniform(random, BALL_MIN_SIZE, BALL_MIN_SIZE);
		
		if (bush2.createObject(posx_start, posy_b2_start, 0.0 , size)) { }
		int num = 0;
		double posx;
		double posy;
		do {
			posx = posx_start-((Math.random()) * 4 + 1);
			if(posy_b2_start-5 >0){posy = posy_b2_start-((Math.random()) * 4 + 1);}
			else{posy= posy_b2_start+((Math.random()) * 4 + 1);}
			size = ScenarioUtils.uniform(random, BALL_MIN_SIZE, BALL_MIN_SIZE);
			if (bush2.createObject(posx, posy, 0.0 , size)) {
				num++;
				counterRightBush++;
			}
			
			//gets the x and y value of the berry with the min values and the berry with the max values
			if(posx > max_x){
				max_x = (int)posx;
			}
			if(posx < min_x){
				min_x = (int)posx;
			}
			if(posy > max_y){
				max_y = (int)posy;
			}
			if(posy < min_y){
				min_y = (int)posy;
			}
					
		} while (num < 10);// creates 10 berries
		
		//Adds the dark green bush that surrounds the berries
		Floor floor = environment.getFloor();
		for (int x = min_x-1; x <= max_x+1; x++) {
			for (int y = min_y-1; y <= max_y+1; y++) {
				floor.setType(x, y, BUSH);
				
			}
		}
		for (int x = min_x-2; x <= max_x+2; x++) {
			for (int y = min_y-1; y <= max_y+1; y++) {
				if(y%2 == 0)
				floor.setType(x, y, BUSH);	
			}
		}
		for (int x = min_x-1; x <= max_x+1; x++) {
			for (int y = min_y-2; y <= max_y+2; y++) {
				if(x%2 == 0)
				floor.setType(x, y, BUSH);	
			}
		}
	}
	
	//checks if a horizontal wall can be added
	protected boolean allowedToAddHorizontalWall(int start_x, int start_y){
		
		boolean wall=true;
		//values can be changed making it harder or easier
		int a = wall_hardness_level;
		int b = wall_hardness_level;
		int c = wall_hardness_level;
		int d = wall_hardness_level;
		
		//checks if the wall is surrounded by other walls
		if(start_x < wall_hardness_level){a = start_x;}
		if(start_x+20 >(ENV_MAX_SIZE_X-6)){b=ENV_MAX_SIZE_X-(start_x+20)-1;}
		if(start_y<wall_hardness_level){c=start_y-1;}
		if(start_y>ENV_MAX_SIZE_Y-wall_hardness_level){d=ENV_MAX_SIZE_Y-start_y-1;}
		
		for(int x = start_x-a;x < start_x+20+b; x++){
			for(int y = start_y-c; y < start_y+d; y++){
				if(wall_array[x][y] == 1){
					wall = false;		
				}
			}
		}
		
		return wall;
	}
	protected boolean allowedToAddVerticalWall(int start_x, int start_y){
				
	boolean wall = true;
	int a = wall_hardness_level;
	int b = wall_hardness_level;
	int c = wall_hardness_level;
	int d = wall_hardness_level;
	
	
	if(start_x < wall_hardness_level){a = start_x;}
	if(start_x >ENV_MAX_SIZE_X-wall_hardness_level){b=ENV_MAX_SIZE_X-start_x-1;}
	if(start_y<wall_hardness_level){c=start_y-1;}
	if(start_y+20>ENV_MAX_SIZE_Y-wall_hardness_level){d=ENV_MAX_SIZE_Y-(start_y+20)-1;}		
	
	
		for(int x = start_x-a;x < start_x+b; x++){
			for(int y = start_y-c; y < start_y+20+d; y++){
				if(wall_array[x][y] == 1){
							wall = false;
				}	
			}
		}
		return wall;
	}
	

	@Override
	protected int determineMaxNumTimeSteps(Random random) {
		// this scenario has no limitation of number of time steps, set a value > 0 to enable time limitation
		return 5000;//UNLIMITED_TIME;
	}

	@Override
	protected VisualizationSettings getVisualisationSettings() {
		VisualizationSettings res = new VisualizationSettings();
		// change elements of the color scheme if needed: lets make the grid red...
		res.getColorScheme().gridColor = Color.RED;
		// enable/disable options if needed: disable skeleton display...
		//res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_SKELETON).setEnabled(false);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_SKELETON).setEnabled(true);
		res.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		return res;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		// TODO Auto-generated method stub
		return 1;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		//store environment for later use
		this.environment = environment;

		// Initialize the floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, FloorCellType.GRASS);
			}
		}
		createLeftBush(environment,random);
		//createRightBush(environment,random);
		
		//score is 0 in the beginning
		score = 0;
		//wall_array is empty in the beginning
		for(int x = 0; x < ENV_MAX_SIZE_X;x++){
			for(int y = 0; y < ENV_MAX_SIZE_Y;y++){
				wall_array[x][y]=0;
			}
		}
		
	}

	protected ControlledAgentData[] getControlledAgents(final Random aRandom, final Random eRandom, AgentState[] agentStates) {
				
				// sensors and actuators
				Sensor sensor = ClosestDiskSensor.getDistanceSensor(environment, Math.toRadians(200), 20);
				Actuator actuator = new Mover(1.0, 1.5, 0.05, 0.001, 0.01);

				// create disk types
				DiskType body = new DiskType(DiskMaterial.RUBBER.withColor(Color.GRAY), actuator, new Sensor[] { sensor });

				// now create constructor specifying relative arrangement of disks
				AgentConstructor ac = new AgentConstructor(environment);
				ac.setRoot(1.0, body);

				// now repeat creating the agent at a random place
				ControlledAgentData agent;
				// random scale factor controlling the size (using the random object for agent)
				double scaleFactor = ScenarioUtils.uniform(aRandom, AGENT_MIN_SCALE, AGENT_MAX_SCALE);
				double initialEnergyLevel = 1.0; // start with full energy
				agentStates[0].resetEnergyLevel(initialEnergyLevel);
				do {
					// random position and orientation (using the random object for environment)
					double posx = ScenarioUtils.uniform(eRandom, 0.0, environment.getMaxX());
					double posy = ScenarioUtils.uniform(eRandom, 0.0, environment.getMaxY());
					double orientation = ScenarioUtils.uniform(eRandom, 0.0, 2 * Math.PI);
					agent = ac.createControlledAgent(posx, posy, orientation, scaleFactor);
				} while (agent == null);

				agent.getDisks().get(0).addEventHandler(new CollisionEventHandler() {
					@Override
					public void collision(CollidableObject collidableObject, Point collisionPoint, double exchangedImpulse) {
						
						if (collidableObject instanceof Wall) {
							hitWall = true;
						}
						else if (collidableObject instanceof Disk) {
							food = true;
							if(((Disk) collidableObject).getDiskType().equals(bush2Type)){
								environment.getDiskComplexesEnsemble().removeDiskComplex(((Disk) collidableObject).getDiskComplex());
								if(counterRightBush == 0){
									//bush
									createLeftBush(environment, aRandom);
									//wall
									count =0;
									createHorizontalWall();
									
								}
								counterRightBush--;
							}else{environment.getDiskComplexesEnsemble().removeDiskComplex(((Disk) collidableObject).getDiskComplex());
								if(counterLeftBush == 0){
									//bush
									createRightBush(environment, aRandom);
									//wall
									count = 0;
									createVerticalWall();	
							
								}
								counterLeftBush--;
							}
						}
					}
				});
				return new ControlledAgentData[] { agent };
	}

	@Override
	protected NPCAgentData[] getNPCAgents(Random random) {
		// no npc (neutral) agents
		return new NPCAgentData[0];
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics) here)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		NUM_OF_TIME_STEPS ++;
		if (food) {
			agentState[0].incEnergyGained(10);
			food = false;
			log("You found food");
		}
		if(hitWall){
			agentState[0].incHarmInflicted(0.001);
			hitWall = false;
			log("You hit a wall");
		}
		
		agentState[0].incEnergyConsumption(0.002);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provides the time dependent hardness level. 											 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected double getHardnessLevel(double time) {
		return 0; // we are merciful in the demo scenario, death is not possible
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
	//Score can bebewteen 0 and 1 depending on how long the agent survies.
	score = (double)NUM_OF_TIME_STEPS/this.getMaxNumTimeSteps();
		
		return score;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Reference controller (implementing the CognitiveController interface, but             */
	/* dedicated to this scenario are implemented here          							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return ExampleReferenceController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent(s) by keyboard or GUI         				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			// we create a keyboard controller with three actions:
			// when key '+' is pressed: increment first value in the actuator array (index 0) by 0.01
			// when key '-' is pressed: decrement it by 0.01
			// when key 0 is pressed: set it to 0.0
			always(0, 0.0);
			always(1, 0.0);
			onKey('W', 0, 1.0, false);
			onKey('S', 0, -1.0, false);
			onKey('D', 1, -1.0, false);
			onKey('A', 1, 1.0, false);

			onKey('0', 2, 0.0, false);
			onKey('0', 3, 0.0, false);
			setLogMessage("Use the following keys to control the agent:\n" +
					"w -> move forward\n" +
					"s -> move backward\n" +
					"d -> turn right \n" +
					"a -> turn left\n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = true;
		boolean useKeyboardController = true;
		boolean useGuiController = false;
		Main.addScenario(FoodSearch.class, useReferenceController, useKeyboardController, useGuiController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}
}
